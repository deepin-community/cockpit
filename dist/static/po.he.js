window.cockpit_po = {
 "": {
  "plural-forms": (n) => (n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && n % 10 == 0) ? 2 : 3)),
  "language": "he",
  "language-direction": "rtl"
 },
 "$0 GiB": [
  null,
  "$0 GiB"
 ],
 "$0 day": [
  null,
  "יום",
  "יומיים",
  "$0 ימים",
  "$0 ימים"
 ],
 "$0 error": [
  null,
  "שגיאת $0"
 ],
 "$0 exited with code $1": [
  null,
  "$0 הסתיים עם הקוד $1"
 ],
 "$0 failed": [
  null,
  "$0 נכשל"
 ],
 "$0 hour": [
  null,
  "שעה",
  "שעתיים",
  "$0 שעות",
  "$0 שעות"
 ],
 "$0 is not available from any repository.": [
  null,
  "$0 אינו זמין מאף מאגר."
 ],
 "$0 key changed": [
  null,
  "המפתח $0 השתנה"
 ],
 "$0 killed with signal $1": [
  null,
  "$0 נקטל עם האות $1"
 ],
 "$0 minute": [
  null,
  "דקה",
  "$0 דקות",
  "$0 דקות",
  "$0 דקות"
 ],
 "$0 month": [
  null,
  "חודש",
  "חודשיים",
  "$0 חודשים",
  "$0 חודשים"
 ],
 "$0 week": [
  null,
  "שבוע",
  "שבועיים",
  "$0 שבועות",
  "$0 שבועות"
 ],
 "$0 will be installed.": [
  null,
  "$0 יותקן."
 ],
 "$0 year": [
  null,
  "שנה",
  "שנתיים",
  "$0 שנים",
  "$0 שנים"
 ],
 "1 day": [
  null,
  "יום"
 ],
 "1 hour": [
  null,
  "שעה"
 ],
 "1 minute": [
  null,
  "דקה"
 ],
 "1 week": [
  null,
  "שבוע"
 ],
 "20 minutes": [
  null,
  "20 דקות"
 ],
 "40 minutes": [
  null,
  "40 דקות"
 ],
 "5 minutes": [
  null,
  "5 דקות"
 ],
 "6 hours": [
  null,
  "6 שעות"
 ],
 "60 minutes": [
  null,
  "60 דקות"
 ],
 "A compatible version of Cockpit is not installed on $0.": [
  null,
  "לא מותקנת גרסה תואמת של Cockpit ב־$0."
 ],
 "A modern browser is required for security, reliability, and performance.": [
  null,
  "נדרש דפדפן עדכני לטובת אבטחה, אמינות וביצועים."
 ],
 "A new SSH key at $0 will be created for $1 on $2 and it will be added to the $3 file of $4 on $5.": [
  null,
  "מפתח SSH חדש תחת $0 ייווצר עבור $1 על גבי $2 והוא יתווסף לקובץ $3 של $4 על גבי $5."
 ],
 "Absent": [
  null,
  "חסר"
 ],
 "Accept key and log in": [
  null,
  "לקבל את המפתח ולהיכנס"
 ],
 "Acceptable password": [
  null,
  "סיסמה מקובלת"
 ],
 "Add $0": [
  null,
  "הוספת $0"
 ],
 "Additional packages:": [
  null,
  "חבילות נוספות:"
 ],
 "Administration with Cockpit Web Console": [
  null,
  "ניהול עם המסוף המקוון Cockpit"
 ],
 "Advanced TCA": [
  null,
  "TCA מתקדם"
 ],
 "All-in-one": [
  null,
  "אול אין ואן"
 ],
 "Ansible": [
  null,
  "Ansible"
 ],
 "Ansible roles documentation": [
  null,
  "תיעוד תפקידים של Ansible"
 ],
 "Authentication": [
  null,
  "אימות"
 ],
 "Authentication failed": [
  null,
  "האימות נכשל"
 ],
 "Authentication is required to perform privileged tasks with the Cockpit Web Console": [
  null,
  "נדרש אימות כדי לבצע משימות שדורשות השראה עם המסוף המקוון Cockpit"
 ],
 "Authorize SSH key": [
  null,
  "אישור מפתח SSH"
 ],
 "Automatically using NTP": [
  null,
  "אוטומטית באמצעות NTP"
 ],
 "Automatically using additional NTP servers": [
  null,
  "אוטומטית באמצעות שרתי NTP נוספים"
 ],
 "Automatically using specific NTP servers": [
  null,
  "אוטומטית באמצעות שרתי NTP מסוימים"
 ],
 "Automation script": [
  null,
  "סקריפט אוטומטי"
 ],
 "Blade": [
  null,
  "בלייד"
 ],
 "Blade enclosure": [
  null,
  "אריזת בלייד"
 ],
 "Bus expansion chassis": [
  null,
  "שלדת הרחבת אפיקים"
 ],
 "Cancel": [
  null,
  "ביטול"
 ],
 "Cannot forward login credentials": [
  null,
  "לא ניתן להעביר פרטי גישה"
 ],
 "Cannot schedule event in the past": [
  null,
  "לא ניתן לתזמן אירוע לעבר"
 ],
 "Change": [
  null,
  "החלפה"
 ],
 "Change system time": [
  null,
  "החלפת שעון המערכת"
 ],
 "Changed keys are often the result of an operating system reinstallation. However, an unexpected change may indicate a third-party attempt to intercept your connection.": [
  null,
  "מפתחות שהוחלפו הם לעתים תוצאה של התקנת מערכת הפעלה מחדש. עם זאת, שינוי בלתי צפוי עשוי להעיד שגורם צד־שלישי מנסה ליירט את החיבור שלך."
 ],
 "Checking installed software": [
  null,
  "התכנה שמותקנת נבדקת"
 ],
 "Close": [
  null,
  "סגירה"
 ],
 "Cockpit": [
  null,
  "Cockpit"
 ],
 "Cockpit authentication is configured incorrectly.": [
  null,
  "האימות של Cockpit לא מוגדר נכון."
 ],
 "Cockpit configuration of NetworkManager and Firewalld": [
  null,
  "ההגדרות של Cockpit ל־NetworkManager ול־Firewalld"
 ],
 "Cockpit could not contact the given host.": [
  null,
  "ל־Cockpit אין אפשרות ליצור קשר עם המארח שסופק."
 ],
 "Cockpit is a server manager that makes it easy to administer your Linux servers via a web browser. Jumping between the terminal and the web tool is no problem. A service started via Cockpit can be stopped via the terminal. Likewise, if an error occurs in the terminal, it can be seen in the Cockpit journal interface.": [
  null,
  "Cockpit הוא מנהל שרתים שמקל על ניהול שרתי הלינוקס שלך דרך הדפדפן. מעבר חטוף בין המסוף והכלי המקוון הוא פשוט וקל. שירות שהופעל דרך Cockpit ניתן לעצור דרך המסוף. באותו האופן, אם מתרחשת שגיאה במסוף ניתן לצפות בה במנשק היומן של Cockpit."
 ],
 "Cockpit is not compatible with the software on the system.": [
  null,
  "Cockpit אינו תואם לתכנה שרצה על המערכת."
 ],
 "Cockpit is not installed": [
  null,
  "Cockpit אינו מותקן"
 ],
 "Cockpit is not installed on the system.": [
  null,
  "Cockpit אינו מותקן על המערכת."
 ],
 "Cockpit is perfect for new sysadmins, allowing them to easily perform simple tasks such as storage administration, inspecting journals and starting and stopping services. You can monitor and administer several servers at the same time. Just add them with a single click and your machines will look after its buddies.": [
  null,
  "Cockpit הוא מושלם למנהלי מערכות מתחילים, מאפשר להם לבצע משימות פשוטות בקלות כגון ניהול אחסון, חקירת יומנים והפעלה ועצירה של שירותים. ניתן לנהל ולעקוב אחר מספר שרתים בו־זמנית. כל שעליך לעשות הוא להוסיף אותם בלחיצה בודדת והמכונות שלך תדאגנה לחברותיהן."
 ],
 "Cockpit might not render correctly in your browser": [
  null,
  "יכול להיות ש־Cockpit לא יעובד כראוי בדפדפן שלך"
 ],
 "Collect and package diagnostic and support data": [
  null,
  "איסוף ואריזה של נתוני ניתוח ותמיכה"
 ],
 "Collect kernel crash dumps": [
  null,
  "איסוף היטלי קריסת ליבה"
 ],
 "Compact PCI": [
  null,
  "PCI חסכוני"
 ],
 "Confirm key password": [
  null,
  "אישור סיסמת מפתח"
 ],
 "Connect to": [
  null,
  "התחברות אל"
 ],
 "Connect to:": [
  null,
  "התחברות אל:"
 ],
 "Connection has timed out.": [
  null,
  "הזמן שהוקצב להתחברות תם."
 ],
 "Convertible": [
  null,
  "מתהפך"
 ],
 "Copied": [
  null,
  "הועתק"
 ],
 "Copy": [
  null,
  "העתקה"
 ],
 "Copy to clipboard": [
  null,
  "העתקה ללוח הגזירים"
 ],
 "Create a new SSH key and authorize it": [
  null,
  "ליצור מפתח SSH חדש ולאשר אותו"
 ],
 "Create new task file with this content.": [
  null,
  "יצירת קובץ משימה עם התוכן הזה."
 ],
 "Ctrl+Insert": [
  null,
  "Ctrl+Insert"
 ],
 "Delay": [
  null,
  "השהיה"
 ],
 "Desktop": [
  null,
  "שולחן עבודה"
 ],
 "Detachable": [
  null,
  "נתיק"
 ],
 "Diagnostic reports": [
  null,
  "דוחות אבחון"
 ],
 "Docking station": [
  null,
  "תחנת עגינה"
 ],
 "Download a new browser for free": [
  null,
  "הורדת דפדפן חדש בחינם"
 ],
 "Downloading $0": [
  null,
  "$0 בהורדה"
 ],
 "Dual rank": [
  null,
  "דו־צדדי"
 ],
 "Embedded PC": [
  null,
  "מחשב משובץ"
 ],
 "Excellent password": [
  null,
  "סיסמה מצוינת"
 ],
 "Expansion chassis": [
  null,
  "שלדת הרחבה"
 ],
 "Failed to change password": [
  null,
  "החלפת הסיסמה נכשלה"
 ],
 "Failed to enable $0 in firewalld": [
  null,
  "הפעלת $0 ב־firewalld נכשלה"
 ],
 "Go to now": [
  null,
  "לעבור כעת"
 ],
 "Handheld": [
  null,
  "נישא"
 ],
 "Hide confirmation password": [
  null,
  "הסתרת סיסמת האישור"
 ],
 "Hide password": [
  null,
  "הסתרת הסיסמה"
 ],
 "Host key is incorrect": [
  null,
  "מפתח המארח שגוי"
 ],
 "If the fingerprint matches, click \"Accept key and log in\". Otherwise, do not log in and contact your administrator.": [
  null,
  "אם טביעת האצבע תואמת, יש ללחוץ על „לקבל את המפתח להיכנס”. אחרת, לא להתחבר וליצור קשר עם הנהלת המערכת."
 ],
 "If the fingerprint matches, click 'Trust and add host'. Otherwise, do not connect and contact your administrator.": [
  null,
  "אם טביעת האצבע תואמת, יש ללחוץ על ‚מתן אמון והוספת מארח’. אחרת, לא להתחבר וליצור קשר עם הנהלת המערכת."
 ],
 "Install": [
  null,
  "התקנה"
 ],
 "Install software": [
  null,
  "התקנת תכנה"
 ],
 "Install the cockpit-system package (and optionally other cockpit packages) on $0 to enable web console access.": [
  null,
  ""
 ],
 "Installing $0": [
  null,
  "$0 בהתקנה"
 ],
 "Internal error": [
  null,
  "שגיאה פנימה"
 ],
 "Internal error: Invalid challenge header": [
  null,
  "שגיאה פנימית: כותרת אתגר שגויה"
 ],
 "Invalid date format": [
  null,
  "מבנה התאריך שגוי"
 ],
 "Invalid date format and invalid time format": [
  null,
  "מבנה תאריך שגוי ומבנה שעה שגוי"
 ],
 "Invalid file permissions": [
  null,
  "הרשאות הקובץ שגויות"
 ],
 "Invalid time format": [
  null,
  "מבנה השעה שגוי"
 ],
 "Invalid timezone": [
  null,
  "אזור זמן שגוי"
 ],
 "IoT gateway": [
  null,
  "שער גישה IoT"
 ],
 "Kernel dump": [
  null,
  "היטל ליבה"
 ],
 "Key password": [
  null,
  "סיסמת מפתח"
 ],
 "Laptop": [
  null,
  "מחשב נייד"
 ],
 "Learn more": [
  null,
  "מידע נוסף"
 ],
 "Loading system modifications...": [
  null,
  "השינויים למערכת נטענים…"
 ],
 "Log in": [
  null,
  "כניסה"
 ],
 "Log in to $0": [
  null,
  "כניסה אל $0"
 ],
 "Log in with your server user account.": [
  null,
  "כניסה עם חשבון המשתמש שלך בשרת."
 ],
 "Log messages": [
  null,
  "הודעות יומן"
 ],
 "Login": [
  null,
  "כניסה"
 ],
 "Login again": [
  null,
  "נא להיכנס שוב"
 ],
 "Login failed": [
  null,
  "הכניסה נכשלה"
 ],
 "Logout successful": [
  null,
  "היציאה הצליחה"
 ],
 "Low profile desktop": [
  null,
  "מחשב שולחני עם פרופיל נמוך"
 ],
 "Lunch box": [
  null,
  "קופסת אוכל"
 ],
 "Main server chassis": [
  null,
  "שלדת שרת ראשית"
 ],
 "Manage storage": [
  null,
  "ניהול אחסון"
 ],
 "Manually": [
  null,
  "ידנית"
 ],
 "Message to logged in users": [
  null,
  "הודעה למשתמשים שנמצאים במערכת"
 ],
 "Mini PC": [
  null,
  "מחשב מוקטן"
 ],
 "Mini tower": [
  null,
  "מארז מוקטן"
 ],
 "Multi-system chassis": [
  null,
  "שלדה למגוון מערכות"
 ],
 "NTP server": [
  null,
  "שרת NTP"
 ],
 "Need at least one NTP server": [
  null,
  "נדרש שרת NTP אחד לפחות"
 ],
 "Networking": [
  null,
  "תקשורת"
 ],
 "New host": [
  null,
  "מארח חדש"
 ],
 "New password was not accepted": [
  null,
  "הסיסמה החדשה לא התקבלה"
 ],
 "No delay": [
  null,
  "אין השהיה"
 ],
 "No results found": [
  null,
  "לא נמצאו תוצאות"
 ],
 "No such file or directory": [
  null,
  "אין קובץ או תיקייה בשם הזה"
 ],
 "No system modifications": [
  null,
  "אין שינויים במערכת"
 ],
 "Not a valid private key": [
  null,
  "לא מפתח פרטי תקני"
 ],
 "Not permitted to perform this action.": [
  null,
  "לא מורשה לבצע את הפעולה הזאת."
 ],
 "Not synchronized": [
  null,
  "לא מסונכרן"
 ],
 "Notebook": [
  null,
  "מחברת"
 ],
 "Occurrences": [
  null,
  "מופעים"
 ],
 "Ok": [
  null,
  "אישור"
 ],
 "Old password not accepted": [
  null,
  "הסיסמה הישנה לא התקבלה"
 ],
 "Once Cockpit is installed, enable it with \"systemctl enable --now cockpit.socket\".": [
  null,
  "לאחר התקנת Cockpit, יש להפעיל את השירות בעזרת „systemctl enable --now cockpit.socket”."
 ],
 "Or use a bundled browser": [
  null,
  "או להשתמש בדפדפן מובנה"
 ],
 "Other": [
  null,
  "אחר"
 ],
 "Other options": [
  null,
  "אפשרויות אחרות"
 ],
 "PackageKit crashed": [
  null,
  "PackageKit קרס"
 ],
 "Packageless session unavailable": [
  null,
  ""
 ],
 "Password": [
  null,
  "סיסמה"
 ],
 "Password is not acceptable": [
  null,
  "הסיסמה לא מקובלת"
 ],
 "Password is too weak": [
  null,
  "הסיסמה חלשה מדי"
 ],
 "Password not accepted": [
  null,
  "הסיסמה לא התקבלה"
 ],
 "Paste": [
  null,
  "הדבקה"
 ],
 "Paste error": [
  null,
  "שגיאת הדבקה"
 ],
 "Path to file": [
  null,
  "הנתיב לקובץ"
 ],
 "Peripheral chassis": [
  null,
  "שלדת התקנים חיצוניים"
 ],
 "Permission denied": [
  null,
  "ההרשאה נדחתה"
 ],
 "Pick date": [
  null,
  "בחירת תאריך"
 ],
 "Pizza box": [
  null,
  "קופסת פיצה"
 ],
 "Please enable JavaScript to use the Web Console.": [
  null,
  "נא להפעיל JavaScript כדי להשתמש במסוף לדפדפן."
 ],
 "Please specify the host to connect to": [
  null,
  "נא לציין את המארח לחיבור"
 ],
 "Portable": [
  null,
  "נייד"
 ],
 "Present": [
  null,
  "נוכחי"
 ],
 "Prompting via ssh-add timed out": [
  null,
  "משך הצגת הבקשה דרך ssh-add תם"
 ],
 "Prompting via ssh-keygen timed out": [
  null,
  "משך הצגת הבקשה דרך ssh-keygen תם"
 ],
 "RAID chassis": [
  null,
  "שלדת RAID"
 ],
 "Rack mount chassis": [
  null,
  "שלדת מעגן מתלה (Rack)"
 ],
 "Reboot": [
  null,
  "הפעלה מחדש"
 ],
 "Recent hosts": [
  null,
  "מארחים אחרונים"
 ],
 "Removals:": [
  null,
  "הסרות:"
 ],
 "Remove host": [
  null,
  "הסרת מארח"
 ],
 "Removing $0": [
  null,
  "$0 בהסרה"
 ],
 "Run this command over a trusted network or physically on the remote machine:": [
  null,
  ""
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "SSH key": [
  null,
  "מפתח SSH"
 ],
 "Sealed-case PC": [
  null,
  "מחשב במארז אטום"
 ],
 "Security Enhanced Linux configuration and troubleshooting": [
  null,
  "הגדרות ופתרון תקלות של Security Enhanced Linux"
 ],
 "Server": [
  null,
  "שרת"
 ],
 "Server has closed the connection.": [
  null,
  "השרת סגר את החיבור."
 ],
 "Set time": [
  null,
  "הגדרת שעה"
 ],
 "Shell script": [
  null,
  "סקריפט מעטפת"
 ],
 "Shift+Insert": [
  null,
  "Shift+Insert"
 ],
 "Show password": [
  null,
  "הצגת הסיסמה"
 ],
 "Shut down": [
  null,
  "כיבוי"
 ],
 "Single rank": [
  null,
  "שורה אחת"
 ],
 "Space-saving computer": [
  null,
  "מחשב חסכוני במקום"
 ],
 "Specific time": [
  null,
  "זמן מסוים"
 ],
 "Stick PC": [
  null,
  "מחשב מקלון"
 ],
 "Storage": [
  null,
  "אחסון"
 ],
 "Sub-Chassis": [
  null,
  "תת שלדה"
 ],
 "Sub-Notebook": [
  null,
  "תת מחברת"
 ],
 "Synchronized": [
  null,
  "מסונכרן"
 ],
 "Synchronized with $0": [
  null,
  "מסונכרן עם $0"
 ],
 "Synchronizing": [
  null,
  "מתבצע סנכרון"
 ],
 "Tablet": [
  null,
  "מחשב לוח"
 ],
 "The SSH key $0 of $1 on $2 will be added to the $3 file of $4 on $5.": [
  null,
  "מפתח ה־SSH‏ $0 של $1 על גבי $2 יתווסף לקובץ $3 של $4 על גבי $5."
 ],
 "The SSH key $0 will be made available for the remainder of the session and will be available for login to other hosts as well.": [
  null,
  "מפתח ה־SSH‏ $0 יהפוך לזמין עד ליציאה מהמערכת ויהיה זמין לכניסה למארחים אחרים גם כן."
 ],
 "The SSH key for logging in to $0 is protected by a password, and the host does not allow logging in with a password. Please provide the password of the key at $1.": [
  null,
  "מפתח ה־SSH לכניסה אל $0 מוגן בסיסמה, והמארח לא מרשה להיכנס למערכת עם סיסמה. נא לספק את הסיסמה למפתח שב־$1."
 ],
 "The SSH key for logging in to $0 is protected. You can log in with either your login password or by providing the password of the key at $1.": [
  null,
  "מפתח ה־SSH לכניסה אל $0 מוגן. יש לך אפשרות להיכנס עם שם המשתמש והסיסמה שלך או על ידי אספקת הסיסמה למפתח שב־$1."
 ],
 "The fingerprint should match:": [
  null,
  "טביעת האצבע אמורה להיות תואמת:"
 ],
 "The key password can not be empty": [
  null,
  "סיסמת המפתח לא יכולה להישאר ריקה"
 ],
 "The key passwords do not match": [
  null,
  "סיסמאות המפתח אינן תואמות"
 ],
 "The logged in user is not permitted to view system modifications": [
  null,
  "המשתמש הנוכחי שנכנס למערכת אינו מורשה לצפות בשינויים שבוצעו במערכת"
 ],
 "The password can not be empty": [
  null,
  "סיסמת המפתח לא יכולה להישאר ריקה"
 ],
 "The resulting fingerprint is fine to share via public methods, including email.": [
  null,
  "זה בסדר לשתף את טביעת האצבע באופן ציבורי, לרבות בדוא״ל."
 ],
 "The resulting fingerprint is fine to share via public methods, including email. If you are asking someone else to do the verification for you, they can send the results using any method.": [
  null,
  ""
 ],
 "The server refused to authenticate '$0' using password authentication, and no other supported authentication methods are available.": [
  null,
  "השרת סירב לאמת את ‚$0’ עם אימות בסיסמה ואין שיטות אימות אחרות נתמכות."
 ],
 "The server refused to authenticate using any supported methods.": [
  null,
  "השרת סירב לאמת בעזרת השיטות הנתמכות."
 ],
 "The web browser configuration prevents Cockpit from running (inaccessible $0)": [
  null,
  "הגדרות הדפדפן מונעות מ־Cockpit לפעול ($0 בלתי נגיש)"
 ],
 "This tool configures the SELinux policy and can help with understanding and resolving policy violations.": [
  null,
  "הכלי הזה מגדיר את המדיניות של SELinux ויכול לסייע והבנת ופתרון הפרות של המדיניות."
 ],
 "This tool generates an archive of configuration and diagnostic information from the running system. The archive may be stored locally or centrally for recording or tracking purposes or may be sent to technical support representatives, developers or system administrators to assist with technical fault-finding and debugging.": [
  null,
  "כלי זה מייצר ארכיון של הגדרות ופרטי ניתוח של המערכת. אפשר לאחסן את הארכיון מקומית או באופן מרכזי למטרות תיעוד או מעקב או לנציגי תמיכה, מתכנתים או מנהלי מערכות כדי שיוכלו לסייע באיתור תקלות טכניות וניפוי שגיאות."
 ],
 "This tool manages local storage, such as filesystems, LVM2 volume groups, and NFS mounts.": [
  null,
  "כלי זה מנהל אחסון מקומי כגון מערכות קבצים, קבוצות כרכים ב־LVM2 ועיגונים של NFS."
 ],
 "This tool manages networking such as bonds, bridges, teams, VLANs and firewalls using NetworkManager and Firewalld. NetworkManager is incompatible with Ubuntu's default systemd-networkd and Debian's ifupdown scripts.": [
  null,
  ""
 ],
 "This web browser is too old to run the Web Console (missing $0)": [
  null,
  "דפדפן זה מיושן מכדי להריץ את המסוף לדפדפן (חסר $0)"
 ],
 "Time zone": [
  null,
  "אזור זמן"
 ],
 "To ensure that your connection is not intercepted by a malicious third-party, please verify the host key fingerprint:": [
  null,
  "כדי לוודא שהחיבור שלך לא מיורט על ידי גורמי צד־שלישי זדוניים, נא לאמת את טביעת האצבע של מפתח המארח:"
 ],
 "To verify a fingerprint, run the following on $0 while physically sitting at the machine or through a trusted network:": [
  null,
  "כדי לאמת את טביעת האצבע, יש להריץ את הפקודה הבאה על $0 במהלך ישיבה פיזית מול המכונה או דרך רשת מהימנה:"
 ],
 "Toggle date picker": [
  null,
  "החלפת מצב בורר תאריכים"
 ],
 "Too much data": [
  null,
  "יותר מדי נתונים"
 ],
 "Total size: $0": [
  null,
  "גודל כולל: $0"
 ],
 "Tower": [
  null,
  "מארז גבוה"
 ],
 "Transient packageless sessions require the same operating system and version, for compatibility reasons: $0.": [
  null,
  ""
 ],
 "Trust and add host": [
  null,
  "מתן אמון והוספת מארח"
 ],
 "Try again": [
  null,
  "לנסות שוב"
 ],
 "Trying to synchronize with $0": [
  null,
  "מתבצע ניסיון להסתנכרן מול $0"
 ],
 "Unable to connect to that address": [
  null,
  "לא ניתן להתחבר לכתובת הזו"
 ],
 "Unable to log in to $0. The host does not accept password login or any of your SSH keys.": [
  null,
  "לא ניתן להיכנס אל $0. המארח לא מקבל כניסה עם סיסמה או אף אחד ממפתחות ה־SSH האחרים שלך."
 ],
 "Unknown": [
  null,
  "לא ידוע"
 ],
 "Untrusted host": [
  null,
  "מארח בלתי מהימן"
 ],
 "User name": [
  null,
  "שם משתמש"
 ],
 "User name cannot be empty": [
  null,
  "שם המשתמש לא יכול להיות ריק"
 ],
 "Validating authentication token": [
  null,
  "אסימון האימות מתוקף"
 ],
 "View all logs": [
  null,
  "הצגת כל היומנים"
 ],
 "View automation script": [
  null,
  "הצגת סקריפט אוטומציה"
 ],
 "Visit firewall": [
  null,
  "ביקור בחומת האש"
 ],
 "Waiting for other software management operations to finish": [
  null,
  "בהמתנה לסיום פעולות ניהול תכנה אחרות"
 ],
 "Web Console for Linux servers": [
  null,
  "מסוף מקוון לשרתי לינוקס"
 ],
 "Wrong user name or password": [
  null,
  "שם משתמש או סיסמה שגויים"
 ],
 "You are connecting to $0 for the first time.": [
  null,
  "זאת ההתחברות הראשונה שלך אל $0."
 ],
 "Your browser does not allow paste from the context menu. You can use Shift+Insert.": [
  null,
  "הדפדפן שלך לא מרשה להדביק מתפריט ההקשר. אפשר להשתמש ב־Shift+Insert."
 ],
 "Your session has been terminated.": [
  null,
  "ההפעלה שלך הושמדה."
 ],
 "Your session has expired. Please log in again.": [
  null,
  "תוקף ההפעלה שלך פג. נא להיכנס שוב."
 ],
 "Zone": [
  null,
  "אזור"
 ],
 "[binary data]": [
  null,
  "[נתונים בינריים]"
 ],
 "[no data]": [
  null,
  "[אין נתונים]"
 ],
 "in less than a minute": [
  null,
  ""
 ],
 "less than a minute ago": [
  null,
  ""
 ],
 "password quality": [
  null,
  "איכות הסיסמה"
 ],
 "show less": [
  null,
  "להציג פחות"
 ],
 "show more": [
  null,
  "להציג יותר"
 ]
};
