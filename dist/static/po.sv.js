window.cockpit_po = {
 "": {
  "plural-forms": (n) => n != 1,
  "language": "sv",
  "language-direction": "ltr"
 },
 "$0 GiB": [
  null,
  "$0 GiB"
 ],
 "$0 day": [
  null,
  "$0 dag",
  "$0 dagar"
 ],
 "$0 error": [
  null,
  "$0 fel"
 ],
 "$0 exited with code $1": [
  null,
  "$0 avslutade med kod $1"
 ],
 "$0 failed": [
  null,
  "$0 misslyckades"
 ],
 "$0 hour": [
  null,
  "$0 timme",
  "$0 timmar"
 ],
 "$0 is not available from any repository.": [
  null,
  "$0 är inte tillgängligt från något förråd."
 ],
 "$0 key changed": [
  null,
  "$0 nyckel ändrad"
 ],
 "$0 killed with signal $1": [
  null,
  "$0 dödad med signal $1"
 ],
 "$0 minute": [
  null,
  "$0 minut",
  "$0 minuter"
 ],
 "$0 month": [
  null,
  "$0 månad",
  "$0 månader"
 ],
 "$0 week": [
  null,
  "$0 vecka",
  "$0 veckor"
 ],
 "$0 will be installed.": [
  null,
  "$0 kommer att installeras."
 ],
 "$0 year": [
  null,
  "$0 år",
  "$0 år"
 ],
 "1 day": [
  null,
  "1 dag"
 ],
 "1 hour": [
  null,
  "1 timme"
 ],
 "1 minute": [
  null,
  "1 minut"
 ],
 "1 week": [
  null,
  "1 vecka"
 ],
 "20 minutes": [
  null,
  "20 minuter"
 ],
 "40 minutes": [
  null,
  "40 minuter"
 ],
 "5 minutes": [
  null,
  "5 minuter"
 ],
 "6 hours": [
  null,
  "6 timmar"
 ],
 "60 minutes": [
  null,
  "60 minuter"
 ],
 "A compatible version of Cockpit is not installed on $0.": [
  null,
  "Ingen kompatibel version av Cockpit är installerad på $0."
 ],
 "A modern browser is required for security, reliability, and performance.": [
  null,
  "En modern webbläsare krävs för säkerhet, pålitlighet och prestanda."
 ],
 "A new SSH key at $0 will be created for $1 on $2 and it will be added to the $3 file of $4 on $5.": [
  null,
  "En ny SSH-nyckel på $0 kommer att skapas för $1 på $2 och den kommer att läggas till $3-filen på $4 på $5."
 ],
 "Absent": [
  null,
  "Frånvarande"
 ],
 "Accept key and log in": [
  null,
  "Acceptera nyckel och logga in"
 ],
 "Acceptable password": [
  null,
  "Acceptabelt lösenord"
 ],
 "Add $0": [
  null,
  "Lägg till $0"
 ],
 "Additional packages:": [
  null,
  "Ytterligare paket:"
 ],
 "Administration with Cockpit Web Console": [
  null,
  "Administration med Cockpits webbkonsol"
 ],
 "Advanced TCA": [
  null,
  "Avancerad TCA"
 ],
 "All-in-one": [
  null,
  "Allt i ett"
 ],
 "Ansible": [
  null,
  "Ansible"
 ],
 "Ansible roles documentation": [
  null,
  "Dokumentation för Ansibleroller"
 ],
 "Authentication": [
  null,
  "Autentisering"
 ],
 "Authentication failed": [
  null,
  "Autentisering misslyckades"
 ],
 "Authentication is required to perform privileged tasks with the Cockpit Web Console": [
  null,
  "Autentisering krävs för att utföra privilegierade uppgifter med Cockpits webbkonsol"
 ],
 "Authorize SSH key": [
  null,
  "Auktorisera SSH-nyckel"
 ],
 "Automatically using NTP": [
  null,
  "Använder automatiskt NTP"
 ],
 "Automatically using additional NTP servers": [
  null,
  "Använder automatiskt ytterligare NTP-servrar"
 ],
 "Automatically using specific NTP servers": [
  null,
  "Använder automatiskt specifika NTP-servrar"
 ],
 "Automation script": [
  null,
  "Automatiseringsskript"
 ],
 "Blade": [
  null,
  "Blad"
 ],
 "Blade enclosure": [
  null,
  "Bladhölje"
 ],
 "Bus expansion chassis": [
  null,
  "Bussexpansionschassi"
 ],
 "Bypass browser check": [
  null,
  "Förbigå webbläsarkontroll"
 ],
 "Cancel": [
  null,
  "Avbryt"
 ],
 "Cannot forward login credentials": [
  null,
  "Kan inte vidarebefordra inloggningsuppgifterna"
 ],
 "Cannot schedule event in the past": [
  null,
  "Kan inte schemalägga händelser som redan hänt"
 ],
 "Change": [
  null,
  "Ändra"
 ],
 "Change system time": [
  null,
  "Ändra systemtid"
 ],
 "Changed keys are often the result of an operating system reinstallation. However, an unexpected change may indicate a third-party attempt to intercept your connection.": [
  null,
  "Ändrade nycklar är ofta resultatet av en ominstallation av operativsystemet. En oväntad förändring kan dock indikera ett försök från tredje part att avlyssna din anslutning."
 ],
 "Checking installed software": [
  null,
  "Kontrollerar installerad programvara"
 ],
 "Close": [
  null,
  "Stäng"
 ],
 "Cockpit": [
  null,
  "Cockpit"
 ],
 "Cockpit authentication is configured incorrectly.": [
  null,
  "Cockpit-autentisering är felaktigt konfigurerad."
 ],
 "Cockpit configuration of NetworkManager and Firewalld": [
  null,
  "Cockpit konfiguration av NetworkManager och Firewalld"
 ],
 "Cockpit could not contact the given host.": [
  null,
  "Cockpit kunde inte kontakta den angivna värden."
 ],
 "Cockpit is a server manager that makes it easy to administer your Linux servers via a web browser. Jumping between the terminal and the web tool is no problem. A service started via Cockpit can be stopped via the terminal. Likewise, if an error occurs in the terminal, it can be seen in the Cockpit journal interface.": [
  null,
  "Cockpit är en serverhanterare som gör det lätt att administrera dina Linuxservrar via en webbläsare. Att hoppa mellan terminalen och webbverktyget är inget problem. En tjänst som startas via Cockpit kan stoppas via terminalen. Likaledes, om ett fel uppstår i terminalen kan det ses i Cockpits journalgränssnitt."
 ],
 "Cockpit is not compatible with the software on the system.": [
  null,
  "Cockpit är inte kompatibelt med programvaran på systemet."
 ],
 "Cockpit is not installed": [
  null,
  "Cockpit är inte installerat"
 ],
 "Cockpit is not installed on the system.": [
  null,
  "Cockpit är inte installerat på systemet."
 ],
 "Cockpit is perfect for new sysadmins, allowing them to easily perform simple tasks such as storage administration, inspecting journals and starting and stopping services. You can monitor and administer several servers at the same time. Just add them with a single click and your machines will look after its buddies.": [
  null,
  "Cockpit är perfekt för nya systemadministratörer, låter dem lätt utföra enkla uppgifter såsom lagringsadministration, inspektion av journaler och att starta och stoppa tjänster. Du kan övervaka och administrera flera servrar på samma gång. Lägg bara till dem med ett enda klick och dina maskiner kommer se efter sina kompisar."
 ],
 "Cockpit might not render correctly in your browser": [
  null,
  "Cockpit kanske inte återges korrekt i din webbläsare"
 ],
 "Collect and package diagnostic and support data": [
  null,
  "Samla och paketera diagnostik och support data"
 ],
 "Collect kernel crash dumps": [
  null,
  "Samla kärnkraschdumpar"
 ],
 "Compact PCI": [
  null,
  "Kompakt PCI"
 ],
 "Confirm key password": [
  null,
  "Bekräfta lösenord för nyckel"
 ],
 "Connect to": [
  null,
  "Anslut till"
 ],
 "Connect to:": [
  null,
  "Anslut till:"
 ],
 "Connection has timed out.": [
  null,
  "Anslutningens tidsgräns överskreds."
 ],
 "Convertible": [
  null,
  "Konvertibel"
 ],
 "Copied": [
  null,
  "Kopierade"
 ],
 "Copy": [
  null,
  "Kopiera"
 ],
 "Copy to clipboard": [
  null,
  "Kopiera till urklipp"
 ],
 "Create $0": [
  null,
  "Skapa $0"
 ],
 "Create a new SSH key and authorize it": [
  null,
  "Skapa en ny SSH-nyckel och auktorisera den"
 ],
 "Create new task file with this content.": [
  null,
  "Skapa en ny uppgiftsfil med detta innehåll."
 ],
 "Ctrl+Insert": [
  null,
  "Ctrl+Insert"
 ],
 "Delay": [
  null,
  "Fördröjning"
 ],
 "Desktop": [
  null,
  "Skrivbord"
 ],
 "Detachable": [
  null,
  "Frånkopplingsbar"
 ],
 "Diagnostic reports": [
  null,
  "Diagnostikrapporter"
 ],
 "Docking station": [
  null,
  "Dockningsstation"
 ],
 "Download a new browser for free": [
  null,
  "Hämta en ny webbläsare gratis"
 ],
 "Downloading $0": [
  null,
  "Hämtar $0"
 ],
 "Dual rank": [
  null,
  "Dubbelrad"
 ],
 "Embedded PC": [
  null,
  "Inbäddad PC"
 ],
 "Excellent password": [
  null,
  "Utmärkt lösenord"
 ],
 "Expansion chassis": [
  null,
  "Expansionschassin"
 ],
 "Failed to change password": [
  null,
  "Misslyckades att ändra lösenord"
 ],
 "Failed to enable $0 in firewalld": [
  null,
  "Misslyckades med att aktivera $0 i firewalld"
 ],
 "Go to now": [
  null,
  "Gå till nu"
 ],
 "Handheld": [
  null,
  "Handhållen"
 ],
 "Hide confirmation password": [
  null,
  "Dölj bekräftelselösenord"
 ],
 "Hide password": [
  null,
  "Dölj lösenord"
 ],
 "Host is unknown": [
  null,
  "Värd är okänd"
 ],
 "Host key is incorrect": [
  null,
  "Värdnyckeln är felaktig"
 ],
 "Hostkey does not match": [
  null,
  "Värdnyckeln matchar inte"
 ],
 "Hostkey is unknown": [
  null,
  "Värdnyckeln är okänd"
 ],
 "If the fingerprint matches, click \"Accept key and log in\". Otherwise, do not log in and contact your administrator.": [
  null,
  "Om fingeravtrycket stämmer, klicka på \"Acceptera nyckel och logga in\". Annars, logga inte in och kontakta din administratör."
 ],
 "If the fingerprint matches, click 'Trust and add host'. Otherwise, do not connect and contact your administrator.": [
  null,
  "Om fingeravtrycket stämmer, klicka på \"Lita på och lägg till värd\". Annars, anslut inte och kontakta din administratör."
 ],
 "Install": [
  null,
  "Installera"
 ],
 "Install software": [
  null,
  "Installera programvara"
 ],
 "Install the cockpit-system package (and optionally other cockpit packages) on $0 to enable web console access.": [
  null,
  "Installera cockpit-systempaketet (och eventuellt andra cockpitpaket) på $0 för att aktivera webbkonsolåtkomst."
 ],
 "Installing $0": [
  null,
  "Installerar $0"
 ],
 "Internal error": [
  null,
  "Internt fel"
 ],
 "Internal error: Invalid challenge header": [
  null,
  "Internt fel: felaktig utmaningshuvud"
 ],
 "Internal protocol error": [
  null,
  "Internt protokoll fel"
 ],
 "Invalid date format": [
  null,
  "Felaktigt datumformat"
 ],
 "Invalid date format and invalid time format": [
  null,
  "Felaktigt datumformat och felaktigt tidsformat"
 ],
 "Invalid file permissions": [
  null,
  "Felaktiga filrättigheter"
 ],
 "Invalid time format": [
  null,
  "Felaktigt tidsformat"
 ],
 "Invalid timezone": [
  null,
  "Felaktig tidszon"
 ],
 "IoT gateway": [
  null,
  "IoT-gateway"
 ],
 "Kernel dump": [
  null,
  "Kärndump"
 ],
 "Key password": [
  null,
  "Nyckellösenord"
 ],
 "Laptop": [
  null,
  "Bärbar dator"
 ],
 "Learn more": [
  null,
  "Mer information"
 ],
 "Loading system modifications...": [
  null,
  "Läser in anpassningar till systemet..."
 ],
 "Log in": [
  null,
  "Logga in"
 ],
 "Log in to $0": [
  null,
  "Logga in till $0"
 ],
 "Log in with your server user account.": [
  null,
  "Logga in med ditt serveranvändarkonto."
 ],
 "Log messages": [
  null,
  "Loggmeddelanden"
 ],
 "Login": [
  null,
  "Inloggning"
 ],
 "Login again": [
  null,
  "Logga in igen"
 ],
 "Login failed": [
  null,
  "Inloggningen misslyckades"
 ],
 "Logout successful": [
  null,
  "Utloggningen lyckades"
 ],
 "Low profile desktop": [
  null,
  "Lågprofilskrivbord"
 ],
 "Lunch box": [
  null,
  "Lunchlåda"
 ],
 "Main server chassis": [
  null,
  "Huvudserverchassi"
 ],
 "Manage storage": [
  null,
  "Hantera lagring"
 ],
 "Manually": [
  null,
  "Manuellt"
 ],
 "Message to logged in users": [
  null,
  "Meddelande till inloggade användare"
 ],
 "Mini PC": [
  null,
  "Mini-PC"
 ],
 "Mini tower": [
  null,
  "Minitorn"
 ],
 "Multi-system chassis": [
  null,
  "Multisystemschassi"
 ],
 "NTP server": [
  null,
  "NTP-server"
 ],
 "Need at least one NTP server": [
  null,
  "Behöver åtminstone en NTP-server"
 ],
 "Networking": [
  null,
  "Nätverk"
 ],
 "New host": [
  null,
  "Ny värd"
 ],
 "New password was not accepted": [
  null,
  "Det nya lösenordet godtogs inte"
 ],
 "No delay": [
  null,
  "Ingen fördröjning"
 ],
 "No results found": [
  null,
  "Inga resultat funna"
 ],
 "No such file or directory": [
  null,
  "Filen eller katalogen finns inte"
 ],
 "No system modifications": [
  null,
  "Inga systemändringar"
 ],
 "Not a valid private key": [
  null,
  "Inte en giltig privat nyckel"
 ],
 "Not permitted to perform this action.": [
  null,
  "Inte tillåtet att utföra denna åtgärd."
 ],
 "Not synchronized": [
  null,
  "Inte synkroniserad"
 ],
 "Notebook": [
  null,
  "Bärbar (notebook)"
 ],
 "Occurrences": [
  null,
  "Förekomster"
 ],
 "Ok": [
  null,
  "Ok"
 ],
 "Old password not accepted": [
  null,
  "Det gamla lösenordet accepterades inte"
 ],
 "Once Cockpit is installed, enable it with \"systemctl enable --now cockpit.socket\".": [
  null,
  "När Cockpit är installerat, aktivera det med ”systemctl enable --now cockpit.socket”."
 ],
 "Or use a bundled browser": [
  null,
  "Eller använd en medpackad bläddrare"
 ],
 "Other": [
  null,
  "Annan"
 ],
 "Other options": [
  null,
  "Andra alternativ"
 ],
 "PackageKit crashed": [
  null,
  "PackageKit kraschade"
 ],
 "Packageless session unavailable": [
  null,
  "Paketlös session är inte tillgänglig"
 ],
 "Password": [
  null,
  "Lösenord"
 ],
 "Password is not acceptable": [
  null,
  "Lösenordet är inte godtagbart"
 ],
 "Password is too weak": [
  null,
  "Lösenordet är för svagt"
 ],
 "Password not accepted": [
  null,
  "Lösenordet accepterades inte"
 ],
 "Paste": [
  null,
  "Klistra in"
 ],
 "Paste error": [
  null,
  "Klistra in fel"
 ],
 "Path to file": [
  null,
  "Sökväg till filen"
 ],
 "Peripheral chassis": [
  null,
  "Periferichassi"
 ],
 "Permission denied": [
  null,
  "Åtkomst nekas"
 ],
 "Pick date": [
  null,
  "Välj datum"
 ],
 "Pizza box": [
  null,
  "Pizzalåda"
 ],
 "Please enable JavaScript to use the Web Console.": [
  null,
  "Aktivera JavaScript för att använda webbkonsolen."
 ],
 "Please specify the host to connect to": [
  null,
  "Ange värden att ansluta till"
 ],
 "Portable": [
  null,
  "Bärbar"
 ],
 "Present": [
  null,
  "Närvarande"
 ],
 "Prompting via ssh-add timed out": [
  null,
  "Tidsgränsen överskreds vid fråga via ssh-add"
 ],
 "Prompting via ssh-keygen timed out": [
  null,
  "Tidsgränsen överskreds vid fråga via ssh-keygen"
 ],
 "RAID chassis": [
  null,
  "RAID-chassi"
 ],
 "Rack mount chassis": [
  null,
  "Rackmonteringschassi"
 ],
 "Reboot": [
  null,
  "Starta om"
 ],
 "Recent hosts": [
  null,
  "Senaste värdar"
 ],
 "Refusing to connect": [
  null,
  "Vägrar att ansluta"
 ],
 "Removals:": [
  null,
  "Borttagningar:"
 ],
 "Remove host": [
  null,
  "Ta bort värd"
 ],
 "Removing $0": [
  null,
  "Tar bort $0"
 ],
 "Row expansion": [
  null,
  "Radexpansion"
 ],
 "Row select": [
  null,
  "Radval"
 ],
 "Run this command over a trusted network or physically on the remote machine:": [
  null,
  "Kör det här kommandot över ett pålitligt nätverk eller fysiskt på fjärrdatorn:"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "SSH key": [
  null,
  "SSH-nyckel"
 ],
 "SSH key login": [
  null,
  "SSH-nyckel inloggning"
 ],
 "Sealed-case PC": [
  null,
  "PC med slutet hölje"
 ],
 "Security Enhanced Linux configuration and troubleshooting": [
  null,
  "Säkerhetsförbättrad Linux-konfiguration och felsökning"
 ],
 "Server": [
  null,
  "Server"
 ],
 "Server closed connection": [
  null,
  "Servern stängde anslutningen"
 ],
 "Server has closed the connection.": [
  null,
  "Servern har stängt förbindelsen."
 ],
 "Set time": [
  null,
  "Ställ in tiden"
 ],
 "Shell script": [
  null,
  "Skalskript"
 ],
 "Shift+Insert": [
  null,
  "Skift+Insert"
 ],
 "Show confirmation password": [
  null,
  "Visa bekräftelselösenord"
 ],
 "Show password": [
  null,
  "Visa lösenord"
 ],
 "Shut down": [
  null,
  "Stäng av"
 ],
 "Single rank": [
  null,
  "Ensam ordning"
 ],
 "Space-saving computer": [
  null,
  "Utrymmessparande dator"
 ],
 "Specific time": [
  null,
  "Specifik tid"
 ],
 "Stick PC": [
  null,
  "Pinndator"
 ],
 "Storage": [
  null,
  "Lagring"
 ],
 "Strong password": [
  null,
  "Starkt lösenord"
 ],
 "Sub-Chassis": [
  null,
  "Under-chassi"
 ],
 "Sub-Notebook": [
  null,
  "ULPC"
 ],
 "Synchronized": [
  null,
  "Synkroniserad"
 ],
 "Synchronized with $0": [
  null,
  "Synkroniserad med $0"
 ],
 "Synchronizing": [
  null,
  "Synkroniserar"
 ],
 "Tablet": [
  null,
  "Platta"
 ],
 "The SSH key $0 of $1 on $2 will be added to the $3 file of $4 on $5.": [
  null,
  "SSH-nyckeln $0 av $1 på $2 kommer att läggas till $3-filen på $4 på $5."
 ],
 "The SSH key $0 will be made available for the remainder of the session and will be available for login to other hosts as well.": [
  null,
  "SSH-nyckeln $0 kommer att göras tillgänglig för resten av sessionen och kommer att vara tillgänglig för inloggning även för andra värdar."
 ],
 "The SSH key for logging in to $0 is protected by a password, and the host does not allow logging in with a password. Please provide the password of the key at $1.": [
  null,
  "SSH-nyckeln för att logga in på $0 är skyddad av ett lösenord, och värden tillåter inte inloggning med ett lösenord. Vänligen ange lösenordet för nyckeln på $1."
 ],
 "The SSH key for logging in to $0 is protected. You can log in with either your login password or by providing the password of the key at $1.": [
  null,
  "SSH-nyckeln för att logga in på $0 är skyddad. Du kan logga in med antingen ditt inloggningslösenord eller genom att ange lösenordet för nyckeln för $1."
 ],
 "The fingerprint should match:": [
  null,
  "Fingeravtrycket ska matcha:"
 ],
 "The key password can not be empty": [
  null,
  "Nyckellösenordet får inte vara tomt"
 ],
 "The key passwords do not match": [
  null,
  "Nyckellösenorden stämmer inte överens"
 ],
 "The logged in user is not permitted to view system modifications": [
  null,
  "Den inloggade användaren har inte tillåtelse att se systemändringar"
 ],
 "The password can not be empty": [
  null,
  "Lösenordet får inte vara tomt"
 ],
 "The resulting fingerprint is fine to share via public methods, including email.": [
  null,
  "Det resulterande fingeravtrycket går bra att dela via offentliga metoder, inklusive e-post."
 ],
 "The resulting fingerprint is fine to share via public methods, including email. If you are asking someone else to do the verification for you, they can send the results using any method.": [
  null,
  "Det resulterande fingeravtrycket är okej att dela via offentliga metoder, inklusive e-post. Om du ber någon annan att göra verifieringen åt dig kan de skicka resultaten med vilken metod som helst."
 ],
 "The server refused to authenticate '$0' using password authentication, and no other supported authentication methods are available.": [
  null,
  "Servern vägrade att autentisera ”$0” med lösenordsautentisering, och inga andra stödda autentiseringsmetoder är tillgängliga."
 ],
 "The server refused to authenticate using any supported methods.": [
  null,
  "Servern vägrade att autentisera med några stödda metoder."
 ],
 "The web browser configuration prevents Cockpit from running (inaccessible $0)": [
  null,
  "Webbläsarens konfiguration förhindrar Cockpit från att köra (oåtkomlig $0)"
 ],
 "This tool configures the SELinux policy and can help with understanding and resolving policy violations.": [
  null,
  "Det här verktyget konfigurerar SELinux-policyn och kan hjälpa till med att förstå och lösa policy överträdelser."
 ],
 "This tool configures the system to write kernel crash dumps. It supports the \"local\" (disk), \"ssh\", and \"nfs\" dump targets.": [
  null,
  "Det här verktyget konfigurerar systemet till att kunna skriva kärnkraschdumpar till disken. Det stödjer \"lokala\" (disk), \"ssh\" och \"nfs\" dumpmål."
 ],
 "This tool generates an archive of configuration and diagnostic information from the running system. The archive may be stored locally or centrally for recording or tracking purposes or may be sent to technical support representatives, developers or system administrators to assist with technical fault-finding and debugging.": [
  null,
  "Detta verktyg genererar ett arkiv med konfiguration och diagnostisk information från det körande systemet. Arkivet kan förvaras lokalt eller centralt för inspelning eller spårningsändamål eller kan skickas till tekniska supportrepresentanter, utvecklare eller systemadministratörer för att hjälpa till med teknisk felspårning och felsökning."
 ],
 "This tool manages local storage, such as filesystems, LVM2 volume groups, and NFS mounts.": [
  null,
  "Detta verktyg hanterar lokal lagring, såsom filsystem, LVM2-volymgrupper och NFS-monteringar."
 ],
 "This tool manages networking such as bonds, bridges, teams, VLANs and firewalls using NetworkManager and Firewalld. NetworkManager is incompatible with Ubuntu's default systemd-networkd and Debian's ifupdown scripts.": [
  null,
  "Detta verktyg hanterar nätverk som bindningar, broar, team, VLAN och brandväggar med NetworkManager och Firewalld. NetworkManager är inkompatibelt med Ubuntus standard systemd-networkd och Debians ifupdown skript."
 ],
 "This web browser is too old to run the Web Console (missing $0)": [
  null,
  "Den här webbläsaren är för gammal för att köra webbkonsolen ($0 saknas)"
 ],
 "Time zone": [
  null,
  "Tidszon"
 ],
 "To ensure that your connection is not intercepted by a malicious third-party, please verify the host key fingerprint:": [
  null,
  "För att säkerställa att din anslutning inte fångas upp av en skadlig tredje part, vänligen verifiera värdnyckelns fingeravtryck:"
 ],
 "To verify a fingerprint, run the following on $0 while physically sitting at the machine or through a trusted network:": [
  null,
  "För att verifiera ett fingeravtryck, kör följande på $0 medan du fysiskt sitter vid maskinen eller genom ett pålitligt nätverk:"
 ],
 "Toggle date picker": [
  null,
  "Växla datumväljare"
 ],
 "Too much data": [
  null,
  "För mycket data"
 ],
 "Total size: $0": [
  null,
  "Total storlek: $0"
 ],
 "Tower": [
  null,
  "Torn"
 ],
 "Transient packageless sessions require the same operating system and version, for compatibility reasons: $0.": [
  null,
  "Transienta paketlösa sessioner kräver samma operativsystem och version av kompatibilitetsskäl: $0."
 ],
 "Trust and add host": [
  null,
  "Lita på och lägg till värd"
 ],
 "Try again": [
  null,
  "Försök igen"
 ],
 "Trying to synchronize with $0": [
  null,
  "Försök att synkronisera med $0"
 ],
 "Unable to connect to that address": [
  null,
  "Kan inte ansluta till den adressen"
 ],
 "Unable to log in to $0 using SSH key authentication. Please provide the password.": [
  null,
  "Det går inte att logga in på $0 med SSH-nyckelautentisering. Ange lösenordet."
 ],
 "Unable to log in to $0. The host does not accept password login or any of your SSH keys.": [
  null,
  "Det går inte att logga in på $0. Värden accepterar inte lösenordsinloggning eller någon av dina SSH-nycklar."
 ],
 "Unknown": [
  null,
  "Okänd"
 ],
 "Unknown host: $0": [
  null,
  "Okänd värd: $0"
 ],
 "Untrusted host": [
  null,
  "Ej betrodd värd"
 ],
 "User name": [
  null,
  "Användarnamn"
 ],
 "User name cannot be empty": [
  null,
  "Användarnamnet får inte vara tomt"
 ],
 "Validating authentication token": [
  null,
  "Validerar autentiseringselementet"
 ],
 "Verify fingerprint": [
  null,
  "Verifiera fingeravtryck"
 ],
 "View all logs": [
  null,
  "Visa alla loggar"
 ],
 "View automation script": [
  null,
  "Visa automatiseringsskript"
 ],
 "Visit firewall": [
  null,
  "Besök brandvägg"
 ],
 "Waiting for other software management operations to finish": [
  null,
  "Väntar på att andra programvaruhanteringsåtgärder skall bli klara"
 ],
 "Weak password": [
  null,
  "Svagt lösenord"
 ],
 "Web Console for Linux servers": [
  null,
  "Webbkonsol för Linuxservrar"
 ],
 "Wrong user name or password": [
  null,
  "Fel användarnamn eller lösenord"
 ],
 "You are connecting to $0 for the first time.": [
  null,
  "Du ansluter till $0 för första gången."
 ],
 "Your browser does not allow paste from the context menu. You can use Shift+Insert.": [
  null,
  "Din webbläsare tillåter inte att klistra in från sammanhangsmenyn. Du kan använda Skift+Insert."
 ],
 "Your session has been terminated.": [
  null,
  "Din session har avslutats."
 ],
 "Your session has expired. Please log in again.": [
  null,
  "Din session har gått ut. Logga in igen."
 ],
 "Zone": [
  null,
  "Zon"
 ],
 "[binary data]": [
  null,
  "[binärdata]"
 ],
 "[no data]": [
  null,
  "[inga data]"
 ],
 "in less than a minute": [
  null,
  "i mindre än en minut"
 ],
 "less than a minute ago": [
  null,
  "mindre än en minut sedan"
 ],
 "password quality": [
  null,
  "lösenordskvalitet"
 ],
 "show less": [
  null,
  "visa mindre"
 ],
 "show more": [
  null,
  "visa mer"
 ]
};
