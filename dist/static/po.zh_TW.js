window.cockpit_po = {
 "": {
  "plural-forms": (n) => 0,
  "language": "zh_TW",
  "language-direction": "ltr"
 },
 "$0 GiB": [
  null,
  "$0 GiB"
 ],
 "$0 day": [
  null,
  "$0 天"
 ],
 "$0 error": [
  null,
  "$0 錯誤"
 ],
 "$0 exited with code $1": [
  null,
  "$0 結束執行並回傳 $1"
 ],
 "$0 failed": [
  null,
  "$0 失敗"
 ],
 "$0 hour": [
  null,
  "$0 小時"
 ],
 "$0 is not available from any repository.": [
  null,
  "$0 在任何套件庫中都未提供。"
 ],
 "$0 key changed": [
  null,
  "金鑰 $0 已被更改"
 ],
 "$0 killed with signal $1": [
  null,
  "$0 被 $1 訊號終止執行"
 ],
 "$0 minute": [
  null,
  "$0 分鐘"
 ],
 "$0 month": [
  null,
  "$0 月"
 ],
 "$0 week": [
  null,
  "$0 週"
 ],
 "$0 will be installed.": [
  null,
  "$0 將會被安裝。"
 ],
 "$0 year": [
  null,
  "$0 年"
 ],
 "1 day": [
  null,
  "1 天"
 ],
 "1 hour": [
  null,
  "1 小時"
 ],
 "1 minute": [
  null,
  "1 分鐘"
 ],
 "1 week": [
  null,
  "1週"
 ],
 "20 minutes": [
  null,
  "20 分鐘"
 ],
 "40 minutes": [
  null,
  "40 分鐘"
 ],
 "5 minutes": [
  null,
  "5 分鐘"
 ],
 "6 hours": [
  null,
  "6 小時"
 ],
 "60 minutes": [
  null,
  "60 分鐘"
 ],
 "A compatible version of Cockpit is not installed on $0.": [
  null,
  "未在 $0 上安裝相容的 Cockpit 版本。"
 ],
 "A modern browser is required for security, reliability, and performance.": [
  null,
  "需要現代瀏覽器以確保安全、可靠與效能。"
 ],
 "A new SSH key at $0 will be created for $1 on $2 and it will be added to the $3 file of $4 on $5.": [
  null,
  "將為了在 $2 上的 $1 建立一個於 $0 的新 SSH 金鑰，且其將會被加到 $5 上 $4 的 $3 檔案中。"
 ],
 "Absent": [
  null,
  "不存在"
 ],
 "Accept key and log in": [
  null,
  "接受金鑰並登入"
 ],
 "Acceptable password": [
  null,
  "可接受的密碼強度"
 ],
 "Add $0": [
  null,
  "新增 $0"
 ],
 "Additional packages:": [
  null,
  "額外套件："
 ],
 "Administration with Cockpit Web Console": [
  null,
  "使用 Cockpit 網頁控制台進行管理"
 ],
 "Advanced TCA": [
  null,
  "進階 TCA"
 ],
 "All-in-one": [
  null,
  "一體成型電腦"
 ],
 "Ansible": [
  null,
  "Ansible"
 ],
 "Ansible roles documentation": [
  null,
  "Ansible 角色文件"
 ],
 "Authentication": [
  null,
  "核對"
 ],
 "Authentication failed": [
  null,
  "認證失敗"
 ],
 "Authentication is required to perform privileged tasks with the Cockpit Web Console": [
  null,
  "需要驗證才可以透過 Cockpit 網頁控制台執行高權限的工作"
 ],
 "Authorize SSH key": [
  null,
  "授權 SSH 金鑰"
 ],
 "Automatically using NTP": [
  null,
  "自動使用 NTP"
 ],
 "Automatically using additional NTP servers": [
  null,
  "自動使用額外的 NTP 伺服器"
 ],
 "Automatically using specific NTP servers": [
  null,
  "自動使用指定的 NTP 伺服器"
 ],
 "Automation script": [
  null,
  "自動化腳本"
 ],
 "Blade": [
  null,
  "刀鋒"
 ],
 "Blade enclosure": [
  null,
  "刀鋒外殼"
 ],
 "Bus expansion chassis": [
  null,
  "匯流排擴充機殼"
 ],
 "Bypass browser check": [
  null,
  "繞過瀏覽器檢查"
 ],
 "Cancel": [
  null,
  "取消"
 ],
 "Cannot forward login credentials": [
  null,
  "無法轉發登入憑證"
 ],
 "Cannot schedule event in the past": [
  null,
  "無法安排過去的活動"
 ],
 "Change": [
  null,
  "變更"
 ],
 "Change system time": [
  null,
  "變更系統時間"
 ],
 "Changed keys are often the result of an operating system reinstallation. However, an unexpected change may indicate a third-party attempt to intercept your connection.": [
  null,
  "重新安裝作業系統通常會導致金鑰變更。然而，未預期的變更可能是第三方嘗試竊聽連線的跡象。"
 ],
 "Checking installed software": [
  null,
  "正在檢查已安裝的軟體"
 ],
 "Close": [
  null,
  "關閉"
 ],
 "Cockpit": [
  null,
  "Cockpit"
 ],
 "Cockpit authentication is configured incorrectly.": [
  null,
  "Cockpit 驗證設定不正確。"
 ],
 "Cockpit configuration of NetworkManager and Firewalld": [
  null,
  "Cockpit 的 NetworkManager 與 Firewalld 設定"
 ],
 "Cockpit could not contact the given host.": [
  null,
  "Cockpit 無法聯絡指定的主機。"
 ],
 "Cockpit is a server manager that makes it easy to administer your Linux servers via a web browser. Jumping between the terminal and the web tool is no problem. A service started via Cockpit can be stopped via the terminal. Likewise, if an error occurs in the terminal, it can be seen in the Cockpit journal interface.": [
  null,
  "Cockpit 是一個伺服器管理器，它可以讓您透過網頁瀏覽器輕鬆管理 Linux 伺服器。同時運用終端機與網頁工具是沒有問題的。由 Cockpit 啟動的服務可在終端機中被停止。同樣地，若終端機中發生錯誤時，也可以在 Cockpit 的紀錄檔日誌介面中看到。"
 ],
 "Cockpit is not compatible with the software on the system.": [
  null,
  "Cockpit 與該系統上的軟體不相容。"
 ],
 "Cockpit is not installed": [
  null,
  "Cockpit 未安裝"
 ],
 "Cockpit is not installed on the system.": [
  null,
  "Cockpit 未安裝在系統上。"
 ],
 "Cockpit is perfect for new sysadmins, allowing them to easily perform simple tasks such as storage administration, inspecting journals and starting and stopping services. You can monitor and administer several servers at the same time. Just add them with a single click and your machines will look after its buddies.": [
  null,
  "Cockpit 非常適合新的系統管理員，使他們能夠輕鬆執行簡單的任務，如：儲存空間管理、檢查日誌紀錄及啟動與停止服務。您可以同時監控和管理多個伺服器。只需輕鬆的新增它們，您的機器們將會關心彼此的狀況。"
 ],
 "Cockpit might not render correctly in your browser": [
  null,
  "Cockpit 可能在您的瀏覽器上無法正確呈現"
 ],
 "Collect and package diagnostic and support data": [
  null,
  "收集並封裝診斷與支援資料"
 ],
 "Collect kernel crash dumps": [
  null,
  "蒐集核心當機傾印"
 ],
 "Compact PCI": [
  null,
  "緊密型 PCI"
 ],
 "Confirm key password": [
  null,
  "確認金鑰密碼"
 ],
 "Connect to": [
  null,
  "連線至"
 ],
 "Connect to:": [
  null,
  "連線至："
 ],
 "Connection has timed out.": [
  null,
  "連線已逾時。"
 ],
 "Convertible": [
  null,
  "二合一電腦"
 ],
 "Copied": [
  null,
  "已複製"
 ],
 "Copy": [
  null,
  "複製"
 ],
 "Copy to clipboard": [
  null,
  "複製到剪貼簿"
 ],
 "Create a new SSH key and authorize it": [
  null,
  "建立新的 SSH 金鑰並對其授權"
 ],
 "Create new task file with this content.": [
  null,
  "以此內容建立新的工作檔案。"
 ],
 "Ctrl+Insert": [
  null,
  "Ctrl+Insert"
 ],
 "Delay": [
  null,
  "延遲"
 ],
 "Desktop": [
  null,
  "桌面環境"
 ],
 "Detachable": [
  null,
  "可拆開"
 ],
 "Diagnostic reports": [
  null,
  "診斷報告"
 ],
 "Docking station": [
  null,
  "擴充基座"
 ],
 "Download a new browser for free": [
  null,
  "免費下載新的瀏覽器"
 ],
 "Downloading $0": [
  null,
  "下載 $0"
 ],
 "Dual rank": [
  null,
  "雙通道"
 ],
 "Embedded PC": [
  null,
  "嵌入式PC"
 ],
 "Excellent password": [
  null,
  "優秀的密碼"
 ],
 "Expansion chassis": [
  null,
  "擴充機殼"
 ],
 "Failed to change password": [
  null,
  "無法更改密碼"
 ],
 "Failed to enable $0 in firewalld": [
  null,
  "啟用 firewalld 中的 $0 失敗"
 ],
 "Go to now": [
  null,
  "移至現在"
 ],
 "Handheld": [
  null,
  "手持裝置"
 ],
 "Hide confirmation password": [
  null,
  "隱藏確認密碼"
 ],
 "Hide password": [
  null,
  "隱藏密碼"
 ],
 "Host is unknown": [
  null,
  "主機未知"
 ],
 "Host key is incorrect": [
  null,
  "主機金鑰不正確"
 ],
 "Hostkey does not match": [
  null,
  "主機金鑰不相符"
 ],
 "Hostkey is unknown": [
  null,
  "主機金鑰未知"
 ],
 "If the fingerprint matches, click \"Accept key and log in\". Otherwise, do not log in and contact your administrator.": [
  null,
  "如果指紋相符，點選「接受金鑰並登入」。否則，請勿登入並聯絡您的管理人員。"
 ],
 "If the fingerprint matches, click 'Trust and add host'. Otherwise, do not connect and contact your administrator.": [
  null,
  "如果指紋相符，點選「信任並新增主機」。否則，不要連線並聯絡您的管理人員。"
 ],
 "Install": [
  null,
  "安裝"
 ],
 "Install software": [
  null,
  "安裝軟體"
 ],
 "Install the cockpit-system package (and optionally other cockpit packages) on $0 to enable web console access.": [
  null,
  "在 $0 上安裝 cockpit-system 套件 (與其他選擇性的 cockpit 套件) 以使用網頁控制台存取功能。"
 ],
 "Installing $0": [
  null,
  "正在安裝 $0"
 ],
 "Internal error": [
  null,
  "內部錯誤"
 ],
 "Internal error: Invalid challenge header": [
  null,
  "內部錯誤：無效的質詢標頭"
 ],
 "Internal protocol error": [
  null,
  "內部協定錯誤"
 ],
 "Invalid date format": [
  null,
  "日期格式無效"
 ],
 "Invalid date format and invalid time format": [
  null,
  "無效的日期與時間格式"
 ],
 "Invalid file permissions": [
  null,
  "檔案權限無效"
 ],
 "Invalid time format": [
  null,
  "時間格式無效"
 ],
 "Invalid timezone": [
  null,
  "無效的時區"
 ],
 "IoT gateway": [
  null,
  "物聯網閘道"
 ],
 "Kernel dump": [
  null,
  "核心傾印"
 ],
 "Key password": [
  null,
  "金鑰密碼"
 ],
 "Laptop": [
  null,
  "筆記型電腦"
 ],
 "Learn more": [
  null,
  "了解更多"
 ],
 "Loading system modifications...": [
  null,
  "讀取系統變更中…"
 ],
 "Log in": [
  null,
  "登入"
 ],
 "Log in to $0": [
  null,
  "登入至 $0"
 ],
 "Log in with your server user account.": [
  null,
  "使用您的伺服器使用者帳號登入。"
 ],
 "Log messages": [
  null,
  "記錄訊息"
 ],
 "Login": [
  null,
  "登入"
 ],
 "Login again": [
  null,
  "再次登入"
 ],
 "Login failed": [
  null,
  "登入失敗"
 ],
 "Logout successful": [
  null,
  "登出成功"
 ],
 "Low profile desktop": [
  null,
  "尺寸較小的桌上型電腦"
 ],
 "Lunch box": [
  null,
  "午餐盒"
 ],
 "Main server chassis": [
  null,
  "主伺服器機殼"
 ],
 "Manage storage": [
  null,
  "管理儲存空間"
 ],
 "Manually": [
  null,
  "手動"
 ],
 "Message to logged in users": [
  null,
  "給已登入使用者的訊息"
 ],
 "Mini PC": [
  null,
  "迷你電腦"
 ],
 "Mini tower": [
  null,
  "迷你塔"
 ],
 "Multi-system chassis": [
  null,
  "多系統機殼"
 ],
 "NTP server": [
  null,
  "NTP 伺服器"
 ],
 "Need at least one NTP server": [
  null,
  "至少需要一台 NTP 伺服器"
 ],
 "Networking": [
  null,
  "網路"
 ],
 "New host": [
  null,
  "新主機"
 ],
 "New password was not accepted": [
  null,
  "不接受新密碼"
 ],
 "No delay": [
  null,
  "沒有延遲"
 ],
 "No results found": [
  null,
  "沒有找到結果"
 ],
 "No such file or directory": [
  null,
  "無此檔案或目錄"
 ],
 "No system modifications": [
  null,
  "沒有系統異動"
 ],
 "Not a valid private key": [
  null,
  "不是有效的私鑰"
 ],
 "Not permitted to perform this action.": [
  null,
  "不允許執行此操作。"
 ],
 "Not synchronized": [
  null,
  "不同步"
 ],
 "Notebook": [
  null,
  "筆記型"
 ],
 "Occurrences": [
  null,
  "發生次數"
 ],
 "Ok": [
  null,
  "確定"
 ],
 "Old password not accepted": [
  null,
  "舊密碼不被接受"
 ],
 "Once Cockpit is installed, enable it with \"systemctl enable --now cockpit.socket\".": [
  null,
  "安裝Cockpit後，使用“systemctl enable --now cockpit.socket”啟用它。"
 ],
 "Or use a bundled browser": [
  null,
  "或使用隨附的瀏覽器"
 ],
 "Other": [
  null,
  "其它"
 ],
 "Other options": [
  null,
  "其他選擇"
 ],
 "PackageKit crashed": [
  null,
  "PackageKit 當掉了"
 ],
 "Packageless session unavailable": [
  null,
  "未提供無須套件的工作階段"
 ],
 "Password": [
  null,
  "密碼"
 ],
 "Password is not acceptable": [
  null,
  "密碼是不可接受的"
 ],
 "Password is too weak": [
  null,
  "密碼太弱了"
 ],
 "Password not accepted": [
  null,
  "密碼不被接受"
 ],
 "Paste": [
  null,
  "貼上"
 ],
 "Paste error": [
  null,
  "貼上時發生錯誤"
 ],
 "Path to file": [
  null,
  "檔案路徑"
 ],
 "Peripheral chassis": [
  null,
  "週邊設備機殼"
 ],
 "Permission denied": [
  null,
  "使用權限被拒"
 ],
 "Pick date": [
  null,
  "選擇日期"
 ],
 "Pizza box": [
  null,
  "披薩盒"
 ],
 "Please enable JavaScript to use the Web Console.": [
  null,
  "請啟用 JavaScript 以使用網頁控制台。"
 ],
 "Please specify the host to connect to": [
  null,
  "請指定要連線的主機"
 ],
 "Portable": [
  null,
  "手提"
 ],
 "Present": [
  null,
  "存在"
 ],
 "Prompting via ssh-add timed out": [
  null,
  "以 ssh-add 提示輸入已逾時"
 ],
 "Prompting via ssh-keygen timed out": [
  null,
  "以 ssh-keygen 提示輸入已逾時"
 ],
 "RAID chassis": [
  null,
  "RAID 機殼"
 ],
 "Rack mount chassis": [
  null,
  "機架式機殼"
 ],
 "Reboot": [
  null,
  "重新開機"
 ],
 "Recent hosts": [
  null,
  "最近的主機"
 ],
 "Refusing to connect": [
  null,
  "拒絕連線"
 ],
 "Removals:": [
  null,
  "移除："
 ],
 "Remove host": [
  null,
  "移除主機"
 ],
 "Removing $0": [
  null,
  "正在移除 $0"
 ],
 "Row expansion": [
  null,
  "展開橫列"
 ],
 "Row select": [
  null,
  "選擇列"
 ],
 "Run this command over a trusted network or physically on the remote machine:": [
  null,
  "由受信任的網路或親自到場於遠端機器上執行此指令："
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "SSH key": [
  null,
  "SSH 金鑰"
 ],
 "SSH key login": [
  null,
  "SSH 金鑰登入"
 ],
 "Sealed-case PC": [
  null,
  "密封式PC"
 ],
 "Security Enhanced Linux configuration and troubleshooting": [
  null,
  "Security Enhanced Linux 設定與疑難排解"
 ],
 "Server": [
  null,
  "伺服器"
 ],
 "Server closed connection": [
  null,
  "伺服器已關閉連線"
 ],
 "Server has closed the connection.": [
  null,
  "伺服器已關閉連線。"
 ],
 "Set time": [
  null,
  "設定時間"
 ],
 "Shell script": [
  null,
  "Shell script"
 ],
 "Shift+Insert": [
  null,
  "Shift+Insert"
 ],
 "Show confirmation password": [
  null,
  "顯示確認密碼"
 ],
 "Show password": [
  null,
  "顯示密碼"
 ],
 "Shut down": [
  null,
  "關機"
 ],
 "Single rank": [
  null,
  "單 rank"
 ],
 "Space-saving computer": [
  null,
  "節省空間的計算機"
 ],
 "Specific time": [
  null,
  "特定的時間"
 ],
 "Stick PC": [
  null,
  "堅持使用PC"
 ],
 "Storage": [
  null,
  "儲存裝置"
 ],
 "Strong password": [
  null,
  "強密碼"
 ],
 "Sub-Chassis": [
  null,
  "子機殼"
 ],
 "Sub-Notebook": [
  null,
  "小筆電"
 ],
 "Synchronized": [
  null,
  "同步"
 ],
 "Synchronized with $0": [
  null,
  "已與 $0 同步"
 ],
 "Synchronizing": [
  null,
  "同步中"
 ],
 "Tablet": [
  null,
  "平板電腦"
 ],
 "The SSH key $0 of $1 on $2 will be added to the $3 file of $4 on $5.": [
  null,
  "$2 上 $1 的 SSH 金鑰 $0 將會被新增至 $5 上 $4 的 $3 檔案中。"
 ],
 "The SSH key $0 will be made available for the remainder of the session and will be available for login to other hosts as well.": [
  null,
  "SSH 金鑰 $0 將會於剩餘的工作階段可供使用，也將會可用於登入其他主機。"
 ],
 "The SSH key for logging in to $0 is protected by a password, and the host does not allow logging in with a password. Please provide the password of the key at $1.": [
  null,
  "登入 $0 的 SSH 所用的金鑰受到密碼保護，且該主機並不允許使用密碼登入。請於 $1 提供該金鑰的密碼。"
 ],
 "The SSH key for logging in to $0 is protected. You can log in with either your login password or by providing the password of the key at $1.": [
  null,
  "登入 $0 的 SSH 金鑰受到保護。您可以用您的登入密碼、或提供前述金鑰的密碼於 $1 以便登入。"
 ],
 "The fingerprint should match:": [
  null,
  "指紋應該相符："
 ],
 "The key password can not be empty": [
  null,
  "金鑰密碼不可為空值"
 ],
 "The key passwords do not match": [
  null,
  "金鑰密碼不相符"
 ],
 "The logged in user is not permitted to view system modifications": [
  null,
  "登入的使用者沒有檢視系統變更所需的權限"
 ],
 "The password can not be empty": [
  null,
  "密碼不可為空值"
 ],
 "The resulting fingerprint is fine to share via public methods, including email.": [
  null,
  "產生的指紋可放心以公開方式分享，包含電子郵件。"
 ],
 "The resulting fingerprint is fine to share via public methods, including email. If you are asking someone else to do the verification for you, they can send the results using any method.": [
  null,
  "產生的指紋可放心以公開方式分享，包含電子郵件。如果您需要其他人士為您進行驗證，那些人士可使用任何方法傳送結果。"
 ],
 "The server refused to authenticate '$0' using password authentication, and no other supported authentication methods are available.": [
  null,
  "伺服器拒絕以密碼方式驗證 '$0'，且沒有其他受支援的驗證方式可供使用。"
 ],
 "The server refused to authenticate using any supported methods.": [
  null,
  "伺服器拒絕使用任何受支援的方式進行驗證。"
 ],
 "The web browser configuration prevents Cockpit from running (inaccessible $0)": [
  null,
  "瀏覽器設定使 Cockpit 無法運作（無法存取 $0）"
 ],
 "This tool configures the SELinux policy and can help with understanding and resolving policy violations.": [
  null,
  "此工具設定 SELinux 政策且能協助瞭解與處理政策違規事件。"
 ],
 "This tool configures the system to write kernel crash dumps. It supports the \"local\" (disk), \"ssh\", and \"nfs\" dump targets.": [
  null,
  "此工具設定系統以儲存核心當機傾印。它支援 \"本機\" (磁碟)、\"ssh\"，與 \"nfs\" 作為傾印儲存目的地。"
 ],
 "This tool generates an archive of configuration and diagnostic information from the running system. The archive may be stored locally or centrally for recording or tracking purposes or may be sent to technical support representatives, developers or system administrators to assist with technical fault-finding and debugging.": [
  null,
  "此工具會從運作中的系統產生一個具有診斷資訊與設定的封存檔案。這個封存檔案可被存在本機、或為了紀錄與追蹤用途集中存放，也可能被傳送至技術支援代表、開發者或系統管理員以協助排除技術障礙與對程式除錯。"
 ],
 "This tool manages local storage, such as filesystems, LVM2 volume groups, and NFS mounts.": [
  null,
  "此工具管理本機儲存空間，如檔案系統、LVM2 磁碟區群組與掛載 NFS。"
 ],
 "This tool manages networking such as bonds, bridges, teams, VLANs and firewalls using NetworkManager and Firewalld. NetworkManager is incompatible with Ubuntu's default systemd-networkd and Debian's ifupdown scripts.": [
  null,
  "此工具使用 NetworkManager 與 Firewalld 管理網路功能，如 bond、橋接、teams、VLAN 和防火牆。NetworkManager 與 Ubuntu 預設的 systemd-networkd 及 Debian 的 ifupdown scripts 皆不相容。"
 ],
 "This web browser is too old to run the Web Console (missing $0)": [
  null,
  "此網頁瀏覽器太舊，以至於無法執行網頁控制台（缺少 $0）"
 ],
 "Time zone": [
  null,
  "時區"
 ],
 "To ensure that your connection is not intercepted by a malicious third-party, please verify the host key fingerprint:": [
  null,
  "要確保您的連線未被惡意的第三方截取，請驗證主機金鑰指紋："
 ],
 "To verify a fingerprint, run the following on $0 while physically sitting at the machine or through a trusted network:": [
  null,
  "要驗證指紋，請實際坐在機器前或透過可信任的網路於 $0 上執行以下指令："
 ],
 "Toggle date picker": [
  null,
  "打開/關閉日期挑選器"
 ],
 "Too much data": [
  null,
  "資料過多"
 ],
 "Total size: $0": [
  null,
  "總大小： $0"
 ],
 "Tower": [
  null,
  "塔"
 ],
 "Transient packageless sessions require the same operating system and version, for compatibility reasons: $0.": [
  null,
  "暫時性的無須套件工作階段因相容性問題，須為相同的作業系統與版本： $0 。"
 ],
 "Trust and add host": [
  null,
  "信任並新增主機"
 ],
 "Try again": [
  null,
  "再試一次"
 ],
 "Trying to synchronize with $0": [
  null,
  "正在嘗試與 $0 同步"
 ],
 "Unable to connect to that address": [
  null,
  "無法連線到該位址"
 ],
 "Unable to log in to $0 using SSH key authentication. Please provide the password.": [
  null,
  "無法使用 SSH 金鑰驗證登入 $0。請提供密碼。"
 ],
 "Unable to log in to $0. The host does not accept password login or any of your SSH keys.": [
  null,
  "無法登入 $0 。該主機不接受密碼登入、或您的任何 SSH 金鑰。"
 ],
 "Unknown": [
  null,
  "不明"
 ],
 "Unknown host: $0": [
  null,
  "未知的主機： $0"
 ],
 "Untrusted host": [
  null,
  "不受信任的主機"
 ],
 "User name": [
  null,
  "使用者名稱"
 ],
 "User name cannot be empty": [
  null,
  "使用者名稱不能留白"
 ],
 "Validating authentication token": [
  null,
  "驗證身份驗證令牌"
 ],
 "Verify fingerprint": [
  null,
  "驗證指紋"
 ],
 "View all logs": [
  null,
  "檢視所有日誌紀錄檔"
 ],
 "View automation script": [
  null,
  "檢視自動化 script"
 ],
 "Visit firewall": [
  null,
  "前往防火牆"
 ],
 "Waiting for other software management operations to finish": [
  null,
  "等待其他軟體管理動作完成"
 ],
 "Weak password": [
  null,
  "弱密碼"
 ],
 "Web Console for Linux servers": [
  null,
  "Linux伺服器的Web控制台"
 ],
 "Wrong user name or password": [
  null,
  "錯誤的使用者名稱或密碼"
 ],
 "You are connecting to $0 for the first time.": [
  null,
  "您第一次連線到 $0 。"
 ],
 "Your browser does not allow paste from the context menu. You can use Shift+Insert.": [
  null,
  "您的瀏覽器不允許從網頁選單貼上。您可以使用 Shift+Insert 。"
 ],
 "Your session has been terminated.": [
  null,
  "您的工作階段已被終止。"
 ],
 "Your session has expired. Please log in again.": [
  null,
  "您的工作階段已過期。請再次登入。"
 ],
 "Zone": [
  null,
  "區域"
 ],
 "[binary data]": [
  null,
  "[二進位資料]"
 ],
 "[no data]": [
  null,
  "[沒有資料]"
 ],
 "in less than a minute": [
  null,
  "於一分鐘內"
 ],
 "less than a minute ago": [
  null,
  "不到一分鐘前"
 ],
 "password quality": [
  null,
  "密碼品質"
 ],
 "show less": [
  null,
  "顯示較少"
 ],
 "show more": [
  null,
  "顯示更多"
 ]
};
