window.cockpit_po = {
 "": {
  "plural-forms": (n) => n != 1,
  "language": "nl",
  "language-direction": "ltr"
 },
 "$0 GiB": [
  null,
  "$0 GiB"
 ],
 "$0 day": [
  null,
  "$0 dag",
  "$0 dagen"
 ],
 "$0 error": [
  null,
  "$0 fout"
 ],
 "$0 exited with code $1": [
  null,
  "$0 verlaten met code $1"
 ],
 "$0 failed": [
  null,
  "$0 mislukte"
 ],
 "$0 hour": [
  null,
  "$0 uur",
  "$0 uren"
 ],
 "$0 is not available from any repository.": [
  null,
  "$0 is van geen enkele repository beschikbaar."
 ],
 "$0 key changed": [
  null,
  "$0 sleutel gewijzigd"
 ],
 "$0 killed with signal $1": [
  null,
  "$0 is afgeschoten met signaal $1"
 ],
 "$0 minute": [
  null,
  "$0 minuut",
  "$0 minuten"
 ],
 "$0 month": [
  null,
  "$0 maand",
  "$0 maanden"
 ],
 "$0 week": [
  null,
  "$0 week",
  "$0 weken"
 ],
 "$0 will be installed.": [
  null,
  "$0 zal geïnstalleerd worden."
 ],
 "$0 year": [
  null,
  "$0 jaar",
  "$0 jaren"
 ],
 "1 day": [
  null,
  "1 dag"
 ],
 "1 hour": [
  null,
  "1 uur"
 ],
 "1 minute": [
  null,
  "1 minuut"
 ],
 "1 week": [
  null,
  "1 week"
 ],
 "20 minutes": [
  null,
  "20 minuten"
 ],
 "40 minutes": [
  null,
  "40 minuten"
 ],
 "5 minutes": [
  null,
  "5 minuten"
 ],
 "6 hours": [
  null,
  "6 uren"
 ],
 "60 minutes": [
  null,
  "60 minuten"
 ],
 "A compatible version of Cockpit is not installed on $0.": [
  null,
  "Er is geen compatibele versie van Cockpit geïnstalleerd op $0."
 ],
 "A modern browser is required for security, reliability, and performance.": [
  null,
  "Een moderne browser is vereist voor goede beveiliging, betrouwbaarheid en prestaties."
 ],
 "A new SSH key at $0 will be created for $1 on $2 and it will be added to the $3 file of $4 on $5.": [
  null,
  "Een nieuwe SSH-sleutel op $0 wordt gemaakt voor $1 op $2 en deze wordt toegevoegd aan het $3-bestand van $4 op $5."
 ],
 "Absent": [
  null,
  "Afwezig"
 ],
 "Accept key and log in": [
  null,
  "Accepteer sleutel en log in"
 ],
 "Acceptable password": [
  null,
  "Acceptabel wachtwoord"
 ],
 "Add $0": [
  null,
  "Toevoegen $0"
 ],
 "Additional packages:": [
  null,
  "Extra pakketten:"
 ],
 "Administration with Cockpit Web Console": [
  null,
  "Beheer met Cockpit Web Console"
 ],
 "Advanced TCA": [
  null,
  "Geavanceerde TCA"
 ],
 "All-in-one": [
  null,
  "Alles in een"
 ],
 "Ansible": [
  null,
  "Ansible"
 ],
 "Ansible roles documentation": [
  null,
  "Ansible rollen documentatie"
 ],
 "Authentication": [
  null,
  "Authenticatie"
 ],
 "Authentication failed": [
  null,
  "Authenticatie mislukte"
 ],
 "Authentication is required to perform privileged tasks with the Cockpit Web Console": [
  null,
  "Authenticatie is vereist voor het uitvoeren van bevoorrechte taken met de Cockpit Web Console"
 ],
 "Authorize SSH key": [
  null,
  "Autoriseer SSH-sleutel"
 ],
 "Automatically using NTP": [
  null,
  "Automatisch gebruik van NTP"
 ],
 "Automatically using additional NTP servers": [
  null,
  "Automatisch gebruik van extra NTP servers"
 ],
 "Automatically using specific NTP servers": [
  null,
  "Automatisch gebruik van specifieke NTP servers"
 ],
 "Automation script": [
  null,
  "Automatiserings-script"
 ],
 "Blade": [
  null,
  "Antenne"
 ],
 "Blade enclosure": [
  null,
  "Antennebehuizing"
 ],
 "Bus expansion chassis": [
  null,
  "Busuitbreidingschassis"
 ],
 "Cancel": [
  null,
  "Annuleren"
 ],
 "Cannot forward login credentials": [
  null,
  "Kan inloggegevens niet doorsturen"
 ],
 "Cannot schedule event in the past": [
  null,
  "Kan evenement niet in het verleden plannen"
 ],
 "Change": [
  null,
  "Verandering"
 ],
 "Change system time": [
  null,
  "Verander systeemtijd"
 ],
 "Changed keys are often the result of an operating system reinstallation. However, an unexpected change may indicate a third-party attempt to intercept your connection.": [
  null,
  "Gewijzigde sleutels zijn vaak het resultaat van een herinstallatie van het besturingssysteem. Een onverwachte wijziging kan echter wijzen op een poging van derden om je verbinding te onderscheppen."
 ],
 "Checking installed software": [
  null,
  "Controleren op geïnstalleerde software"
 ],
 "Close": [
  null,
  "Sluiten"
 ],
 "Cockpit": [
  null,
  "Cockpit"
 ],
 "Cockpit authentication is configured incorrectly.": [
  null,
  "Cockpit authenticatie is verkeerd geconfigureerd."
 ],
 "Cockpit configuration of NetworkManager and Firewalld": [
  null,
  "Cockpit configuratie van NetworkManager en Firewalld"
 ],
 "Cockpit could not contact the given host.": [
  null,
  "Cockpit kon geen contact maken met de opgegeven host."
 ],
 "Cockpit is a server manager that makes it easy to administer your Linux servers via a web browser. Jumping between the terminal and the web tool is no problem. A service started via Cockpit can be stopped via the terminal. Likewise, if an error occurs in the terminal, it can be seen in the Cockpit journal interface.": [
  null,
  "Cockpit is een serverbeheerder waarmee je Linux-servers eenvoudig kunt beheren via een webbrowser. Omschakelen tussen de terminal en het webgereedschap is geen probleem. Een service gestart via Cockpit kan via de terminal worden gestopt. Evenzo, als er een fout optreedt in de terminal, kan deze worden gezien in de Cockpit-logboekinterface."
 ],
 "Cockpit is not compatible with the software on the system.": [
  null,
  "Cockpit is niet compatibel met de software op het systeem."
 ],
 "Cockpit is not installed": [
  null,
  "Cockpit is niet geïnstalleerd"
 ],
 "Cockpit is not installed on the system.": [
  null,
  "Cockpit is niet op het systeem geïnstalleerd."
 ],
 "Cockpit is perfect for new sysadmins, allowing them to easily perform simple tasks such as storage administration, inspecting journals and starting and stopping services. You can monitor and administer several servers at the same time. Just add them with a single click and your machines will look after its buddies.": [
  null,
  "Cockpit is perfect voor nieuwe systeembeheerders, waardoor ze eenvoudig eenvoudige taken kunnen uitvoeren, zoals opslagbeheer, het inspecteren van logboeken en het starten en stoppen van services. Je kunt meerdere servers tegelijkertijd bewaken en beheren. Voeg ze gewoon toe met een enkele klik en je machines zullen voor zijn maatjes zorgen."
 ],
 "Cockpit might not render correctly in your browser": [
  null,
  "Cockpit wordt mogelijk niet correct weergegeven in je browser"
 ],
 "Collect and package diagnostic and support data": [
  null,
  "Verzamel en verpak diagnostische en ondersteuningsdata"
 ],
 "Collect kernel crash dumps": [
  null,
  "Verzamel kernelcrashdumps"
 ],
 "Compact PCI": [
  null,
  "Compact PCI"
 ],
 "Confirm key password": [
  null,
  "Bevestig sleutelwachtwoord"
 ],
 "Connect to": [
  null,
  "Verbinden met"
 ],
 "Connect to:": [
  null,
  "Verbinden met:"
 ],
 "Connection has timed out.": [
  null,
  "Er is een time-out opgetreden voor de verbinding."
 ],
 "Convertible": [
  null,
  "Converteerbaar"
 ],
 "Copied": [
  null,
  "Gekopieerd"
 ],
 "Copy": [
  null,
  "Kopiëren"
 ],
 "Copy to clipboard": [
  null,
  "Kopiëren naar clipboard"
 ],
 "Create a new SSH key and authorize it": [
  null,
  "Maak een nieuwe SSH-sleutel en autoriseer deze"
 ],
 "Create new task file with this content.": [
  null,
  "Maak nieuw taakbestand aan met deze inhoud."
 ],
 "Ctrl+Insert": [
  null,
  "Ctrl+Insert"
 ],
 "Delay": [
  null,
  "Vertraging"
 ],
 "Desktop": [
  null,
  "Bureaublad"
 ],
 "Detachable": [
  null,
  "Demonteerbaar"
 ],
 "Diagnostic reports": [
  null,
  "Diagnostische rapporten"
 ],
 "Docking station": [
  null,
  "Docking station"
 ],
 "Download a new browser for free": [
  null,
  "Download gratis een nieuwe browser"
 ],
 "Downloading $0": [
  null,
  "$0 downloaden"
 ],
 "Dual rank": [
  null,
  "Dubbele rangorde"
 ],
 "Embedded PC": [
  null,
  "Ingebouwde pc"
 ],
 "Excellent password": [
  null,
  "Uitstekend wachtwoord"
 ],
 "Expansion chassis": [
  null,
  "Uitbreidingschassis"
 ],
 "Failed to change password": [
  null,
  "Kan wachtwoord niet veranderen"
 ],
 "Failed to enable $0 in firewalld": [
  null,
  "Kan $0 niet inschakelen in firewalld"
 ],
 "Go to now": [
  null,
  "Ga nu naar"
 ],
 "Handheld": [
  null,
  "Handheld"
 ],
 "Hide confirmation password": [
  null,
  "Bevestigingswachtwoord verbergen"
 ],
 "Hide password": [
  null,
  "Wachtwoord verbergen"
 ],
 "Host key is incorrect": [
  null,
  "Hostsleutel is onjuist"
 ],
 "If the fingerprint matches, click \"Accept key and log in\". Otherwise, do not log in and contact your administrator.": [
  null,
  "Als de vingerafdruk overeenkomt, klik dan op \"Accepteer sleutel en log in\". Log anders niet in en neem contact op met je beheerder."
 ],
 "If the fingerprint matches, click 'Trust and add host'. Otherwise, do not connect and contact your administrator.": [
  null,
  "Als de vingerafdruk overeenkomt, klik dan op 'Vertrouw en host toevoegen'. Verbind anders niet en neem contact op met je beheerder."
 ],
 "Install": [
  null,
  "Installeren"
 ],
 "Install software": [
  null,
  "Installeer software"
 ],
 "Install the cockpit-system package (and optionally other cockpit packages) on $0 to enable web console access.": [
  null,
  ""
 ],
 "Installing $0": [
  null,
  "$0 installeren"
 ],
 "Internal error": [
  null,
  "Interne fout"
 ],
 "Internal error: Invalid challenge header": [
  null,
  "Interne fout: ongeldige exceptie koptekst"
 ],
 "Invalid date format": [
  null,
  "Ongeldige datum"
 ],
 "Invalid date format and invalid time format": [
  null,
  "Ongeldige datumnotatie en ongeldige tijdnotatie"
 ],
 "Invalid file permissions": [
  null,
  "Ongeldige bestandsrechten"
 ],
 "Invalid time format": [
  null,
  "Ongeldige tijdnotatie"
 ],
 "Invalid timezone": [
  null,
  "Ongeldige tijdzone"
 ],
 "IoT gateway": [
  null,
  "IoT-gateway"
 ],
 "Kernel dump": [
  null,
  "Kerneldump"
 ],
 "Key password": [
  null,
  "Sleutelwachtwoord"
 ],
 "Laptop": [
  null,
  "Laptop"
 ],
 "Learn more": [
  null,
  "Kom meer te weten"
 ],
 "Loading system modifications...": [
  null,
  "Systeemwijzigingen laden..."
 ],
 "Log in": [
  null,
  "Inloggen"
 ],
 "Log in to $0": [
  null,
  "Log in op $0"
 ],
 "Log in with your server user account.": [
  null,
  "Log in met je servergebruikersaccount."
 ],
 "Log messages": [
  null,
  "Log berichten"
 ],
 "Login": [
  null,
  "Log in"
 ],
 "Login again": [
  null,
  "Log opnieuw in"
 ],
 "Login failed": [
  null,
  "Inloggen mislukte"
 ],
 "Logout successful": [
  null,
  "Uitloggen succesvol"
 ],
 "Low profile desktop": [
  null,
  "Laag profiel bureaublad"
 ],
 "Lunch box": [
  null,
  "Lunchbox"
 ],
 "Main server chassis": [
  null,
  "Hoofdserverchassis"
 ],
 "Manage storage": [
  null,
  "Beheerde opslag"
 ],
 "Manually": [
  null,
  "Handmatig"
 ],
 "Message to logged in users": [
  null,
  "Bericht aan ingelogde gebruikers"
 ],
 "Mini PC": [
  null,
  "Mini PC"
 ],
 "Mini tower": [
  null,
  "Mini tower"
 ],
 "Multi-system chassis": [
  null,
  "Chassis met meerdere systemen"
 ],
 "NTP server": [
  null,
  "NTP-server"
 ],
 "Need at least one NTP server": [
  null,
  "Minimaal één NTP-server nodig"
 ],
 "Networking": [
  null,
  "Netwerken"
 ],
 "New host": [
  null,
  "Nieuwe host"
 ],
 "New password was not accepted": [
  null,
  "Nieuw wachtwoord is niet geaccepteerd"
 ],
 "No delay": [
  null,
  "Geen vertraging"
 ],
 "No results found": [
  null,
  "Geen resultaten gevonden"
 ],
 "No such file or directory": [
  null,
  "Bestand of map bestaat niet"
 ],
 "No system modifications": [
  null,
  "Geen systeemwijzigingen"
 ],
 "Not a valid private key": [
  null,
  "Geen geldige privésleutel"
 ],
 "Not permitted to perform this action.": [
  null,
  "Niet toegestaan om deze actie uit te voeren."
 ],
 "Not synchronized": [
  null,
  "Niet gesynchroniseerd"
 ],
 "Notebook": [
  null,
  "Notebook"
 ],
 "Occurrences": [
  null,
  "Voorvallen"
 ],
 "Ok": [
  null,
  "OK"
 ],
 "Old password not accepted": [
  null,
  "Oude wachtwoord niet geaccepteerd"
 ],
 "Once Cockpit is installed, enable it with \"systemctl enable --now cockpit.socket\".": [
  null,
  "Nadat Cockpit geïnstalleerd is, schakel je het in met \"systemctl enable --now cockpit.socket\"."
 ],
 "Or use a bundled browser": [
  null,
  "Of gebruik een gebundelde browser"
 ],
 "Other": [
  null,
  "Andere"
 ],
 "Other options": [
  null,
  "Andere opties"
 ],
 "PackageKit crashed": [
  null,
  "PackageKit is gecrasht"
 ],
 "Packageless session unavailable": [
  null,
  ""
 ],
 "Password": [
  null,
  "Wachtwoord"
 ],
 "Password is not acceptable": [
  null,
  "Wachtwoord is niet acceptabel"
 ],
 "Password is too weak": [
  null,
  "Wachtwoord is te zwak"
 ],
 "Password not accepted": [
  null,
  "Wachtwoord wordt niet geaccepteerd"
 ],
 "Paste": [
  null,
  "Plakken"
 ],
 "Paste error": [
  null,
  "Plakfout"
 ],
 "Path to file": [
  null,
  "Pad naar bestand"
 ],
 "Peripheral chassis": [
  null,
  "Randchassis"
 ],
 "Permission denied": [
  null,
  "Toestemming geweigerd"
 ],
 "Pick date": [
  null,
  "Kies datum"
 ],
 "Pizza box": [
  null,
  "Pizzadoos"
 ],
 "Please enable JavaScript to use the Web Console.": [
  null,
  "Zet JavaScript aan om de Webconsole te gebruiken."
 ],
 "Please specify the host to connect to": [
  null,
  "Geef de host op waarmee je verbinding wilt maken"
 ],
 "Portable": [
  null,
  "Draagbaar"
 ],
 "Present": [
  null,
  "Aanwezig"
 ],
 "Prompting via ssh-add timed out": [
  null,
  "Vragen via ssh-add is verlopen"
 ],
 "Prompting via ssh-keygen timed out": [
  null,
  "Vragen ssh-keygen is verlopen"
 ],
 "RAID chassis": [
  null,
  "RAID-chassis"
 ],
 "Rack mount chassis": [
  null,
  "Rackmontagechassis"
 ],
 "Reboot": [
  null,
  "Opnieuw opstarten"
 ],
 "Recent hosts": [
  null,
  "Recente hosts"
 ],
 "Removals:": [
  null,
  "Verwijderingen:"
 ],
 "Remove host": [
  null,
  "Verwijder host"
 ],
 "Removing $0": [
  null,
  "$0 verwijderen"
 ],
 "Run this command over a trusted network or physically on the remote machine:": [
  null,
  "Voer deze opdracht uit via een vertrouwd netwerk of fysiek op de externe machine:"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "SSH key": [
  null,
  "SSH-sleutel"
 ],
 "Sealed-case PC": [
  null,
  "Gesloten PC"
 ],
 "Security Enhanced Linux configuration and troubleshooting": [
  null,
  "Security Enhanced Linux configuratie en probleemoplossing"
 ],
 "Server": [
  null,
  "Server"
 ],
 "Server has closed the connection.": [
  null,
  "Server heeft de verbinding verbroken."
 ],
 "Set time": [
  null,
  "Stel tijd in"
 ],
 "Shell script": [
  null,
  "Shell-script"
 ],
 "Shift+Insert": [
  null,
  "Shift+Insert"
 ],
 "Show confirmation password": [
  null,
  "Bevestigingswachtwoord tonen"
 ],
 "Show password": [
  null,
  "Wachtwoord tonen"
 ],
 "Shut down": [
  null,
  "Afsluiten"
 ],
 "Single rank": [
  null,
  "Enkele rang"
 ],
 "Space-saving computer": [
  null,
  "Ruimtebesparende computer"
 ],
 "Specific time": [
  null,
  "Specifieke tijd"
 ],
 "Stick PC": [
  null,
  "Stick PC"
 ],
 "Storage": [
  null,
  "Opslag"
 ],
 "Strong password": [
  null,
  "Sterk wachtwoord"
 ],
 "Sub-Chassis": [
  null,
  "Sub-chassis"
 ],
 "Sub-Notebook": [
  null,
  "Sub-notebook"
 ],
 "Synchronized": [
  null,
  "Gesynchroniseerd"
 ],
 "Synchronized with $0": [
  null,
  "Gesynchroniseerd met $0"
 ],
 "Synchronizing": [
  null,
  "Synchroniseren"
 ],
 "Tablet": [
  null,
  "Tablet"
 ],
 "The SSH key $0 of $1 on $2 will be added to the $3 file of $4 on $5.": [
  null,
  "De SSH-sleutel $0 van $1 op $2 zal worden toegevoegd aan het $3 bestand van $4 op $5."
 ],
 "The SSH key $0 will be made available for the remainder of the session and will be available for login to other hosts as well.": [
  null,
  "De SSH-sleutel $0 zal beschikbaar worden gesteld voor de rest van de sessie en zal ook beschikbaar zijn om in te loggen bij andere hosts."
 ],
 "The SSH key for logging in to $0 is protected by a password, and the host does not allow logging in with a password. Please provide the password of the key at $1.": [
  null,
  "De SSH-sleutel voor inloggen op $0 is beschermd met een wachtwoord en de host staat inloggen met een wachtwoord niet toe. Geef het wachtwoord van de sleutel op $1."
 ],
 "The SSH key for logging in to $0 is protected. You can log in with either your login password or by providing the password of the key at $1.": [
  null,
  "De SSH-sleutel voor inloggen op $0 is beschermd. Je kunt inloggen met je inlogwachtwoord of door het wachtwoord van de sleutel op te geven op $1."
 ],
 "The fingerprint should match:": [
  null,
  "De vingerafdruk moet overeenkomen met:"
 ],
 "The key password can not be empty": [
  null,
  "Het sleutelwachtwoord mag niet leeg zijn"
 ],
 "The key passwords do not match": [
  null,
  "De sleutelwachtwoorden komen niet overeen"
 ],
 "The logged in user is not permitted to view system modifications": [
  null,
  "De ingelogde gebruiker mag geen systeemwijzigingen bekijken"
 ],
 "The password can not be empty": [
  null,
  "Het wachtwoord mag niet leeg zijn"
 ],
 "The resulting fingerprint is fine to share via public methods, including email.": [
  null,
  "De resulterende vingerafdruk is prima te delen via openbare methoden, inclusief e-mail."
 ],
 "The resulting fingerprint is fine to share via public methods, including email. If you are asking someone else to do the verification for you, they can send the results using any method.": [
  null,
  "De resulterende vingerafdruk kan prima worden gedeeld via openbare methoden, waaronder e-mail. Als je iemand anders vraagt om de verificatie voor je uit te voeren, kan hij of zij de resultaten op elke gewenste manier verzenden."
 ],
 "The server refused to authenticate '$0' using password authentication, and no other supported authentication methods are available.": [
  null,
  "De server weigerde '$0' te verifiëren met wachtwoordverificatie en er zijn geen andere ondersteunde verificatiemethoden beschikbaar."
 ],
 "The server refused to authenticate using any supported methods.": [
  null,
  "De server weigerde te verifiëren met behulp van ondersteunde methoden."
 ],
 "The web browser configuration prevents Cockpit from running (inaccessible $0)": [
  null,
  "De configuratie van de webbrowser voorkomt dat Cockpit wordt uitgevoerd (ontoegankelijk $0)"
 ],
 "This tool configures the SELinux policy and can help with understanding and resolving policy violations.": [
  null,
  "Dit gereedschap configureert het SELinux-beleid en kan helpen bij het begrijpen en oplossen van beleidsschendingen."
 ],
 "This tool generates an archive of configuration and diagnostic information from the running system. The archive may be stored locally or centrally for recording or tracking purposes or may be sent to technical support representatives, developers or system administrators to assist with technical fault-finding and debugging.": [
  null,
  "Dit gereedschap genereert een archief met configuratie- en diagnostische informatie van het draaiende systeem. Het archief kan lokaal of centraal worden opgeslagen voor opname- of trackingsoeleinden of kan worden verzonden naar vertegenwoordigers van de technische ondersteuning, ontwikkelaars of systeembeheerders om te helpen bij het opsporen van technische fouten en het debuggen."
 ],
 "This tool manages local storage, such as filesystems, LVM2 volume groups, and NFS mounts.": [
  null,
  "Dit gereedschap beheert lokale opslag, zoals bestandssystemen, LVM2-volumegroepen en NFS-koppelingen."
 ],
 "This tool manages networking such as bonds, bridges, teams, VLANs and firewalls using NetworkManager and Firewalld. NetworkManager is incompatible with Ubuntu's default systemd-networkd and Debian's ifupdown scripts.": [
  null,
  "Dit gereedschap beheert netwerken zoals bindingen, bruggen, teams, VLAN's en firewalls met behulp van NetworkManager en Firewalld. NetworkManager is niet compatibel met Ubuntu's standaard systemd-networkd en Debian's ifupdown-scripts."
 ],
 "This web browser is too old to run the Web Console (missing $0)": [
  null,
  "Deze webbrowser is te oud om de Webconsole uit te voeren ($0 ontbreekt)"
 ],
 "Time zone": [
  null,
  "Tijdzone"
 ],
 "To ensure that your connection is not intercepted by a malicious third-party, please verify the host key fingerprint:": [
  null,
  "Controleer de vingerafdruk van de hostsleutel om ervoor te zorgen dat je verbinding niet wordt onderschept door een kwaadwillende derde partij:"
 ],
 "To verify a fingerprint, run the following on $0 while physically sitting at the machine or through a trusted network:": [
  null,
  "Om een vingerafdruk te verifiëren, voer je het volgende uit op $0 terwijl je fysiek achter de machine zit of via een vertrouwd netwerk:"
 ],
 "Toggle date picker": [
  null,
  "Datumkiezer omschakelen"
 ],
 "Too much data": [
  null,
  "Teveel data"
 ],
 "Total size: $0": [
  null,
  "Totale grootte: $0"
 ],
 "Tower": [
  null,
  "Toren"
 ],
 "Transient packageless sessions require the same operating system and version, for compatibility reasons: $0.": [
  null,
  ""
 ],
 "Trust and add host": [
  null,
  "Vertrouw en voeg host toe"
 ],
 "Try again": [
  null,
  "Probeer opnieuw"
 ],
 "Trying to synchronize with $0": [
  null,
  "Bezig met synchroniseren met $0"
 ],
 "Unable to connect to that address": [
  null,
  "Kan niet verbinden met dat adres"
 ],
 "Unable to log in to $0. The host does not accept password login or any of your SSH keys.": [
  null,
  "Kan niet inloggen op $0. De host accepteert geen aanmelden met wachtwoord of een van je SSH-sleutels."
 ],
 "Unknown": [
  null,
  "Onbekend"
 ],
 "Untrusted host": [
  null,
  "Niet vertrouwde host"
 ],
 "User name": [
  null,
  "Gebruikersnaam"
 ],
 "User name cannot be empty": [
  null,
  "Gebruikersnaam mag niet leeg zijn"
 ],
 "Validating authentication token": [
  null,
  "Verificatietoken valideren"
 ],
 "Verify fingerprint": [
  null,
  "Vingerafdruk verifiëren"
 ],
 "View all logs": [
  null,
  "Bekijk alle logboeken"
 ],
 "View automation script": [
  null,
  "Bekijk automatiseringsscript"
 ],
 "Visit firewall": [
  null,
  "Bezoek firewall"
 ],
 "Waiting for other software management operations to finish": [
  null,
  "Wachten tot andere softwarebeheerhandelingen voltooid zijn"
 ],
 "Weak password": [
  null,
  "Zwak wachtwoord"
 ],
 "Web Console for Linux servers": [
  null,
  "Webconsole voor Linux-servers"
 ],
 "Wrong user name or password": [
  null,
  "Verkeerde gebruikersnaam of wachtwoord"
 ],
 "You are connecting to $0 for the first time.": [
  null,
  "Je maakt voor de eerste keer verbinding met $0."
 ],
 "Your browser does not allow paste from the context menu. You can use Shift+Insert.": [
  null,
  "Je browser staat plakken vanuit het contextmenu niet toe. Je kunt Shift+Insert gebruiken."
 ],
 "Your session has been terminated.": [
  null,
  "Je sessie is beëindigd."
 ],
 "Your session has expired. Please log in again.": [
  null,
  "Je sessie is verlopen. Log nogmaals in."
 ],
 "Zone": [
  null,
  "Zone"
 ],
 "[binary data]": [
  null,
  "[binaire data]"
 ],
 "[no data]": [
  null,
  "[geen data]"
 ],
 "in less than a minute": [
  null,
  ""
 ],
 "less than a minute ago": [
  null,
  ""
 ],
 "password quality": [
  null,
  "wachtwoordkwaliteit"
 ],
 "show less": [
  null,
  "toon minder"
 ],
 "show more": [
  null,
  "toon meer"
 ]
};
