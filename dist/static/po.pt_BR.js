window.cockpit_po = {
 "": {
  "plural-forms": (n) => (n != 1),
  "language": "pt_BR",
  "language-direction": "ltr"
 },
 "$0 GiB": [
  null,
  "$0 GiB"
 ],
 "$0 day": [
  null,
  "$0 dia",
  "$0 dias"
 ],
 "$0 error": [
  null,
  "$0 erro"
 ],
 "$0 exited with code $1": [
  null,
  "$0 saiu com o código $1"
 ],
 "$0 failed": [
  null,
  "$0 falhou"
 ],
 "$0 hour": [
  null,
  "$0 hora",
  "$0 horas"
 ],
 "$0 is not available from any repository.": [
  null,
  "$0 não está disponível em nenhum repositório."
 ],
 "$0 key changed": [
  null,
  "$0 chave alterada"
 ],
 "$0 killed with signal $1": [
  null,
  "$0 mortos com sinal $1"
 ],
 "$0 minute": [
  null,
  "$0 minuto",
  "$0 minutos"
 ],
 "$0 month": [
  null,
  "$0 mês",
  "$0 meses"
 ],
 "$0 week": [
  null,
  "$0 semana",
  "$0 semanas"
 ],
 "$0 will be installed.": [
  null,
  "$0 será instalado."
 ],
 "$0 year": [
  null,
  "$0 ano",
  "$0 anos"
 ],
 "1 day": [
  null,
  "1 dia"
 ],
 "1 hour": [
  null,
  "1 hora"
 ],
 "1 minute": [
  null,
  "1 Minuto"
 ],
 "1 week": [
  null,
  "1 semana"
 ],
 "20 minutes": [
  null,
  "20 Minutos"
 ],
 "40 minutes": [
  null,
  "40 Minutos"
 ],
 "5 minutes": [
  null,
  "5 minutos"
 ],
 "6 hours": [
  null,
  "6 horas"
 ],
 "60 minutes": [
  null,
  "60 Minutos"
 ],
 "A compatible version of Cockpit is not installed on $0.": [
  null,
  "Uma versão compatível do Cockpit não está instalada em $0."
 ],
 "A modern browser is required for security, reliability, and performance.": [
  null,
  "Um navegador moderno é necessário para segurança, confiabilidade e desempenho."
 ],
 "Absent": [
  null,
  "Ausente"
 ],
 "Accept key and log in": [
  null,
  "Aceitar a chave e entrar"
 ],
 "Add $0": [
  null,
  "Adicionar $0"
 ],
 "Additional packages:": [
  null,
  "Pacotes adicionais:"
 ],
 "Administration with Cockpit Web Console": [
  null,
  "Administração com o Console Web do Cockpit"
 ],
 "Advanced TCA": [
  null,
  "TCA Avançado"
 ],
 "Ansible": [
  null,
  "Ansible"
 ],
 "Ansible roles documentation": [
  null,
  "Documentação de papéis do Ansible"
 ],
 "Authentication": [
  null,
  "Autenticação"
 ],
 "Authentication failed": [
  null,
  "Falha na Autenticação"
 ],
 "Authentication is required to perform privileged tasks with the Cockpit Web Console": [
  null,
  "Autenticação é necessária para executar ações privilegiadas no Console Web do Cockpit"
 ],
 "Authorize SSH key": [
  null,
  "Autorizar chave SSH"
 ],
 "Automatically using NTP": [
  null,
  "Usando automaticamente o NTP"
 ],
 "Automatically using specific NTP servers": [
  null,
  "Usando automaticamente servidores NTP específicos"
 ],
 "Automation script": [
  null,
  "Script de automação"
 ],
 "Blade": [
  null,
  "Blade"
 ],
 "Bus expansion chassis": [
  null,
  "Chassi de Expansão de Barramento"
 ],
 "Cancel": [
  null,
  "Cancelar"
 ],
 "Cannot forward login credentials": [
  null,
  "Não é possível prosseguir com as credenciais de login"
 ],
 "Cannot schedule event in the past": [
  null,
  "Não é possível agendar eventos no passado"
 ],
 "Change": [
  null,
  "Alterar"
 ],
 "Change system time": [
  null,
  "Alterar Horário do Sistema"
 ],
 "Changed keys are often the result of an operating system reinstallation. However, an unexpected change may indicate a third-party attempt to intercept your connection.": [
  null,
  "As chaves alteradas frequentemente são resultado de uma reinstalação do sistema operacional. No entanto, uma alteração inesperada pode indicar uma tentativa de terceiros de interceptar sua conexão."
 ],
 "Checking installed software": [
  null,
  "Verificando o software instalado"
 ],
 "Close": [
  null,
  "Fechar"
 ],
 "Cockpit": [
  null,
  "Cockpit"
 ],
 "Cockpit authentication is configured incorrectly.": [
  null,
  "A autenticação do Cockpit está configurada incorretamente."
 ],
 "Cockpit configuration of NetworkManager and Firewalld": [
  null,
  ""
 ],
 "Cockpit could not contact the given host.": [
  null,
  "O Cockpit não poderia entrar em contato com o host fornecido."
 ],
 "Cockpit is a server manager that makes it easy to administer your Linux servers via a web browser. Jumping between the terminal and the web tool is no problem. A service started via Cockpit can be stopped via the terminal. Likewise, if an error occurs in the terminal, it can be seen in the Cockpit journal interface.": [
  null,
  "Cockpit é um gerenciador de Servidores que facilita a tarefa de administrar seu servidor Linux via navegador web. Alternar entre o terminal e a ferramenta web, não é dif[icil. Um serviço pode ser iniciado pelo Cockpit e finalizado pelo Terminal. Da mesma forma, se um erro ocorrer no terminal, este pode ser detectado via interface do Cockpit."
 ],
 "Cockpit is not compatible with the software on the system.": [
  null,
  "O Cockpit não é compatível com o software no sistema."
 ],
 "Cockpit is not installed": [
  null,
  "Cockpit não está instalado"
 ],
 "Cockpit is not installed on the system.": [
  null,
  "Cockpit não está instalado no sistema."
 ],
 "Cockpit is perfect for new sysadmins, allowing them to easily perform simple tasks such as storage administration, inspecting journals and starting and stopping services. You can monitor and administer several servers at the same time. Just add them with a single click and your machines will look after its buddies.": [
  null,
  "Cockpit é perfeito para novos administradores de sistemas, permitindo-lhes facilmente realizar tarefas simples, como a administração de armazenamento, inspecionando logs e iniciar/parar serviços. É possível monitorar e administrar vários servidores ao mesmo tempo. Basta adicioná-los com um único clique e suas máquinas vão cuidar de seus companheiros."
 ],
 "Cockpit might not render correctly in your browser": [
  null,
  ""
 ],
 "Collect and package diagnostic and support data": [
  null,
  ""
 ],
 "Compact PCI": [
  null,
  "Compacto PCI"
 ],
 "Confirm key password": [
  null,
  "Confirme a senha da chave"
 ],
 "Connect to": [
  null,
  "Conectar à"
 ],
 "Connect to:": [
  null,
  "Conectar à:"
 ],
 "Connection has timed out.": [
  null,
  "A conexão expirou."
 ],
 "Convertible": [
  null,
  "Conversível"
 ],
 "Copied": [
  null,
  ""
 ],
 "Copy": [
  null,
  "Copiar"
 ],
 "Copy to clipboard": [
  null,
  "Copiar para área de transferência"
 ],
 "Create a new SSH key and authorize it": [
  null,
  ""
 ],
 "Create new task file with this content.": [
  null,
  ""
 ],
 "Ctrl+Insert": [
  null,
  "Ctrl+Insert"
 ],
 "Delay": [
  null,
  "Atraso"
 ],
 "Desktop": [
  null,
  "Desktop"
 ],
 "Detachable": [
  null,
  "Destacável"
 ],
 "Diagnostic reports": [
  null,
  "Relatório de diagnostico"
 ],
 "Docking station": [
  null,
  "Estação de ancoragem"
 ],
 "Download a new browser for free": [
  null,
  "Baixe um novo navegador gratuitamente"
 ],
 "Downloading $0": [
  null,
  "Baixando $0"
 ],
 "Dual rank": [
  null,
  ""
 ],
 "Embedded PC": [
  null,
  ""
 ],
 "Excellent password": [
  null,
  "Senha excelente"
 ],
 "Expansion chassis": [
  null,
  "Chassi de Expansão"
 ],
 "Failed to change password": [
  null,
  "Falha ao mudar senha"
 ],
 "Go to now": [
  null,
  "Ir para agora"
 ],
 "Host key is incorrect": [
  null,
  "Chave de Host incorreta"
 ],
 "If the fingerprint matches, click \"Accept key and log in\". Otherwise, do not log in and contact your administrator.": [
  null,
  ""
 ],
 "If the fingerprint matches, click 'Trust and add host'. Otherwise, do not connect and contact your administrator.": [
  null,
  ""
 ],
 "Install": [
  null,
  "Instale"
 ],
 "Install software": [
  null,
  "Instale Software"
 ],
 "Install the cockpit-system package (and optionally other cockpit packages) on $0 to enable web console access.": [
  null,
  ""
 ],
 "Installing $0": [
  null,
  "Instalando $0"
 ],
 "Internal error": [
  null,
  "Erro interno"
 ],
 "Internal error: Invalid challenge header": [
  null,
  "Erro interno: Cabeçalho de desafio inválido"
 ],
 "Invalid date format": [
  null,
  "Formato de data inválido"
 ],
 "Invalid date format and invalid time format": [
  null,
  "Formato de data inválido e formato de tempo inválido"
 ],
 "Invalid file permissions": [
  null,
  "Permissão de arquivos inválida"
 ],
 "Invalid time format": [
  null,
  "Formato de tempo inválido"
 ],
 "Invalid timezone": [
  null,
  "Fuso horário inválido"
 ],
 "IoT gateway": [
  null,
  "Gateway IoT"
 ],
 "Kernel dump": [
  null,
  "Dump do Kernel"
 ],
 "Key password": [
  null,
  "Nova senha"
 ],
 "Laptop": [
  null,
  "Laptop"
 ],
 "Learn more": [
  null,
  "Saiba mais"
 ],
 "Loading system modifications...": [
  null,
  "Carregando modificações do sistema..."
 ],
 "Log in": [
  null,
  "Entrar"
 ],
 "Log in with your server user account.": [
  null,
  "Faça o login com sua conta de usuário do servidor."
 ],
 "Log messages": [
  null,
  "Mensagens de Log"
 ],
 "Login again": [
  null,
  "Logar Novamente"
 ],
 "Login failed": [
  null,
  "Falha ao logar"
 ],
 "Logout successful": [
  null,
  "Login Bem Sucedido"
 ],
 "Low profile desktop": [
  null,
  "Desktop de baixo perfil"
 ],
 "Lunch box": [
  null,
  "Lunch box"
 ],
 "Main server chassis": [
  null,
  "Chassi do Servidor Principal"
 ],
 "Manually": [
  null,
  "Manualmente"
 ],
 "Message to logged in users": [
  null,
  "Mensagem para usuários logados"
 ],
 "Mini PC": [
  null,
  "Mini PC"
 ],
 "Mini tower": [
  null,
  "Mini Torre"
 ],
 "Multi-system chassis": [
  null,
  "Chassi Multi-sistema"
 ],
 "NTP server": [
  null,
  "Servidor NTP"
 ],
 "Need at least one NTP server": [
  null,
  "Precisa de pelo menos um servidor NTP"
 ],
 "Networking": [
  null,
  "Rede"
 ],
 "New host": [
  null,
  "Novo host"
 ],
 "New password was not accepted": [
  null,
  "Nova senha não foi aceita"
 ],
 "No delay": [
  null,
  "Sem Atraso"
 ],
 "No results found": [
  null,
  "Nenhum resultado encontrado"
 ],
 "No such file or directory": [
  null,
  "Diretório ou arquivo não encontrado"
 ],
 "No system modifications": [
  null,
  "Nenhuma modificações no sistema"
 ],
 "Not a valid private key": [
  null,
  "Chave privada não válida"
 ],
 "Not permitted to perform this action.": [
  null,
  "Não é permitido executar esta ação."
 ],
 "Not synchronized": [
  null,
  "Não sincronizado"
 ],
 "Notebook": [
  null,
  "Notebook"
 ],
 "Ok": [
  null,
  "Ok"
 ],
 "Old password not accepted": [
  null,
  "Senha antiga não aceita"
 ],
 "Once Cockpit is installed, enable it with \"systemctl enable --now cockpit.socket\".": [
  null,
  "Uma vez instalado o Cockpit, habilite-o com \"systemctl enable --now cockpit.socket\"."
 ],
 "Or use a bundled browser": [
  null,
  "Ou use um navegador incluído"
 ],
 "Other": [
  null,
  "De outros"
 ],
 "Other options": [
  null,
  "Outras Opções"
 ],
 "PackageKit crashed": [
  null,
  "PackageKit caiu"
 ],
 "Packageless session unavailable": [
  null,
  ""
 ],
 "Password": [
  null,
  "Senha"
 ],
 "Password is not acceptable": [
  null,
  "Senha não é aceitavél"
 ],
 "Password is too weak": [
  null,
  "Senha é muito fraca"
 ],
 "Password not accepted": [
  null,
  "Senha não aceita"
 ],
 "Paste": [
  null,
  "Colar"
 ],
 "Path to file": [
  null,
  "Caminho para o arquivo"
 ],
 "Peripheral chassis": [
  null,
  "Chassi Periférico"
 ],
 "Permission denied": [
  null,
  "Permissão negada"
 ],
 "Pick date": [
  null,
  ""
 ],
 "Please enable JavaScript to use the Web Console.": [
  null,
  ""
 ],
 "Please specify the host to connect to": [
  null,
  "Por favor, especifique o host para se conectar"
 ],
 "Portable": [
  null,
  "Portatil"
 ],
 "Present": [
  null,
  "Presente"
 ],
 "Prompting via ssh-add timed out": [
  null,
  "A solicitação via ssh-add expirou"
 ],
 "Prompting via ssh-keygen timed out": [
  null,
  "Solicitação via ssh-keygen expirou"
 ],
 "Reboot": [
  null,
  "Reiniciar"
 ],
 "Removals:": [
  null,
  "Remoções:"
 ],
 "Removing $0": [
  null,
  "Removendo $0"
 ],
 "Run this command over a trusted network or physically on the remote machine:": [
  null,
  ""
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "SSH key": [
  null,
  "Chave SSH"
 ],
 "Sealed-case PC": [
  null,
  "PC com caixa vedada"
 ],
 "Security Enhanced Linux configuration and troubleshooting": [
  null,
  ""
 ],
 "Server": [
  null,
  "Servidor"
 ],
 "Server has closed the connection.": [
  null,
  "O servidor encerrou a conexão."
 ],
 "Set time": [
  null,
  "Definir Tempo"
 ],
 "Shell script": [
  null,
  "Shell script"
 ],
 "Shift+Insert": [
  null,
  "Shift+Insert"
 ],
 "Shut down": [
  null,
  "Encerrar"
 ],
 "Single rank": [
  null,
  ""
 ],
 "Space-saving computer": [
  null,
  "Computador com economia de espaço"
 ],
 "Specific time": [
  null,
  "Tempo Específico"
 ],
 "Stick PC": [
  null,
  "Stick PC"
 ],
 "Storage": [
  null,
  "Armazenamento"
 ],
 "Synchronized": [
  null,
  "Sincronizado"
 ],
 "Synchronizing": [
  null,
  "Sincronizando"
 ],
 "Tablet": [
  null,
  "Tablet"
 ],
 "The SSH key $0 of $1 on $2 will be added to the $3 file of $4 on $5.": [
  null,
  ""
 ],
 "The SSH key $0 will be made available for the remainder of the session and will be available for login to other hosts as well.": [
  null,
  ""
 ],
 "The SSH key for logging in to $0 is protected by a password, and the host does not allow logging in with a password. Please provide the password of the key at $1.": [
  null,
  ""
 ],
 "The SSH key for logging in to $0 is protected. You can log in with either your login password or by providing the password of the key at $1.": [
  null,
  ""
 ],
 "The key password can not be empty": [
  null,
  "A senha da chave não pode estar vazia"
 ],
 "The key passwords do not match": [
  null,
  "As senhas da chave não coincidem"
 ],
 "The logged in user is not permitted to view system modifications": [
  null,
  ""
 ],
 "The resulting fingerprint is fine to share via public methods, including email.": [
  null,
  ""
 ],
 "The resulting fingerprint is fine to share via public methods, including email. If you are asking someone else to do the verification for you, they can send the results using any method.": [
  null,
  ""
 ],
 "The server refused to authenticate '$0' using password authentication, and no other supported authentication methods are available.": [
  null,
  "O servidor se recusou a autenticar '$0' usando a autenticação de senha e nenhum outro método de autenticação suportado está disponível."
 ],
 "The server refused to authenticate using any supported methods.": [
  null,
  "O servidor se recusou a autenticar usando quaisquer métodos suportados."
 ],
 "The web browser configuration prevents Cockpit from running (inaccessible $0)": [
  null,
  "A configuração do navegador da Web impede que o Cockpit seja executado (inacessível $0)"
 ],
 "This tool configures the SELinux policy and can help with understanding and resolving policy violations.": [
  null,
  ""
 ],
 "This tool configures the system to write kernel crash dumps. It supports the \"local\" (disk), \"ssh\", and \"nfs\" dump targets.": [
  null,
  ""
 ],
 "This tool generates an archive of configuration and diagnostic information from the running system. The archive may be stored locally or centrally for recording or tracking purposes or may be sent to technical support representatives, developers or system administrators to assist with technical fault-finding and debugging.": [
  null,
  ""
 ],
 "This tool manages local storage, such as filesystems, LVM2 volume groups, and NFS mounts.": [
  null,
  ""
 ],
 "This tool manages networking such as bonds, bridges, teams, VLANs and firewalls using NetworkManager and Firewalld. NetworkManager is incompatible with Ubuntu's default systemd-networkd and Debian's ifupdown scripts.": [
  null,
  ""
 ],
 "Time zone": [
  null,
  "Fuso Horário"
 ],
 "To ensure that your connection is not intercepted by a malicious third-party, please verify the host key fingerprint:": [
  null,
  ""
 ],
 "To verify a fingerprint, run the following on $0 while physically sitting at the machine or through a trusted network:": [
  null,
  ""
 ],
 "Toggle date picker": [
  null,
  ""
 ],
 "Too much data": [
  null,
  "Muitos dados"
 ],
 "Total size: $0": [
  null,
  "Tamanho total: $0"
 ],
 "Tower": [
  null,
  "Torre"
 ],
 "Transient packageless sessions require the same operating system and version, for compatibility reasons: $0.": [
  null,
  ""
 ],
 "Try again": [
  null,
  "Tentar novamente"
 ],
 "Trying to synchronize with $0": [
  null,
  "Tentando sincronizar com $0"
 ],
 "Unable to connect to that address": [
  null,
  "Não é possível conectar a esse endereço"
 ],
 "Unable to log in to $0 using SSH key authentication. Please provide the password.": [
  null,
  ""
 ],
 "Unable to log in to $0. The host does not accept password login or any of your SSH keys.": [
  null,
  ""
 ],
 "Unknown": [
  null,
  "Desconhecido"
 ],
 "Untrusted host": [
  null,
  "Host não confiável"
 ],
 "User name": [
  null,
  "Nome do usuário"
 ],
 "User name cannot be empty": [
  null,
  "O nome de usuário não pode estar vazio"
 ],
 "Validating authentication token": [
  null,
  "Validando token de autenticação"
 ],
 "View all logs": [
  null,
  "Ver todos os logs"
 ],
 "View automation script": [
  null,
  ""
 ],
 "Waiting for other software management operations to finish": [
  null,
  "Aguardando que outras operações de gerenciamento de software terminem"
 ],
 "Web Console for Linux servers": [
  null,
  "Console da Web para servidores Linux"
 ],
 "Wrong user name or password": [
  null,
  "Nome de usuário ou senha incorretos"
 ],
 "You are connecting to $0 for the first time.": [
  null,
  ""
 ],
 "Your browser does not allow paste from the context menu. You can use Shift+Insert.": [
  null,
  ""
 ],
 "Your session has been terminated.": [
  null,
  "Sua sessão foi encerrada."
 ],
 "Your session has expired. Please log in again.": [
  null,
  "Sua sessão expirou. Por favor, faça o login novamente."
 ],
 "Zone": [
  null,
  "Zona"
 ],
 "[binary data]": [
  null,
  "[dados binários]"
 ],
 "[no data]": [
  null,
  "[sem dados]"
 ],
 "in less than a minute": [
  null,
  ""
 ],
 "less than a minute ago": [
  null,
  ""
 ],
 "password quality": [
  null,
  ""
 ],
 "show less": [
  null,
  "mostrar menos"
 ],
 "show more": [
  null,
  "mostrar mais"
 ]
};
