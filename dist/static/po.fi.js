window.cockpit_po = {
 "": {
  "plural-forms": (n) => n != 1,
  "language": "fi",
  "language-direction": "ltr"
 },
 "$0 GiB": [
  null,
  "$0 Git"
 ],
 "$0 day": [
  null,
  "$0 päivä",
  "$0 päivää"
 ],
 "$0 error": [
  null,
  "$0 virhe"
 ],
 "$0 exited with code $1": [
  null,
  "$0 poistui koodilla $1"
 ],
 "$0 failed": [
  null,
  "$0 epäonnistui"
 ],
 "$0 hour": [
  null,
  "$0 tunti",
  "$0 tuntia"
 ],
 "$0 is not available from any repository.": [
  null,
  "$0 ei ole saatavilla mistään ohjelmistovarastosta."
 ],
 "$0 key changed": [
  null,
  "$0 avain muuttunut"
 ],
 "$0 killed with signal $1": [
  null,
  "$0 tapettu signaalilla $1"
 ],
 "$0 minute": [
  null,
  "$0 minuutti",
  "$0 minuuttia"
 ],
 "$0 month": [
  null,
  "$0 kuukausi",
  "$0 kuukautta"
 ],
 "$0 week": [
  null,
  "$0 viikko",
  "$0 viikkoa"
 ],
 "$0 will be installed.": [
  null,
  "$0 asennetaan."
 ],
 "$0 year": [
  null,
  "$0 vuosi",
  "$0 vuotta"
 ],
 "1 day": [
  null,
  "1 päivä"
 ],
 "1 hour": [
  null,
  "1 tunti"
 ],
 "1 minute": [
  null,
  "1 minuutti"
 ],
 "1 week": [
  null,
  "1 viikko"
 ],
 "20 minutes": [
  null,
  "20 minuuttia"
 ],
 "40 minutes": [
  null,
  "40 minuuttia"
 ],
 "5 minutes": [
  null,
  "5 minuuttia"
 ],
 "6 hours": [
  null,
  "6 tuntia"
 ],
 "60 minutes": [
  null,
  "60 minuuttia"
 ],
 "A compatible version of Cockpit is not installed on $0.": [
  null,
  "Yhteensopivaa versiota Cockpitistä ei ole asennettu kohteessa $0."
 ],
 "A modern browser is required for security, reliability, and performance.": [
  null,
  "Nykyaikainen selain vaaditaan turvallisuuden, luotettavuuden ja suorituskyvyn takaamiseksi."
 ],
 "A new SSH key at $0 will be created for $1 on $2 and it will be added to the $3 file of $4 on $5.": [
  null,
  "Uusi SSH-avain nimellä $0 luodaan käyttäjälle $1 koneella $2 ja se lisätään käyttäjän $3 tiedostoon $4 koneella $5."
 ],
 "Absent": [
  null,
  "Poissa"
 ],
 "Accept key and log in": [
  null,
  "Hyväksy avain ja kirjaudu sisään"
 ],
 "Add $0": [
  null,
  "Lisää $0"
 ],
 "Additional packages:": [
  null,
  "Ylimääräiset paketit:"
 ],
 "Administration with Cockpit Web Console": [
  null,
  "Hallinta Cockpit-verkkokonsolilla"
 ],
 "Advanced TCA": [
  null,
  "Edistynyt TCA"
 ],
 "All-in-one": [
  null,
  "Kaikki yhdessä"
 ],
 "Ansible": [
  null,
  "Ansible"
 ],
 "Ansible roles documentation": [
  null,
  "Ansible-roolien dokumentaatio"
 ],
 "Authentication": [
  null,
  "Tunnistautuminen"
 ],
 "Authentication failed": [
  null,
  "Tunnistautuminen epäonnistui"
 ],
 "Authentication is required to perform privileged tasks with the Cockpit Web Console": [
  null,
  "Todennus vaaditaan etuoikeutettujen tehtävien suorittamiseen Cockpit Web Console:ssa"
 ],
 "Authorize SSH key": [
  null,
  "Valtuuta SSH-avain"
 ],
 "Automatically using NTP": [
  null,
  "Käytetään automaattisesti NTP:tä"
 ],
 "Automatically using additional NTP servers": [
  null,
  "Käytetään automaattisesti lisättyjä NTP-palvelimia"
 ],
 "Automatically using specific NTP servers": [
  null,
  "Käytetään automaattisesti tiettyjä NTP-palvelimia"
 ],
 "Automation script": [
  null,
  "Automaatio-komentosarja"
 ],
 "Blade": [
  null,
  "Terä"
 ],
 "Blade enclosure": [
  null,
  "Teräkotelo"
 ],
 "Bus expansion chassis": [
  null,
  "Väylän laajennusalusta"
 ],
 "Cancel": [
  null,
  "Peru"
 ],
 "Cannot forward login credentials": [
  null,
  "Kirjautumistietoja ei voi välittää eteenpäin"
 ],
 "Cannot schedule event in the past": [
  null,
  "Tapahtumaa ei voi aikatauluttaa menneisyyteen"
 ],
 "Change": [
  null,
  "Vaihda"
 ],
 "Change system time": [
  null,
  "Vaihda järjestelmän aika"
 ],
 "Changed keys are often the result of an operating system reinstallation. However, an unexpected change may indicate a third-party attempt to intercept your connection.": [
  null,
  "Muutetut avaimet ovat usein seurausta käyttöjärjestelmän uudelleenasennuksesta. Odottamaton muutos voi kuitenkin tarkoittaa kolmannen osapuolen yritystä siepata yhteys."
 ],
 "Checking installed software": [
  null,
  "Tarkistetaan asennettu ohjelmisto"
 ],
 "Close": [
  null,
  "Sulje"
 ],
 "Cockpit": [
  null,
  "Cockpit"
 ],
 "Cockpit authentication is configured incorrectly.": [
  null,
  "Cockpitin tunnistautuminen on konfiguroitu virheellisesti."
 ],
 "Cockpit configuration of NetworkManager and Firewalld": [
  null,
  "NetworkManagerin ja Firewalld:n Cockit-asetukset"
 ],
 "Cockpit could not contact the given host.": [
  null,
  "Cockpit ei saanut yhteyttä koneeseen."
 ],
 "Cockpit is a server manager that makes it easy to administer your Linux servers via a web browser. Jumping between the terminal and the web tool is no problem. A service started via Cockpit can be stopped via the terminal. Likewise, if an error occurs in the terminal, it can be seen in the Cockpit journal interface.": [
  null,
  "Cockpit on palvelinhallintatyökalu, joka tekee ylläpidon helpoksi selaimen kautta. Liikkuminen päätteen ja verkkokäyttöliittymän välillä ei ole ongelma. Cockpitissä aloitettu palvelu voidaan lopettaa päätteessä. Samaten päätteessä näkyvä virheilmoitus voidaan nähdä myös Cockpitin journal-näkymässä."
 ],
 "Cockpit is not compatible with the software on the system.": [
  null,
  "Cockpit ei ole yhteensopiva järjestelmän ohjelmiston kanssa."
 ],
 "Cockpit is not installed": [
  null,
  "Cockpit ei ole asennettu"
 ],
 "Cockpit is not installed on the system.": [
  null,
  "Cockpit ei ole asennettu järjestelmässä."
 ],
 "Cockpit is perfect for new sysadmins, allowing them to easily perform simple tasks such as storage administration, inspecting journals and starting and stopping services. You can monitor and administer several servers at the same time. Just add them with a single click and your machines will look after its buddies.": [
  null,
  "Cockpit on täydellinen uusille ylläpitäjille. Sen avulla voi tehdä helposti toimenpiteitä kuten tallennustilan hallintaa, lokien tarkistamista sekä palveluiden käynnistämistä ja lopettamista. Voit monitoroida ja hallita useita palvelimia samanaikaisesti. Lisää ne yhdellä napsautuksella ja koneesi katsovat kavereidensa perään."
 ],
 "Cockpit might not render correctly in your browser": [
  null,
  "Cockpit ei ehkä näy oikein selaimessasi"
 ],
 "Collect and package diagnostic and support data": [
  null,
  "Kerää ja paketoi diagnostiikkaa ja tukitietoja"
 ],
 "Collect kernel crash dumps": [
  null,
  "Kerää ytimen kaatumisvedoksia"
 ],
 "Compact PCI": [
  null,
  "Kompakti PCI"
 ],
 "Confirm key password": [
  null,
  "Vahvista avaimen salasana"
 ],
 "Connect to": [
  null,
  "Yhdistä"
 ],
 "Connect to:": [
  null,
  "Yhdistä kohteeseen:"
 ],
 "Connection has timed out.": [
  null,
  "Yhteys aikakatkaistiin."
 ],
 "Convertible": [
  null,
  "Muunnettavissa"
 ],
 "Copied": [
  null,
  "Kopioitu"
 ],
 "Copy": [
  null,
  "Kopio"
 ],
 "Copy to clipboard": [
  null,
  "Kopioi leikepöydälle"
 ],
 "Create a new SSH key and authorize it": [
  null,
  "Luo uusi SSH-avain ja valtuuta se"
 ],
 "Create new task file with this content.": [
  null,
  "Luo uusi tehtävätiedosto tällä sisällöllä."
 ],
 "Ctrl+Insert": [
  null,
  "Ctrl+Insert"
 ],
 "Delay": [
  null,
  "Viive"
 ],
 "Desktop": [
  null,
  "Työpöytä"
 ],
 "Detachable": [
  null,
  "Irrotettava"
 ],
 "Diagnostic reports": [
  null,
  "Diagnostiikkaraportit"
 ],
 "Docking station": [
  null,
  "Telakka"
 ],
 "Download a new browser for free": [
  null,
  "Lataa uusi selain ilmaiseksi"
 ],
 "Downloading $0": [
  null,
  "Ladataan $0"
 ],
 "Dual rank": [
  null,
  "Kaksinkertainen sijoitus"
 ],
 "Embedded PC": [
  null,
  "Sulautettu tietokone"
 ],
 "Excellent password": [
  null,
  "Erinomainen salasana"
 ],
 "Expansion chassis": [
  null,
  "Laajennusrunko"
 ],
 "Failed to change password": [
  null,
  "Salasanan vaihtaminen epäonnistui"
 ],
 "Failed to enable $0 in firewalld": [
  null,
  "$0:n käyttöönotto firewalld:ssä epäonnistui"
 ],
 "Go to now": [
  null,
  "Mene nyt"
 ],
 "Handheld": [
  null,
  "Kädessä pidettävä"
 ],
 "Hide password": [
  null,
  "Piilota salasana"
 ],
 "Host key is incorrect": [
  null,
  "Koneen avain on väärin"
 ],
 "If the fingerprint matches, click \"Accept key and log in\". Otherwise, do not log in and contact your administrator.": [
  null,
  "Jos sormenjälki on sama, napsauta \"Hyväksy avain ja kirjaudu sisään\". Muussa tapauksessa älä kirjaudu sisään ja ota yhteyttä ylläpitäjään."
 ],
 "If the fingerprint matches, click 'Trust and add host'. Otherwise, do not connect and contact your administrator.": [
  null,
  "Jos sormenjälki on sama, napsauta 'Luota ja lisää yhteys'. Muussa tapauksessa älä muodosta yhteyttä ja ota yhteyttä järjestelmänvalvojaan."
 ],
 "Install": [
  null,
  "Asennus"
 ],
 "Install software": [
  null,
  "Asennetaan ohjelmistoja"
 ],
 "Install the cockpit-system package (and optionally other cockpit packages) on $0 to enable web console access.": [
  null,
  ""
 ],
 "Installing $0": [
  null,
  "Asennetaan $0"
 ],
 "Internal error": [
  null,
  "Sisäinen virhe"
 ],
 "Internal error: Invalid challenge header": [
  null,
  "Sisäinen virhe: Virheellinen haasteen otsikko"
 ],
 "Invalid date format": [
  null,
  "Virheellinen päivämuoto"
 ],
 "Invalid date format and invalid time format": [
  null,
  "Virheellinen päivämuoto ja aikamuoto"
 ],
 "Invalid file permissions": [
  null,
  "Virheelliset tiedosto-oikeudet"
 ],
 "Invalid time format": [
  null,
  "Virheellinen aikamuoto"
 ],
 "Invalid timezone": [
  null,
  "Virheellinen aikavyöhyke"
 ],
 "IoT gateway": [
  null,
  "IoT -yhdyskäytävä"
 ],
 "Kernel dump": [
  null,
  "Ytimen tyhjennys"
 ],
 "Key password": [
  null,
  "Avaimen salasana"
 ],
 "Laptop": [
  null,
  "Kannettava"
 ],
 "Learn more": [
  null,
  "Opi lisää"
 ],
 "Loading system modifications...": [
  null,
  "Ladataan järjestelmän muutoksia ..."
 ],
 "Log in": [
  null,
  "Kirjaudu sisään"
 ],
 "Log in to $0": [
  null,
  "Kirjaudu kohteeseen $0"
 ],
 "Log in with your server user account.": [
  null,
  "Kirjaudu sisään palvelimesi käyttäjätilillä."
 ],
 "Log messages": [
  null,
  "Kirjaa viestit"
 ],
 "Login": [
  null,
  "Sisäänkirjautuminen"
 ],
 "Login again": [
  null,
  "Kirjaudu sisään uudelleen"
 ],
 "Login failed": [
  null,
  "Kirjautuminen epäonnistui"
 ],
 "Logout successful": [
  null,
  "Uloskirjautuminen onnistui"
 ],
 "Low profile desktop": [
  null,
  "Matalan tason työpöytä"
 ],
 "Lunch box": [
  null,
  "Eväslaatikko"
 ],
 "Main server chassis": [
  null,
  "Pääpalvelimen runko"
 ],
 "Manage storage": [
  null,
  "Tallennustilan hallinta"
 ],
 "Manually": [
  null,
  "Manuaalisesti"
 ],
 "Message to logged in users": [
  null,
  "Viesti sisäänkirjautuneille käyttäjille"
 ],
 "Mini PC": [
  null,
  "Minitietokone"
 ],
 "Mini tower": [
  null,
  "Minitorni"
 ],
 "Multi-system chassis": [
  null,
  "Monijärjestelmäinen alusta"
 ],
 "NTP server": [
  null,
  "NTP-palvelin"
 ],
 "Need at least one NTP server": [
  null,
  "Tarvitaan vähintään yksi NTP-palvelin"
 ],
 "Networking": [
  null,
  "Verkko"
 ],
 "New host": [
  null,
  "Uusi kone"
 ],
 "New password was not accepted": [
  null,
  "Uutta salasanaa ei hyväksytty"
 ],
 "No delay": [
  null,
  "Ei viivettä"
 ],
 "No results found": [
  null,
  "Tuloksia ei löytynyt"
 ],
 "No such file or directory": [
  null,
  "Tiedostoa tai hakemistoa ei löydy"
 ],
 "No system modifications": [
  null,
  "Ei järjestelmän muutoksia"
 ],
 "Not a valid private key": [
  null,
  "Ei kelvollinen yksityinen avain"
 ],
 "Not permitted to perform this action.": [
  null,
  "Ei oikeutta suorittaa tätä toimintoa."
 ],
 "Not synchronized": [
  null,
  "Ei synkronisoitu"
 ],
 "Notebook": [
  null,
  "Muistikirja"
 ],
 "Occurrences": [
  null,
  "Tapahtumat"
 ],
 "Ok": [
  null,
  "OK"
 ],
 "Old password not accepted": [
  null,
  "Vanhaa salasanaa ei hyväksytty"
 ],
 "Once Cockpit is installed, enable it with \"systemctl enable --now cockpit.socket\".": [
  null,
  "Kun Cockpit on asennettu, ota se käyttöön 'systemctl enable --now cockpit.socket'."
 ],
 "Or use a bundled browser": [
  null,
  "Tai käytä valmiiksi asennettua selainta"
 ],
 "Other": [
  null,
  "Muu"
 ],
 "Other options": [
  null,
  "Muut valinnat"
 ],
 "PackageKit crashed": [
  null,
  "PackageKit kaatui"
 ],
 "Packageless session unavailable": [
  null,
  ""
 ],
 "Password": [
  null,
  "Salasana"
 ],
 "Password is not acceptable": [
  null,
  "Salasana ei ole hyväksyttävä"
 ],
 "Password is too weak": [
  null,
  "Salasana on liian heikko"
 ],
 "Password not accepted": [
  null,
  "Salasanaa ei hyväksytty"
 ],
 "Paste": [
  null,
  "Siirrä"
 ],
 "Paste error": [
  null,
  "Liittämisvirhe"
 ],
 "Path to file": [
  null,
  "Polku tiedostoon"
 ],
 "Peripheral chassis": [
  null,
  "Lisälaitteen kotelo"
 ],
 "Permission denied": [
  null,
  "Käyttö estetty"
 ],
 "Pick date": [
  null,
  "Valitse päivämäärä"
 ],
 "Pizza box": [
  null,
  "Pizza-laatikko"
 ],
 "Please enable JavaScript to use the Web Console.": [
  null,
  "Ota JavaScript käyttöön, jotta voit käyttää Verkkokonsolia."
 ],
 "Please specify the host to connect to": [
  null,
  "Määritä kone, johon haluat muodostaa yhteyden"
 ],
 "Portable": [
  null,
  "Kannettava"
 ],
 "Present": [
  null,
  "Nykyinen"
 ],
 "Prompting via ssh-add timed out": [
  null,
  "Kysely 'ssh-add':in kautta aikakatkaistiin"
 ],
 "Prompting via ssh-keygen timed out": [
  null,
  "Kysely 'ssh-keygen':in kautta aikakatkaistiin"
 ],
 "RAID chassis": [
  null,
  "RAID-runko"
 ],
 "Rack mount chassis": [
  null,
  "Räkkiin liitettävä runko"
 ],
 "Reboot": [
  null,
  "Käynnistä uudelleen"
 ],
 "Recent hosts": [
  null,
  "Viimeaikaiset isännät"
 ],
 "Removals:": [
  null,
  "Poistot:"
 ],
 "Remove host": [
  null,
  "Poista kone"
 ],
 "Removing $0": [
  null,
  "Poistetaan $0"
 ],
 "Run this command over a trusted network or physically on the remote machine:": [
  null,
  ""
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "SSH key": [
  null,
  "SSH-avain"
 ],
 "Sealed-case PC": [
  null,
  "Suljettu tietokonekotelo"
 ],
 "Security Enhanced Linux configuration and troubleshooting": [
  null,
  "Security Enhanced Linuxin asetukset ja ongelmanratkaisu"
 ],
 "Server": [
  null,
  "Palvelin"
 ],
 "Server has closed the connection.": [
  null,
  "Palvelin on sulkenut yhteyden."
 ],
 "Set time": [
  null,
  "Aseta aika"
 ],
 "Shell script": [
  null,
  "Komentotulkin komentosarja"
 ],
 "Shift+Insert": [
  null,
  "Vaihto + Syöttö"
 ],
 "Show password": [
  null,
  "Näytä salasana"
 ],
 "Shut down": [
  null,
  "Sammuta"
 ],
 "Single rank": [
  null,
  "Yksi sijoitus"
 ],
 "Space-saving computer": [
  null,
  "Tilaa säästävä tietokone"
 ],
 "Specific time": [
  null,
  "Tietty aika"
 ],
 "Stick PC": [
  null,
  "Tikku-PC"
 ],
 "Storage": [
  null,
  "Tallennustila"
 ],
 "Sub-Chassis": [
  null,
  "Alirunko"
 ],
 "Sub-Notebook": [
  null,
  "Pieni kannettava tietokone"
 ],
 "Synchronized": [
  null,
  "Synkronoitu"
 ],
 "Synchronized with $0": [
  null,
  "Synkronoi palvelimen $0 kanssa"
 ],
 "Synchronizing": [
  null,
  "Synkronoidaan"
 ],
 "Tablet": [
  null,
  "Tabletti"
 ],
 "The SSH key $0 of $1 on $2 will be added to the $3 file of $4 on $5.": [
  null,
  "Käyttäjän $0:n SSH-avain $1 koneella $2 lisätään käyttäjän $4 tiedostoon $3 koneella $5."
 ],
 "The SSH key $0 will be made available for the remainder of the session and will be available for login to other hosts as well.": [
  null,
  "SSH-avain $0 on käytettävissä koko istunnon ajan ja on käytettävissä myös muille koneille sisäänkirjautumista varten."
 ],
 "The SSH key for logging in to $0 is protected by a password, and the host does not allow logging in with a password. Please provide the password of the key at $1.": [
  null,
  "SSH-avain koneelle $0 sisäänkirjautumista varten on suojattu salasanalla, eikä kone salli salasanalla kirjautumista. Anna avaimen $1 salasana."
 ],
 "The SSH key for logging in to $0 is protected. You can log in with either your login password or by providing the password of the key at $1.": [
  null,
  "SSH-avain koneelle $0 sisäänkirjautumista varten on suojattu. Voit kirjautua sisään joko kirjautumissalasanallasi tai antamalla avaimen $1 salasanan."
 ],
 "The key password can not be empty": [
  null,
  "Avaimen salasana ei voi olla tyhjä"
 ],
 "The key passwords do not match": [
  null,
  "Avainsalasanat eivät täsmää"
 ],
 "The logged in user is not permitted to view system modifications": [
  null,
  "Sisäänkirjautunut käyttäjä ei saa tarkastella järjestelmän muutoksia"
 ],
 "The password can not be empty": [
  null,
  "Salasana ei voi olla tyhjä"
 ],
 "The resulting fingerprint is fine to share via public methods, including email.": [
  null,
  "Tuloksena oleva sormenjälki sopii jakaa julkisilla menetelmillä, mukaan lukien sähköposti."
 ],
 "The resulting fingerprint is fine to share via public methods, including email. If you are asking someone else to do the verification for you, they can send the results using any method.": [
  null,
  ""
 ],
 "The server refused to authenticate '$0' using password authentication, and no other supported authentication methods are available.": [
  null,
  "Palvelin kieltäytyi todentamasta käyttäjää '$0' salasanatodennusta käyttäen, eikä muita tuettuja todennustapoja ole käytettävissä."
 ],
 "The server refused to authenticate using any supported methods.": [
  null,
  "Palvelin kieltäytyi tunnistautumista käyttäen mitään tuetuista tavoista."
 ],
 "The web browser configuration prevents Cockpit from running (inaccessible $0)": [
  null,
  "Verkkoselaimen kokoonpano estää Cockpitin suorittamasta (ei käytettävissä $0)"
 ],
 "This tool configures the SELinux policy and can help with understanding and resolving policy violations.": [
  null,
  "Tämä työkalu asettaa SELinux-käytännön ja auttaa käytäntörikkeiden ymmärtämisessä ja ratkaisemisessa."
 ],
 "This tool generates an archive of configuration and diagnostic information from the running system. The archive may be stored locally or centrally for recording or tracking purposes or may be sent to technical support representatives, developers or system administrators to assist with technical fault-finding and debugging.": [
  null,
  "Tämä työkalu luo arkiston konfiguraatio- ja diagnostiikkatiedoista käynnissä olevasta järjestelmästä. Arkisto voidaan tallentaa paikallisesti tai keskitetysti tallennus- tai seurantatarkoituksiin tai se voidaan lähettää teknisen tuen edustajille, kehittäjille tai järjestelmänvalvojille auttamaan teknisten vikojen etsinnässä ja virheenkorjauksessa."
 ],
 "This tool manages local storage, such as filesystems, LVM2 volume groups, and NFS mounts.": [
  null,
  "Tämä työkalu hallinnoi paikallista tallennustilaa, kuten tiedostojärjestelmiä, LVM2-taltioryhmiä ja NFS-liitoksia."
 ],
 "This tool manages networking such as bonds, bridges, teams, VLANs and firewalls using NetworkManager and Firewalld. NetworkManager is incompatible with Ubuntu's default systemd-networkd and Debian's ifupdown scripts.": [
  null,
  "Tämä työkalu hallitsee verkkoyhteyksiä, kuten sidoksia, siltoja, ryhmiä, VLAN-verkkoja ja palomuureja NetworkManagerin ja Firewalld:n avulla. NetworkManager ei ole yhteensopiva Ubuntun oletusarvoisten systemd-networkd- ja Debianin ifupdown-komentosarjojen kanssa."
 ],
 "This web browser is too old to run the Web Console (missing $0)": [
  null,
  "Tämä verkkoselain on liian vanha Web-konsolin käyttämiseen (puuttuu $0)"
 ],
 "Time zone": [
  null,
  "Aikavyöhyke"
 ],
 "To ensure that your connection is not intercepted by a malicious third-party, please verify the host key fingerprint:": [
  null,
  "Tarkista koneen avaimen sormenjälki varmistaaksesi, että haitallinen kolmas osapuoli ei sieppaa yhteyttä:"
 ],
 "To verify a fingerprint, run the following on $0 while physically sitting at the machine or through a trusted network:": [
  null,
  "Vahvistaaksesi sormenjäljen, suorita seuraava koneella $0 istuessasi fyysisesti koneen ääressä tai luotettavan verkon kautta:"
 ],
 "Toggle date picker": [
  null,
  "Vaihda päivämäärän valitsin"
 ],
 "Too much data": [
  null,
  "Liian paljon dataa"
 ],
 "Total size: $0": [
  null,
  "Koko yhteensä: $0"
 ],
 "Tower": [
  null,
  "Torni"
 ],
 "Transient packageless sessions require the same operating system and version, for compatibility reasons: $0.": [
  null,
  ""
 ],
 "Try again": [
  null,
  "Yritä uudelleen"
 ],
 "Trying to synchronize with $0": [
  null,
  "Yritetään synkronoida palvelimen $0 kanssa"
 ],
 "Unable to connect to that address": [
  null,
  "Siihen osoitteeseen yhdistäminen epäonnistui"
 ],
 "Unable to log in to $0. The host does not accept password login or any of your SSH keys.": [
  null,
  "Ei voida kirjautua sisään koneelle $0. Kone ei hyväksy salasanakirjautumista tai mitään SSH-avaimistasi."
 ],
 "Unknown": [
  null,
  "Tuntematon"
 ],
 "Untrusted host": [
  null,
  "Epäluotettava kone"
 ],
 "User name": [
  null,
  "Käyttäjänimi"
 ],
 "User name cannot be empty": [
  null,
  "Käyttäjätunnus ei voi olla tyhjä"
 ],
 "Validating authentication token": [
  null,
  "Vahvistetaan todennustunnus"
 ],
 "Verify fingerprint": [
  null,
  "Vahvista sormenjälki"
 ],
 "View all logs": [
  null,
  "Katso kaikki lokit"
 ],
 "View automation script": [
  null,
  "Näytä automaatio-komentosarja"
 ],
 "Visit firewall": [
  null,
  "Käy palomuurissa"
 ],
 "Waiting for other software management operations to finish": [
  null,
  "Odotetaan muiden ohjelmistojen hallintatoimintojen päättymistä"
 ],
 "Web Console for Linux servers": [
  null,
  "Verkkokonsoli Linux-palvelimille"
 ],
 "Wrong user name or password": [
  null,
  "Väärä käyttäjätunnus tai salasana"
 ],
 "You are connecting to $0 for the first time.": [
  null,
  "Yhdistät koneeseen $0 ensimmäistä kertaa."
 ],
 "Your browser does not allow paste from the context menu. You can use Shift+Insert.": [
  null,
  "Selaimesi ei salli liittämistä pikavalikosta. Voit käyttää Vaihto+Insert."
 ],
 "Your session has been terminated.": [
  null,
  "Istuntosi on päätetty."
 ],
 "Your session has expired. Please log in again.": [
  null,
  "Istuntosi on vahventunut. Ole hyvä ja kirjaudu uudelleen sisään."
 ],
 "Zone": [
  null,
  "Alue"
 ],
 "[binary data]": [
  null,
  "[binääridata]"
 ],
 "[no data]": [
  null,
  "[ei dataa]"
 ],
 "in less than a minute": [
  null,
  ""
 ],
 "less than a minute ago": [
  null,
  ""
 ],
 "password quality": [
  null,
  "salasanan laatu"
 ],
 "show less": [
  null,
  "näytä vähemmän"
 ],
 "show more": [
  null,
  "näytä enemmän"
 ]
};
