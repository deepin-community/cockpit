window.cockpit_po = {
 "": {
  "plural-forms": (n) => (n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2,
  "language": "cs",
  "language-direction": "ltr"
 },
 "$0 GiB": [
  null,
  "$0 GiB"
 ],
 "$0 day": [
  null,
  "$0 den",
  "$0 dny",
  "$0 dnů"
 ],
 "$0 error": [
  null,
  "$0 chyba"
 ],
 "$0 exited with code $1": [
  null,
  "$0 skončilo s kódem $1"
 ],
 "$0 failed": [
  null,
  "$0 se nezdařilo"
 ],
 "$0 hour": [
  null,
  "$0 hodina",
  "$0 hodiny",
  "$0 hodin"
 ],
 "$0 is not available from any repository.": [
  null,
  "$0 není k dispozici z žádného z repozitářů."
 ],
 "$0 key changed": [
  null,
  "$0 klíč změněn"
 ],
 "$0 killed with signal $1": [
  null,
  "$0 vynuceně ukončeno signálem $1"
 ],
 "$0 minute": [
  null,
  "$0 minuta",
  "$0 minuty",
  "$0 minut"
 ],
 "$0 month": [
  null,
  "$0 měsíc",
  "$0 měsíce",
  "$0 měsíců"
 ],
 "$0 week": [
  null,
  "$0 týden",
  "$0 týdny",
  "$0 týdnů"
 ],
 "$0 will be installed.": [
  null,
  "$0 bude nainstalováno."
 ],
 "$0 year": [
  null,
  "$0 rok",
  "$0 roky",
  "$0 let"
 ],
 "1 day": [
  null,
  "1 den"
 ],
 "1 hour": [
  null,
  "1 hodina"
 ],
 "1 minute": [
  null,
  "1 minuta"
 ],
 "1 week": [
  null,
  "1 týden"
 ],
 "20 minutes": [
  null,
  "20 minut"
 ],
 "40 minutes": [
  null,
  "40 minut"
 ],
 "5 minutes": [
  null,
  "5 minut"
 ],
 "6 hours": [
  null,
  "6 hodin"
 ],
 "60 minutes": [
  null,
  "60 minut"
 ],
 "A compatible version of Cockpit is not installed on $0.": [
  null,
  "Na $0 není nainstalovaná kompatibilní verze Cockpit."
 ],
 "A modern browser is required for security, reliability, and performance.": [
  null,
  "Z důvodu zabezpečení, spolehlivosti a rychlosti je zapotřebí moderní prohlížeč."
 ],
 "A new SSH key at $0 will be created for $1 on $2 and it will be added to the $3 file of $4 on $5.": [
  null,
  "Na $0 bude vytvořen nový SSH klíč pro $1 na $2 a bude přidán do souboru $3 od $4 na $5."
 ],
 "Absent": [
  null,
  "Chybí"
 ],
 "Accept key and log in": [
  null,
  "Přijmout klíč a přihlásit se"
 ],
 "Acceptable password": [
  null,
  "Přijatelné heslo"
 ],
 "Add $0": [
  null,
  "Přidat $0"
 ],
 "Additional packages:": [
  null,
  "Další balíčky:"
 ],
 "Administration with Cockpit Web Console": [
  null,
  "Správa pomocí webové konzole Cockpit"
 ],
 "Advanced TCA": [
  null,
  "Pokročilé TCA"
 ],
 "All-in-one": [
  null,
  "Vše-v-jednom"
 ],
 "Ansible": [
  null,
  "Ansible"
 ],
 "Ansible roles documentation": [
  null,
  "Dokumentace k Ansible rolím"
 ],
 "Authentication": [
  null,
  "Ověření se"
 ],
 "Authentication failed": [
  null,
  "Ověření se nezdařilo"
 ],
 "Authentication is required to perform privileged tasks with the Cockpit Web Console": [
  null,
  "Pro provádění privilegovaných úloh pomocí webové konzole Cockpit je třeba ověřit se"
 ],
 "Authorize SSH key": [
  null,
  "Pověřit SSH klíč"
 ],
 "Automatically using NTP": [
  null,
  "Automatické použití NTP"
 ],
 "Automatically using additional NTP servers": [
  null,
  "Automatické použití dalších NTP serverů"
 ],
 "Automatically using specific NTP servers": [
  null,
  "Automatické použití uvedených NTP serverů"
 ],
 "Automation script": [
  null,
  "Automatizační skript"
 ],
 "Blade": [
  null,
  "Blade server"
 ],
 "Blade enclosure": [
  null,
  "Skříň se šachtami pro blade servery"
 ],
 "Bus expansion chassis": [
  null,
  "Skříň rozšíření sběrnice"
 ],
 "Bypass browser check": [
  null,
  "Obejít kontrolu prohlížeče"
 ],
 "Cancel": [
  null,
  "Storno"
 ],
 "Cannot forward login credentials": [
  null,
  "Nedaří přeposlat přístupové údaje"
 ],
 "Cannot schedule event in the past": [
  null,
  "Nelze naplánovat událost v minulosti"
 ],
 "Change": [
  null,
  "Změnit"
 ],
 "Change system time": [
  null,
  "Změnit systémový čas"
 ],
 "Changed keys are often the result of an operating system reinstallation. However, an unexpected change may indicate a third-party attempt to intercept your connection.": [
  null,
  "Změněné klíče jsou často výsledkem přeinstalace operačního systému. Nicméně, neočekávaná změna může značit pokus třetí strany o vložení se do vaší komunikace."
 ],
 "Checking installed software": [
  null,
  "Zjišťuje se nainstalovaný sofware"
 ],
 "Close": [
  null,
  "Zavřít"
 ],
 "Cockpit": [
  null,
  "Cockpit"
 ],
 "Cockpit authentication is configured incorrectly.": [
  null,
  "Cockpit ověřování není nastaveno správně."
 ],
 "Cockpit configuration of NetworkManager and Firewalld": [
  null,
  "Nastavování NetworkManager a Firewalld v pomocí Cockpit"
 ],
 "Cockpit could not contact the given host.": [
  null,
  "Cockpit se nepodařilo daný stroj kontaktovat."
 ],
 "Cockpit is a server manager that makes it easy to administer your Linux servers via a web browser. Jumping between the terminal and the web tool is no problem. A service started via Cockpit can be stopped via the terminal. Likewise, if an error occurs in the terminal, it can be seen in the Cockpit journal interface.": [
  null,
  "Cockpit je nástroj, který usnadňuje správu linuxových serverů prostřednictvím webového prohlížeče. Není žádným problémem přecházet mezi terminálem a webovým nástrojem. Služba spuštěná přes Cockpit může být zastavena v terminálu. Podobně, pokud dojde k chybě v terminálu, je toto vidět v rozhraní žurnálu v Cockpit."
 ],
 "Cockpit is not compatible with the software on the system.": [
  null,
  "Cockpit není kompatibilní se softwarem v systému."
 ],
 "Cockpit is not installed": [
  null,
  "Cockpit není nainstalován"
 ],
 "Cockpit is not installed on the system.": [
  null,
  "Cockpit není v systému nainstalován."
 ],
 "Cockpit is perfect for new sysadmins, allowing them to easily perform simple tasks such as storage administration, inspecting journals and starting and stopping services. You can monitor and administer several servers at the same time. Just add them with a single click and your machines will look after its buddies.": [
  null,
  "Cockpit je ideální nástroj pro nové správce systémů, neboť jim umožňuje snadno provádět jednoduché úkoly, jako je správa úložišť, kontrola žurnálu či spouštění a zastavování služeb. Je možné souběžně sledovat a spravovat několik serverů naráz. Stačí je jedním kliknutím přidat a vaše stroje se budou starat o své kamarády."
 ],
 "Cockpit might not render correctly in your browser": [
  null,
  "Může se stávat, že ve vámi používaném webovém prohlížeči nebude Cockpit správně zobrazován"
 ],
 "Collect and package diagnostic and support data": [
  null,
  "Shromáždit a zabalit data pro diagnostiku a podporu"
 ],
 "Collect kernel crash dumps": [
  null,
  "Shromáždit výpisy pádů jádra systému"
 ],
 "Compact PCI": [
  null,
  "Compact PCI"
 ],
 "Confirm key password": [
  null,
  "Potvrdit heslo ke klíči"
 ],
 "Connect to": [
  null,
  "Připojit se k"
 ],
 "Connect to:": [
  null,
  "Připojit se k:"
 ],
 "Connection has timed out.": [
  null,
  "Překročen časový limit připojení."
 ],
 "Convertible": [
  null,
  "Počítač 2v1"
 ],
 "Copied": [
  null,
  "Zkopírováno"
 ],
 "Copy": [
  null,
  "Zkopírovat"
 ],
 "Copy to clipboard": [
  null,
  "Zkopírovat do schránky"
 ],
 "Create a new SSH key and authorize it": [
  null,
  "Vytvořit nový SSH klíč a pověřit ho"
 ],
 "Create new task file with this content.": [
  null,
  "Vytvořte nový soubor s úlohou s tímto obsahem."
 ],
 "Ctrl+Insert": [
  null,
  "Ctrl+Insert"
 ],
 "Delay": [
  null,
  "Prodleva"
 ],
 "Desktop": [
  null,
  "Desktop"
 ],
 "Detachable": [
  null,
  "Odpojitelné"
 ],
 "Diagnostic reports": [
  null,
  "Diagnostická hlášení"
 ],
 "Docking station": [
  null,
  "Dokovací stanice"
 ],
 "Download a new browser for free": [
  null,
  "Zdarma si stáhnout nový prohlížeč"
 ],
 "Downloading $0": [
  null,
  "Stahuje se $0"
 ],
 "Dual rank": [
  null,
  "Dual rank"
 ],
 "Embedded PC": [
  null,
  "Jednodeskový počítač"
 ],
 "Excellent password": [
  null,
  "Skvělé heslo"
 ],
 "Expansion chassis": [
  null,
  "Rozšiřující šasi"
 ],
 "Failed to change password": [
  null,
  "Nepodařilo se změnit heslo"
 ],
 "Failed to enable $0 in firewalld": [
  null,
  "Nepodařilo se povolit $0 ve firewalld"
 ],
 "Go to now": [
  null,
  "Přejít na nyní"
 ],
 "Handheld": [
  null,
  "Pro držení v rukou"
 ],
 "Hide confirmation password": [
  null,
  "Skrýt potvrzení hesla"
 ],
 "Hide password": [
  null,
  "Skrýt heslo"
 ],
 "Host is unknown": [
  null,
  "Hostitel je neznámý"
 ],
 "Host key is incorrect": [
  null,
  "Klíč stroje není správný"
 ],
 "Hostkey does not match": [
  null,
  "Klíč hostitele neodpovídá"
 ],
 "Hostkey is unknown": [
  null,
  "Klíč hostitele je neznámý"
 ],
 "If the fingerprint matches, click \"Accept key and log in\". Otherwise, do not log in and contact your administrator.": [
  null,
  "Pokud se otisk shoduje, klikněte na „Přijmout klíč a přihlásit se“. V opačném případě se nepřipojujte a obraťte se na správce."
 ],
 "If the fingerprint matches, click 'Trust and add host'. Otherwise, do not connect and contact your administrator.": [
  null,
  "Pokud se otisk shoduje, klikněte na „Důvěřovat a přidat stroj“. V opačném případě se nepřipojujte a obraťte se na správce."
 ],
 "Install": [
  null,
  "Nainstalovat"
 ],
 "Install software": [
  null,
  "Nainstalovat software"
 ],
 "Install the cockpit-system package (and optionally other cockpit packages) on $0 to enable web console access.": [
  null,
  "Pro zpřístupnění webové konzole nainstalujte na $0 balíček cockpit-system (a případně další cockpit balíčky)."
 ],
 "Installing $0": [
  null,
  "Instaluje se $0"
 ],
 "Internal error": [
  null,
  "Vnitřní chyba"
 ],
 "Internal error: Invalid challenge header": [
  null,
  "Vnitřní chyba: neplatná hlavička výzvy"
 ],
 "Internal protocol error": [
  null,
  "Vnitřní chyba protokolu"
 ],
 "Invalid date format": [
  null,
  "Neplatný formát data"
 ],
 "Invalid date format and invalid time format": [
  null,
  "Neplatný formát data a času"
 ],
 "Invalid file permissions": [
  null,
  "Neplatná oprávnění k souboru"
 ],
 "Invalid time format": [
  null,
  "Neplatný formát času"
 ],
 "Invalid timezone": [
  null,
  "Neplatné časové pásmo"
 ],
 "IoT gateway": [
  null,
  "Brána Internetu věcí (IoT)"
 ],
 "Kernel dump": [
  null,
  "Výpis paměti jádra"
 ],
 "Key password": [
  null,
  "Heslo ke klíči"
 ],
 "Laptop": [
  null,
  "Notebook"
 ],
 "Learn more": [
  null,
  "Zjistit více"
 ],
 "Loading system modifications...": [
  null,
  "Načítání modifikací systému…"
 ],
 "Log in": [
  null,
  "Přihlásit se"
 ],
 "Log in to $0": [
  null,
  "Přihlásit se k $0"
 ],
 "Log in with your server user account.": [
  null,
  "Přihlášení pomocí uživatelského účtu na tomto serveru."
 ],
 "Log messages": [
  null,
  "Zprávy záznamu událostí"
 ],
 "Login": [
  null,
  "Přihlásit"
 ],
 "Login again": [
  null,
  "Znovu přihlásit"
 ],
 "Login failed": [
  null,
  "Přihlášení se nezdařilo"
 ],
 "Logout successful": [
  null,
  "Odhlášení úspěšné"
 ],
 "Low profile desktop": [
  null,
  "Nízký desktop"
 ],
 "Lunch box": [
  null,
  "Kufříkový počítač"
 ],
 "Main server chassis": [
  null,
  "Hlavní skříň serveru"
 ],
 "Manage storage": [
  null,
  "Spravovat úložiště"
 ],
 "Manually": [
  null,
  "Ručně"
 ],
 "Message to logged in users": [
  null,
  "Zpráva přihlášeným uživatelům"
 ],
 "Mini PC": [
  null,
  "Mini PC"
 ],
 "Mini tower": [
  null,
  "Mini věž"
 ],
 "Multi-system chassis": [
  null,
  "Skříň pro více systémů"
 ],
 "NTP server": [
  null,
  "NTP server"
 ],
 "Need at least one NTP server": [
  null,
  "Je třeba alespoň jeden NTP server"
 ],
 "Networking": [
  null,
  "Síť"
 ],
 "New host": [
  null,
  "Nový hostitel"
 ],
 "New password was not accepted": [
  null,
  "Nové heslo nebylo přijato"
 ],
 "No delay": [
  null,
  "Bez prodlevy"
 ],
 "No results found": [
  null,
  "Nenalezeny žádné výsledky"
 ],
 "No such file or directory": [
  null,
  "Žádný takový soubor nebo složka"
 ],
 "No system modifications": [
  null,
  "Žádné modifikace systému"
 ],
 "Not a valid private key": [
  null,
  "Není platná soukromá část klíče"
 ],
 "Not permitted to perform this action.": [
  null,
  "Neoprávněni k provedení této akce."
 ],
 "Not synchronized": [
  null,
  "Nesynchronizováno"
 ],
 "Notebook": [
  null,
  "Notebook"
 ],
 "Occurrences": [
  null,
  "Výskyty"
 ],
 "Ok": [
  null,
  "OK"
 ],
 "Old password not accepted": [
  null,
  "Původní heslo nebylo přijato"
 ],
 "Once Cockpit is installed, enable it with \"systemctl enable --now cockpit.socket\".": [
  null,
  "Jakmile bude Cockpit nainstalovaný, zapněte ho pomocí příkazu „systemctl enable --now cockpit.socket“."
 ],
 "Or use a bundled browser": [
  null,
  "Nebo použijte přibalený prohlížeč"
 ],
 "Other": [
  null,
  "Ostatní"
 ],
 "Other options": [
  null,
  "Ostatní volby"
 ],
 "PackageKit crashed": [
  null,
  "PackageKit zhavaroval"
 ],
 "Packageless session unavailable": [
  null,
  "Relace bez balíčků není k dispozici"
 ],
 "Password": [
  null,
  "Heslo"
 ],
 "Password is not acceptable": [
  null,
  "Heslo není přijatelné"
 ],
 "Password is too weak": [
  null,
  "Heslo je příliš slabé"
 ],
 "Password not accepted": [
  null,
  "Heslo nepřijato"
 ],
 "Paste": [
  null,
  "Vložit"
 ],
 "Paste error": [
  null,
  "Chyba vkládání"
 ],
 "Path to file": [
  null,
  "Popis umístění souboru"
 ],
 "Peripheral chassis": [
  null,
  "Skříň periferií"
 ],
 "Permission denied": [
  null,
  "Přístup zamítnut"
 ],
 "Pick date": [
  null,
  "Vyberte datum"
 ],
 "Pizza box": [
  null,
  "Velikost „krabice od pizzy“"
 ],
 "Please enable JavaScript to use the Web Console.": [
  null,
  "Pokud chcete používat webovou konzoli, zapněte podporu pro JavaScript."
 ],
 "Please specify the host to connect to": [
  null,
  "Zadejte stroj ke kterému se připojit"
 ],
 "Portable": [
  null,
  "Přenosný"
 ],
 "Present": [
  null,
  "Přítomno"
 ],
 "Prompting via ssh-add timed out": [
  null,
  "Časový limit výzvy prostřednictvím ssh-add překročen"
 ],
 "Prompting via ssh-keygen timed out": [
  null,
  "Časový limit výzvy prostřednictvím ssh-keygen překročen"
 ],
 "RAID chassis": [
  null,
  "RAID skříň"
 ],
 "Rack mount chassis": [
  null,
  "Skříň do stojanu"
 ],
 "Reboot": [
  null,
  "Restartovat"
 ],
 "Recent hosts": [
  null,
  "Nedávní hostitelé"
 ],
 "Refusing to connect": [
  null,
  "Odmítá se připojit"
 ],
 "Removals:": [
  null,
  "Odebrání:"
 ],
 "Remove host": [
  null,
  "Odebrat hostitele"
 ],
 "Removing $0": [
  null,
  "Odebírá se $0"
 ],
 "Row expansion": [
  null,
  "Rozbalení řádku"
 ],
 "Row select": [
  null,
  "Výběr řádku"
 ],
 "Run this command over a trusted network or physically on the remote machine:": [
  null,
  "Na vzdáleném stroji spusťte – přes důvěryhodnou síť nebo fyzicky přímo na něm – tento příkaz:"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "SSH key": [
  null,
  "SSH klíč"
 ],
 "SSH key login": [
  null,
  "Přihlášení SSH klíčem"
 ],
 "Sealed-case PC": [
  null,
  "Počítač se zapečetěnou skříní"
 ],
 "Security Enhanced Linux configuration and troubleshooting": [
  null,
  "Nastavení SELinux a řešení problémů"
 ],
 "Server": [
  null,
  "Server"
 ],
 "Server closed connection": [
  null,
  "Server zavřel spojení"
 ],
 "Server has closed the connection.": [
  null,
  "Server zavřel spojení."
 ],
 "Set time": [
  null,
  "Nastavit čas"
 ],
 "Shell script": [
  null,
  "Shellový skript"
 ],
 "Shift+Insert": [
  null,
  "Shift+Insert"
 ],
 "Show confirmation password": [
  null,
  "Zobrazit potvrzení hesla"
 ],
 "Show password": [
  null,
  "Zobrazit heslo"
 ],
 "Shut down": [
  null,
  "Vypnout"
 ],
 "Single rank": [
  null,
  "Single rank"
 ],
 "Space-saving computer": [
  null,
  "Prostorově úsporný počítač"
 ],
 "Specific time": [
  null,
  "Konkrétní čas"
 ],
 "Stick PC": [
  null,
  "Počítač v klíčence"
 ],
 "Storage": [
  null,
  "Úložiště"
 ],
 "Strong password": [
  null,
  "Odolné heslo"
 ],
 "Sub-Chassis": [
  null,
  "Zmenšená skříň"
 ],
 "Sub-Notebook": [
  null,
  "Zmenšený notebook"
 ],
 "Synchronized": [
  null,
  "Synchronizováno"
 ],
 "Synchronized with $0": [
  null,
  "Synchronizováno s $0"
 ],
 "Synchronizing": [
  null,
  "Synchronizuje se"
 ],
 "Tablet": [
  null,
  "Tablet"
 ],
 "The SSH key $0 of $1 on $2 will be added to the $3 file of $4 on $5.": [
  null,
  "SSH klíč $0 uživatele $1 na $2 bude přidán do souboru $3 uživatele $4 na $5."
 ],
 "The SSH key $0 will be made available for the remainder of the session and will be available for login to other hosts as well.": [
  null,
  "SSH klíč $0 bude zpřístupněn po celou relaci a bude k dispozici také pro přihlašování se k ostatním strojům."
 ],
 "The SSH key for logging in to $0 is protected by a password, and the host does not allow logging in with a password. Please provide the password of the key at $1.": [
  null,
  "SSH klíč pro přihlašování se k $0 je chráněn. Můžete se buď přihlásit svým přihlašovacím heslem nebo zadáním hesla ke klíči na $1."
 ],
 "The SSH key for logging in to $0 is protected. You can log in with either your login password or by providing the password of the key at $1.": [
  null,
  "SSH klíč pro přihlašování se k $0 je chráněn. Můžete se buď přihlásit svým přihlašovacím heslem nebo zadáním hesla ke klíči na $1."
 ],
 "The fingerprint should match:": [
  null,
  "Otisk by se měl shodovat:"
 ],
 "The key password can not be empty": [
  null,
  "Heslo ke klíči je třeba vyplnit"
 ],
 "The key passwords do not match": [
  null,
  "Zadání hesla ke klíči se neshodují"
 ],
 "The logged in user is not permitted to view system modifications": [
  null,
  "Přihlášený uživatel není oprávněn zobrazovat modifikace systému"
 ],
 "The password can not be empty": [
  null,
  "Heslo je třeba vyplnit"
 ],
 "The resulting fingerprint is fine to share via public methods, including email.": [
  null,
  "Výsledný otisk je možné sdílet veřejnými způsoby, včetně e-mailu."
 ],
 "The resulting fingerprint is fine to share via public methods, including email. If you are asking someone else to do the verification for you, they can send the results using any method.": [
  null,
  "Výsledný otisk je možné bez problémů sdílet prostřednictvím veřejných metod, včetně e-mailu. Pokud někoho jiného požádáte, aby pro vás ověřil, může výsledky poslat libovolnou metodou."
 ],
 "The server refused to authenticate '$0' using password authentication, and no other supported authentication methods are available.": [
  null,
  "Server odmítl ověřit „$0“ pomocí ověření heslem a nejsou k dispozici žádné další metody ověření."
 ],
 "The server refused to authenticate using any supported methods.": [
  null,
  "Server odmítl ověřit u všech podporovaných metod."
 ],
 "The web browser configuration prevents Cockpit from running (inaccessible $0)": [
  null,
  "Nastavení webového prohlížeče brání ve spuštění Cockpit (nepřístupné $0)"
 ],
 "This tool configures the SELinux policy and can help with understanding and resolving policy violations.": [
  null,
  "Tento nástroj nastavuje zásady pro SELinux a může pomoci s porozuměním a řešením jejich porušení."
 ],
 "This tool configures the system to write kernel crash dumps. It supports the \"local\" (disk), \"ssh\", and \"nfs\" dump targets.": [
  null,
  "Tento nástroj nastavuje systém tak, aby byly zapisovány výpisy pádů jádra. Podporuje cíle výstupu „local“ (disk), „ssh“ a „nfs“."
 ],
 "This tool generates an archive of configuration and diagnostic information from the running system. The archive may be stored locally or centrally for recording or tracking purposes or may be sent to technical support representatives, developers or system administrators to assist with technical fault-finding and debugging.": [
  null,
  "Tento nástroj vytváří archiv nastavení a diagnostických informací z běžícího systému. Archiv je možné uložit lokálně nebo centrálně pro účely sledování či záznamu nebo je možné ho poslat zástupcům technické podpory, vývojářům nebo správcům systémů aby pomohli s hledáním technických selhání a laděním."
 ],
 "This tool manages local storage, such as filesystems, LVM2 volume groups, and NFS mounts.": [
  null,
  "Tento nástroj spravuje místní úložiště, jako například souborové systémy, LVM2 skupiny svazků a NFS připojení."
 ],
 "This tool manages networking such as bonds, bridges, teams, VLANs and firewalls using NetworkManager and Firewalld. NetworkManager is incompatible with Ubuntu's default systemd-networkd and Debian's ifupdown scripts.": [
  null,
  "Tento nástroj spravuje síťování jako například spřažení linek (typu bond i tým), mosty, VLAN sítě a brány firewall pomocí NetworkManager a Firewalld. NetworkManager není kompatibilní s výchozím stavem v Ubuntu (to používá systemd-networkd) a skripty ifupdown v distribuci Debian."
 ],
 "This web browser is too old to run the Web Console (missing $0)": [
  null,
  "Tento webový prohlížeč je příliš starý pro spuštění Web Console (chybí $0)"
 ],
 "Time zone": [
  null,
  "Časové pásmo"
 ],
 "To ensure that your connection is not intercepted by a malicious third-party, please verify the host key fingerprint:": [
  null,
  "Abyste zajistili, že do vašeho připojení není zasahováno záškodnickou třetí stranou, ověřte otisk klíče hostitele:"
 ],
 "To verify a fingerprint, run the following on $0 while physically sitting at the machine or through a trusted network:": [
  null,
  "Pokud chcete otisk ověřit, spusťte následující na $0 když jste fyzicky u stroje nebo prostřednictvím důvěryhodné sítě:"
 ],
 "Toggle date picker": [
  null,
  "Přepnout volič datumů"
 ],
 "Too much data": [
  null,
  "Příliš mnoho dat"
 ],
 "Total size: $0": [
  null,
  "Celková velikost: $0"
 ],
 "Tower": [
  null,
  "Věž"
 ],
 "Transient packageless sessions require the same operating system and version, for compatibility reasons: $0.": [
  null,
  "Přechodná bezbalíčkové relace vyžadují stejný operační systém a jeho verzi (z důvodů kompatibility): $0."
 ],
 "Trust and add host": [
  null,
  "Důvěřovat a přidat hostitele"
 ],
 "Try again": [
  null,
  "Zkusit znovu"
 ],
 "Trying to synchronize with $0": [
  null,
  "Pokus o synchronizaci se $0"
 ],
 "Unable to connect to that address": [
  null,
  "K této adrese se nedaří připojit"
 ],
 "Unable to log in to $0 using SSH key authentication. Please provide the password.": [
  null,
  "Nepodařilo se přihlásit k $0 pomocí ověření se SSH klíčem. Prosím zadejte heslo."
 ],
 "Unable to log in to $0. The host does not accept password login or any of your SSH keys.": [
  null,
  "Nedaří se přihlásit k $0. Hostitel nepřijímá přihlášení heslem nebo žádný z vašich SSH klíčů."
 ],
 "Unknown": [
  null,
  "Neznámé"
 ],
 "Unknown host: $0": [
  null,
  "Neznámý hostitel: $0"
 ],
 "Untrusted host": [
  null,
  "Nedůvěryhodný stroj"
 ],
 "User name": [
  null,
  "Uživatelské jméno"
 ],
 "User name cannot be empty": [
  null,
  "Uživatelské jméno je třeba vyplnit"
 ],
 "Validating authentication token": [
  null,
  "Kontroluje se ověřovací token"
 ],
 "Verify fingerprint": [
  null,
  "Ověřit otisk"
 ],
 "View all logs": [
  null,
  "Zobrazit všechny záznamy událostí"
 ],
 "View automation script": [
  null,
  "Zobrazit automatizační skript"
 ],
 "Visit firewall": [
  null,
  "Jít na bránu firewall"
 ],
 "Waiting for other software management operations to finish": [
  null,
  "Čeká se na dokončení ostatních operací správy balíčků"
 ],
 "Weak password": [
  null,
  "Snadno prolomitelné heslo"
 ],
 "Web Console for Linux servers": [
  null,
  "Webová konzole pro linuxové servery"
 ],
 "Wrong user name or password": [
  null,
  "Chybné uživatelské jméno nebo heslo"
 ],
 "You are connecting to $0 for the first time.": [
  null,
  "K $0 se připojujete poprvé."
 ],
 "Your browser does not allow paste from the context menu. You can use Shift+Insert.": [
  null,
  "Vámi využívaný prohlížeč neumožňuje vkládání z kontextové nabídky. Náhradně je možné použít Shift+Insert."
 ],
 "Your session has been terminated.": [
  null,
  "Vaše sezení bylo ukončeno."
 ],
 "Your session has expired. Please log in again.": [
  null,
  "Platnost vašeho sezení skončila. Přihlaste se znovu."
 ],
 "Zone": [
  null,
  "Zóna"
 ],
 "[binary data]": [
  null,
  "[binární data]"
 ],
 "[no data]": [
  null,
  "[žádná data]"
 ],
 "in less than a minute": [
  null,
  "za méně než minutu"
 ],
 "less than a minute ago": [
  null,
  "před méně než minutou"
 ],
 "password quality": [
  null,
  "odolnost hesla"
 ],
 "show less": [
  null,
  "zobrazit méně"
 ],
 "show more": [
  null,
  "zobrazit více"
 ]
};
