window.cockpit_po = {
 "": {
  "plural-forms": (n) => n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2,
  "language": "pl",
  "language-direction": "ltr"
 },
 "$0 GiB": [
  null,
  "$0 GiB"
 ],
 "$0 day": [
  null,
  "$0 dzień",
  "$0 dni",
  "$0 dni"
 ],
 "$0 error": [
  null,
  "błąd $0"
 ],
 "$0 exited with code $1": [
  null,
  "$0 zakończyło działanie z kodem $1"
 ],
 "$0 failed": [
  null,
  "$0 się nie powiodło"
 ],
 "$0 hour": [
  null,
  "$0 godzina",
  "$0 godziny",
  "$0 godzin"
 ],
 "$0 is not available from any repository.": [
  null,
  "$0 nie jest dostępne w żadnym repozytorium."
 ],
 "$0 key changed": [
  null,
  "Zmieniono klucz $0"
 ],
 "$0 killed with signal $1": [
  null,
  "$0 zostało zakończone z sygnałem $1"
 ],
 "$0 minute": [
  null,
  "$0 minuta",
  "$0 minuty",
  "$0 minut"
 ],
 "$0 month": [
  null,
  "$0 miesiąc",
  "$0 miesiące",
  "$0 miesięcy"
 ],
 "$0 week": [
  null,
  "$0 tydzień",
  "$0 tygodnie",
  "$0 tygodni"
 ],
 "$0 will be installed.": [
  null,
  "$0 zostanie zainstalowane."
 ],
 "$0 year": [
  null,
  "$0 rok",
  "$0 lata",
  "$0 lat"
 ],
 "1 day": [
  null,
  "1 dzień"
 ],
 "1 hour": [
  null,
  "1 godzina"
 ],
 "1 minute": [
  null,
  "1 minuta"
 ],
 "1 week": [
  null,
  "1 tydzień"
 ],
 "20 minutes": [
  null,
  "20 minut"
 ],
 "40 minutes": [
  null,
  "40 minut"
 ],
 "5 minutes": [
  null,
  "5 minut"
 ],
 "6 hours": [
  null,
  "6 godzin"
 ],
 "60 minutes": [
  null,
  "Godzina"
 ],
 "A compatible version of Cockpit is not installed on $0.": [
  null,
  "Na $0 nie zainstalowano zgodnej wersji Cockpit."
 ],
 "A modern browser is required for security, reliability, and performance.": [
  null,
  "Z powodów bezpieczeństwa, niezawodności i wydajności wymagana jest nowoczesna przeglądarka."
 ],
 "A new SSH key at $0 will be created for $1 on $2 and it will be added to the $3 file of $4 on $5.": [
  null,
  "Nowy klucz SSH w $0 zostanie utworzony dla użytkownika $1 na komputerze $2 i zostanie dodany do pliku $3 użytkownika $4 na komputerze $5."
 ],
 "Absent": [
  null,
  "Nieobecne"
 ],
 "Accept key and log in": [
  null,
  "Przyjmij klucz i zaloguj się"
 ],
 "Add $0": [
  null,
  "Dodaj $0"
 ],
 "Additional packages:": [
  null,
  "Dodatkowe pakiety:"
 ],
 "Administration with Cockpit Web Console": [
  null,
  "Administracja za pomocą konsoli internetowej Cockpit"
 ],
 "Advanced TCA": [
  null,
  "Zaawansowane TCA"
 ],
 "All-in-one": [
  null,
  "Wszystko w jednym"
 ],
 "Ansible": [
  null,
  "Ansible"
 ],
 "Ansible roles documentation": [
  null,
  "Dokumentacja ról Ansible"
 ],
 "Authentication": [
  null,
  "Uwierzytelnienie"
 ],
 "Authentication failed": [
  null,
  "Uwierzytelnienie się nie powiodło"
 ],
 "Authentication is required to perform privileged tasks with the Cockpit Web Console": [
  null,
  "Wymagane jest uwierzytelnienie, aby wykonać zadania wymagające uprawnień za pomocą konsoli internetowej Cockpit"
 ],
 "Authorize SSH key": [
  null,
  "Upoważnij klucz SSH"
 ],
 "Automatically using NTP": [
  null,
  "Automatycznie za pomocą NTP"
 ],
 "Automatically using additional NTP servers": [
  null,
  "Automatycznie za pomocą dodatkowych serwerów NTP"
 ],
 "Automatically using specific NTP servers": [
  null,
  "Automatycznie za pomocą podanych serwerów NTP"
 ],
 "Automation script": [
  null,
  "Skrypt automatyzacji"
 ],
 "Blade": [
  null,
  "Kasetowy"
 ],
 "Blade enclosure": [
  null,
  "Obudowa kasetowa"
 ],
 "Bus expansion chassis": [
  null,
  "Obudowa rozszerzenia magistrali"
 ],
 "Cancel": [
  null,
  "Anuluj"
 ],
 "Cannot forward login credentials": [
  null,
  "Nie można przekazać danych uwierzytelniania logowania"
 ],
 "Cannot schedule event in the past": [
  null,
  "Nie można planować zdarzeń w przeszłości"
 ],
 "Change": [
  null,
  "Zmień"
 ],
 "Change system time": [
  null,
  "Zmień czas systemu"
 ],
 "Changed keys are often the result of an operating system reinstallation. However, an unexpected change may indicate a third-party attempt to intercept your connection.": [
  null,
  "Zmienione klucze są często wynikiem przeinstalowania systemu operacyjnego. Nieoczekiwana zmiana może jednak wskazywać na próbę przechwycenia połączenia przez stronę trzecią."
 ],
 "Checking installed software": [
  null,
  "Sprawdzanie zainstalowanego oprogramowania"
 ],
 "Close": [
  null,
  "Zamknij"
 ],
 "Cockpit": [
  null,
  "Cockpit"
 ],
 "Cockpit authentication is configured incorrectly.": [
  null,
  "Uwierzytelnianie Cockpit jest niepoprawnie skonfigurowane."
 ],
 "Cockpit configuration of NetworkManager and Firewalld": [
  null,
  "Konfiguracja usług NetworkManager i firewalld w programie Cockpit"
 ],
 "Cockpit could not contact the given host.": [
  null,
  "Cockpit nie może skontaktować się z podanym komputerem."
 ],
 "Cockpit is a server manager that makes it easy to administer your Linux servers via a web browser. Jumping between the terminal and the web tool is no problem. A service started via Cockpit can be stopped via the terminal. Likewise, if an error occurs in the terminal, it can be seen in the Cockpit journal interface.": [
  null,
  "Cockpit to menedżer serwerów ułatwiający administrowanie serwerów Linux przez przeglądarkę WWW. Można z łatwością przechodzić między terminalem a narzędziami WWW. Usługa uruchomiona za pomocą Cockpit może zostać zatrzymana za pomocą terminala. Jeśli błąd wystąpi w terminalu, to można go zobaczyć w interfejsie dziennika Cockpit."
 ],
 "Cockpit is not compatible with the software on the system.": [
  null,
  "Cockpit nie jest zgodny z oprogramowaniem w systemie."
 ],
 "Cockpit is not installed": [
  null,
  "Cockpit nie jest zainstalowany"
 ],
 "Cockpit is not installed on the system.": [
  null,
  "Cockpit nie jest zainstalowany w systemie."
 ],
 "Cockpit is perfect for new sysadmins, allowing them to easily perform simple tasks such as storage administration, inspecting journals and starting and stopping services. You can monitor and administer several servers at the same time. Just add them with a single click and your machines will look after its buddies.": [
  null,
  "Cockpit jest idealny dla nowych administratorów, umożliwiając łatwe wykonywanie prostych zadań, takich jak administracja urządzeniami do przechowywania danych, badanie dzienników oraz uruchamianie i zatrzymywanie usług. Można monitorować i administrować wiele serwerów w tym samym czasie. Wystarczy dodać je pojedynczym kliknięciem."
 ],
 "Cockpit might not render correctly in your browser": [
  null,
  "Cockpit może nie być poprawnie wyświetlany w używanej przeglądarce"
 ],
 "Collect and package diagnostic and support data": [
  null,
  "Zbieranie i pakowanie danych diagnostycznych i wsparcia"
 ],
 "Collect kernel crash dumps": [
  null,
  "Zbieranie zrzutów awarii jądra"
 ],
 "Compact PCI": [
  null,
  "Kompaktowe PCI"
 ],
 "Confirm key password": [
  null,
  "Potwierdź hasło klucza"
 ],
 "Connect to": [
  null,
  "Połącz z"
 ],
 "Connect to:": [
  null,
  "Połącz z:"
 ],
 "Connection has timed out.": [
  null,
  "Połączenie przekroczyło czas oczekiwania."
 ],
 "Convertible": [
  null,
  "2 w jednym"
 ],
 "Copied": [
  null,
  "Skopiowano"
 ],
 "Copy": [
  null,
  "Skopiuj"
 ],
 "Copy to clipboard": [
  null,
  "Skopiuj do schowka"
 ],
 "Create a new SSH key and authorize it": [
  null,
  "Utwórz nowy klucz SSH i go upoważnij"
 ],
 "Create new task file with this content.": [
  null,
  "Utwórz nowy plik zadania o tej treści."
 ],
 "Ctrl+Insert": [
  null,
  "Ctrl+Insert"
 ],
 "Delay": [
  null,
  "Opóźnienie"
 ],
 "Desktop": [
  null,
  "Komputer stacjonarny"
 ],
 "Detachable": [
  null,
  "Odłączalny"
 ],
 "Diagnostic reports": [
  null,
  "Raporty diagnostyczne"
 ],
 "Docking station": [
  null,
  "Stacja dokująca"
 ],
 "Download a new browser for free": [
  null,
  "Pobierz nową przeglądarkę za darmo"
 ],
 "Downloading $0": [
  null,
  "Pobieranie $0"
 ],
 "Dual rank": [
  null,
  "Podwójny stopień"
 ],
 "Embedded PC": [
  null,
  "Komputer osadzony"
 ],
 "Excellent password": [
  null,
  "Doskonałe hasło"
 ],
 "Expansion chassis": [
  null,
  "Obudowa rozszerzenia"
 ],
 "Failed to change password": [
  null,
  "Zmiana hasła się nie powiodła"
 ],
 "Failed to enable $0 in firewalld": [
  null,
  "Włączenie $0 w usłudze firewalld się nie powiodło"
 ],
 "Go to now": [
  null,
  "Przejdź teraz"
 ],
 "Handheld": [
  null,
  "Przenośny"
 ],
 "Host key is incorrect": [
  null,
  "Klucz komputera jest niepoprawny"
 ],
 "If the fingerprint matches, click \"Accept key and log in\". Otherwise, do not log in and contact your administrator.": [
  null,
  "Jeśli odcisk się zgadza, należy kliknąć „Przyjmij klucz i zaloguj się”. W przeciwnym przypadku nie należy się logować i należy skontaktować się z administratorem."
 ],
 "If the fingerprint matches, click 'Trust and add host'. Otherwise, do not connect and contact your administrator.": [
  null,
  "Jeśli odcisk się zgadza, należy kliknąć „Zaufaj i dodaj komputer”. W przeciwnym przypadku nie należy się łączyć i należy skontaktować się z administratorem."
 ],
 "Install": [
  null,
  "Zainstaluj"
 ],
 "Install software": [
  null,
  "Zainstaluj oprogramowanie"
 ],
 "Install the cockpit-system package (and optionally other cockpit packages) on $0 to enable web console access.": [
  null,
  ""
 ],
 "Installing $0": [
  null,
  "Instalowanie $0"
 ],
 "Internal error": [
  null,
  "Wewnętrzny błąd"
 ],
 "Internal error: Invalid challenge header": [
  null,
  "Wewnętrzny błąd: nieprawidłowy nagłówek wyzwania"
 ],
 "Invalid date format": [
  null,
  "Nieprawidłowy format daty"
 ],
 "Invalid date format and invalid time format": [
  null,
  "Nieprawidłowy format daty i nieprawidłowy format czasu"
 ],
 "Invalid file permissions": [
  null,
  "Nieprawidłowe uprawnienia pliku"
 ],
 "Invalid time format": [
  null,
  "Nieprawidłowy format czasu"
 ],
 "Invalid timezone": [
  null,
  "Nieprawidłowa strefa czasowa"
 ],
 "IoT gateway": [
  null,
  "Brama IoT"
 ],
 "Kernel dump": [
  null,
  "Zrzut jądra"
 ],
 "Key password": [
  null,
  "Hasło klucza"
 ],
 "Laptop": [
  null,
  "Laptop"
 ],
 "Learn more": [
  null,
  "Więcej informacji"
 ],
 "Loading system modifications...": [
  null,
  "Wczytywanie modyfikacji systemu…"
 ],
 "Log in": [
  null,
  "Zaloguj"
 ],
 "Log in to $0": [
  null,
  "Zaloguj się na $0"
 ],
 "Log in with your server user account.": [
  null,
  "Logowanie za pomocą konta użytkownika serwera."
 ],
 "Log messages": [
  null,
  "Komunikaty dziennika"
 ],
 "Login": [
  null,
  "Logowanie"
 ],
 "Login again": [
  null,
  "Zaloguj ponownie"
 ],
 "Login failed": [
  null,
  "Logowanie się nie powiodło"
 ],
 "Logout successful": [
  null,
  "Pomyślnie wylogowano"
 ],
 "Low profile desktop": [
  null,
  "Komputer stacjonarny o mniejszym rozmiarze"
 ],
 "Lunch box": [
  null,
  "Lunch box"
 ],
 "Main server chassis": [
  null,
  "Główna obudowa serwera"
 ],
 "Manage storage": [
  null,
  "Zarządzanie urządzeniami do przechowywania danych"
 ],
 "Manually": [
  null,
  "Ręcznie"
 ],
 "Message to logged in users": [
  null,
  "Wiadomość do zalogowanych użytkowników"
 ],
 "Mini PC": [
  null,
  "Mini PC"
 ],
 "Mini tower": [
  null,
  "Mini tower"
 ],
 "Multi-system chassis": [
  null,
  "Obudowa dla wielu komputerów"
 ],
 "NTP server": [
  null,
  "Serwer NTP"
 ],
 "Need at least one NTP server": [
  null,
  "Wymagany jest co najmniej jeden serwer NTP"
 ],
 "Networking": [
  null,
  "Sieć"
 ],
 "New host": [
  null,
  "Nowy komputer"
 ],
 "New password was not accepted": [
  null,
  "Nie przyjęto nowego hasła"
 ],
 "No delay": [
  null,
  "Brak opóźnienia"
 ],
 "No results found": [
  null,
  "Brak wyników"
 ],
 "No such file or directory": [
  null,
  "Nie ma takiego pliku lub katalogu"
 ],
 "No system modifications": [
  null,
  "Brak modyfikacji systemu"
 ],
 "Not a valid private key": [
  null,
  "Nieprawidłowy klucz prywatny"
 ],
 "Not permitted to perform this action.": [
  null,
  "Brak uprawnień do wykonania tego działania."
 ],
 "Not synchronized": [
  null,
  "Niesynchronizowane"
 ],
 "Notebook": [
  null,
  "Notebook"
 ],
 "Occurrences": [
  null,
  "Wystąpienia"
 ],
 "Ok": [
  null,
  "OK"
 ],
 "Old password not accepted": [
  null,
  "Nie przyjęto poprzedniego hasła"
 ],
 "Once Cockpit is installed, enable it with \"systemctl enable --now cockpit.socket\".": [
  null,
  "Po zainstalowaniu programu Cockpit należy go włączyć za pomocą polecenia „systemctl enable --now cockpit.socket”."
 ],
 "Or use a bundled browser": [
  null,
  "Lub użyj dołączonej przeglądarki"
 ],
 "Other": [
  null,
  "Inne"
 ],
 "Other options": [
  null,
  "Inne opcje"
 ],
 "PackageKit crashed": [
  null,
  "Usługa PackageKit uległa awarii"
 ],
 "Packageless session unavailable": [
  null,
  ""
 ],
 "Password": [
  null,
  "Hasło"
 ],
 "Password is not acceptable": [
  null,
  "Hasło jest do przyjęcia"
 ],
 "Password is too weak": [
  null,
  "Hasło jest za słabe"
 ],
 "Password not accepted": [
  null,
  "Nie przyjęto hasła"
 ],
 "Paste": [
  null,
  "Wklej"
 ],
 "Paste error": [
  null,
  "Błąd wklejania"
 ],
 "Path to file": [
  null,
  "Ścieżka do pliku"
 ],
 "Peripheral chassis": [
  null,
  "Obudowa peryferyjna"
 ],
 "Permission denied": [
  null,
  "Odmowa dostępu"
 ],
 "Pick date": [
  null,
  "Wybierz datę"
 ],
 "Pizza box": [
  null,
  "Pizza box"
 ],
 "Please enable JavaScript to use the Web Console.": [
  null,
  "Proszę włączyć obsługę języka JavaScript, aby używać konsoli internetowej."
 ],
 "Please specify the host to connect to": [
  null,
  "Proszę podać komputer, z którym się połączyć"
 ],
 "Portable": [
  null,
  "Przenośne"
 ],
 "Present": [
  null,
  "Obecne"
 ],
 "Prompting via ssh-add timed out": [
  null,
  "Pytanie przez ssh-add przekroczyło czas oczekiwania"
 ],
 "Prompting via ssh-keygen timed out": [
  null,
  "Pytanie przez ssh-keygen przekroczyło czas oczekiwania"
 ],
 "RAID chassis": [
  null,
  "Obudowa RAID"
 ],
 "Rack mount chassis": [
  null,
  "Obudowa do montowania w szafie"
 ],
 "Reboot": [
  null,
  "Uruchom ponownie"
 ],
 "Recent hosts": [
  null,
  "Ostatnie komputery"
 ],
 "Removals:": [
  null,
  "Usuwane:"
 ],
 "Remove host": [
  null,
  "Usuń komputer"
 ],
 "Removing $0": [
  null,
  "Usuwanie $0"
 ],
 "Run this command over a trusted network or physically on the remote machine:": [
  null,
  "Należy wykonać to polecenie przez zaufaną sieć lub fizycznie na zdalnym komputerze:"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "SSH key": [
  null,
  "Klucz SSH"
 ],
 "Sealed-case PC": [
  null,
  "Sealed-case PC"
 ],
 "Security Enhanced Linux configuration and troubleshooting": [
  null,
  "Konfiguracja i rozwiązywanie błędów z SELinuksem"
 ],
 "Server": [
  null,
  "Serwer"
 ],
 "Server has closed the connection.": [
  null,
  "Serwer zamknął połączenie."
 ],
 "Set time": [
  null,
  "Ustaw czas"
 ],
 "Shell script": [
  null,
  "Skrypt powłoki"
 ],
 "Shift+Insert": [
  null,
  "Shift+Insert"
 ],
 "Shut down": [
  null,
  "Wyłącz"
 ],
 "Single rank": [
  null,
  "Pojedynczy stopień"
 ],
 "Space-saving computer": [
  null,
  "Komputer oszczędzający miejsce"
 ],
 "Specific time": [
  null,
  "Podany czas"
 ],
 "Stick PC": [
  null,
  "Stick PC"
 ],
 "Storage": [
  null,
  "Przechowywanie danych"
 ],
 "Sub-Chassis": [
  null,
  "Obudowa podrzędna"
 ],
 "Sub-Notebook": [
  null,
  "Sub-Notebook"
 ],
 "Synchronized": [
  null,
  "Synchronizowane"
 ],
 "Synchronized with $0": [
  null,
  "Synchronizowane z $0"
 ],
 "Synchronizing": [
  null,
  "Synchronizowanie"
 ],
 "Tablet": [
  null,
  "Tablet"
 ],
 "The SSH key $0 of $1 on $2 will be added to the $3 file of $4 on $5.": [
  null,
  "Klucz SSH $0 użytkownika $1 na komputerze $2 zostanie dodany do pliku $3 użytkownika $4 na komputerze $5."
 ],
 "The SSH key $0 will be made available for the remainder of the session and will be available for login to other hosts as well.": [
  null,
  "Klucz SSH $0 stanie się dostępny przez pozostały czas sesji i będzie dostępny do logowania także na innych komputerach."
 ],
 "The SSH key for logging in to $0 is protected by a password, and the host does not allow logging in with a password. Please provide the password of the key at $1.": [
  null,
  "Klucz SSH do logowania w $0 jest chroniony hasłem, ale komputer nie zezwala na logowanie za pomocą hasła. Proszę podać hasło klucza w $1."
 ],
 "The SSH key for logging in to $0 is protected. You can log in with either your login password or by providing the password of the key at $1.": [
  null,
  "Klucz SSH do logowania w $0 jest chroniony. Można zalogować się za pomocą hasła logowania lub podając hasło klucza w $1."
 ],
 "The fingerprint should match:": [
  null,
  "Odcisk powinien się zgadzać:"
 ],
 "The key password can not be empty": [
  null,
  "Hasło klucza nie może być puste"
 ],
 "The key passwords do not match": [
  null,
  "Hasła klucza się nie zgadzają"
 ],
 "The logged in user is not permitted to view system modifications": [
  null,
  "Zalogowany użytkownik nie ma zezwolenia na wyświetlanie modyfikacji systemu"
 ],
 "The password can not be empty": [
  null,
  "Hasło nie może być puste"
 ],
 "The resulting fingerprint is fine to share via public methods, including email.": [
  null,
  "Powstały odcisk można udostępniać publicznie, na przykład przez e-mail."
 ],
 "The resulting fingerprint is fine to share via public methods, including email. If you are asking someone else to do the verification for you, they can send the results using any method.": [
  null,
  "Powstały odcisk można udostępniać publicznie, na przykład przez e-mail. Jeśli weryfikacja jest wykonywana za użytkownika przez kogoś innego, to ta osoba może wysłać wyniki na dowolny sposób."
 ],
 "The server refused to authenticate '$0' using password authentication, and no other supported authentication methods are available.": [
  null,
  "Serwer odmówił uwierzytelnienia „$0” za pomocą hasła, a żadne inne obsługiwane metody uwierzytelniania nie są dostępne."
 ],
 "The server refused to authenticate using any supported methods.": [
  null,
  "Serwer odmówił uwierzytelnienia za pomocą wszystkich obsługiwanych metod."
 ],
 "The web browser configuration prevents Cockpit from running (inaccessible $0)": [
  null,
  "Konfiguracja przeglądarki uniemożliwia działanie programu Cockpit ($0 jest niedostępne)"
 ],
 "This tool configures the SELinux policy and can help with understanding and resolving policy violations.": [
  null,
  "To narzędzie konfiguruje zasady SELinuksa i może pomóc w zrozumieniu i rozwiązywaniu naruszeń zasad."
 ],
 "This tool generates an archive of configuration and diagnostic information from the running system. The archive may be stored locally or centrally for recording or tracking purposes or may be sent to technical support representatives, developers or system administrators to assist with technical fault-finding and debugging.": [
  null,
  "To narzędzie tworzy archiwum konfiguracji i informacji diagnostycznych z działającego komputera. Archiwum może być przechowywane lokalnie lub centralnie do celów nagrywania i śledzenia, albo może być wysyłane do przedstawicieli pomocy technicznej, programistów lub administratorów komputera w celu wspomagania znajdowania źródła problemu i debugowania."
 ],
 "This tool manages local storage, such as filesystems, LVM2 volume groups, and NFS mounts.": [
  null,
  "To narzędzie zarządza lokalnymi urządzeniami do przechowywania danych, takimi jak systemy plików, grupy woluminów LVM2 czy punkty montowania NFS."
 ],
 "This tool manages networking such as bonds, bridges, teams, VLANs and firewalls using NetworkManager and Firewalld. NetworkManager is incompatible with Ubuntu's default systemd-networkd and Debian's ifupdown scripts.": [
  null,
  "To narzędzie zarządza sieciami, takimi jak wiązania, mostki, zespoły, VLAN i zapory sieciowe za pomocą usług NetworkManager i firewalld. Usługa NetworkManager jest niezgodna z domyślnymi skryptami systemd-networkd systemu Ubuntu i ifupdown systemu Debian."
 ],
 "This web browser is too old to run the Web Console (missing $0)": [
  null,
  "Ta przeglądarka jest za stara, aby uruchomić konsolę internetową (brak $0)"
 ],
 "Time zone": [
  null,
  "Strefa czasowa"
 ],
 "To ensure that your connection is not intercepted by a malicious third-party, please verify the host key fingerprint:": [
  null,
  "Aby upewnić się, że połączenie nie jest przechwytywane przez szkodliwą stronę trzecią, proszę zweryfikować odcisk klucza komputera:"
 ],
 "To verify a fingerprint, run the following on $0 while physically sitting at the machine or through a trusted network:": [
  null,
  "Aby zweryfikować odcisk klucza, należy wykonać poniższe polecenie na $0 fizycznie siedząc przy komputerze lub przez zaufaną sieć:"
 ],
 "Toggle date picker": [
  null,
  "Przełącz wybór daty"
 ],
 "Too much data": [
  null,
  "Za dużo danych"
 ],
 "Total size: $0": [
  null,
  "Całkowity rozmiar: $0"
 ],
 "Tower": [
  null,
  "Tower"
 ],
 "Transient packageless sessions require the same operating system and version, for compatibility reasons: $0.": [
  null,
  ""
 ],
 "Trust and add host": [
  null,
  "Zaufaj i dodaj komputer"
 ],
 "Try again": [
  null,
  "Spróbuj ponownie"
 ],
 "Trying to synchronize with $0": [
  null,
  "Próbowanie synchronizacji z $0"
 ],
 "Unable to connect to that address": [
  null,
  "Nie można połączyć z tym adresem"
 ],
 "Unable to log in to $0. The host does not accept password login or any of your SSH keys.": [
  null,
  "Nie można zalogować się w $0. Komputer nie przyjmuje logowania hasłem ani żadnego z kluczy SSH użytkownika."
 ],
 "Unknown": [
  null,
  "Nieznane"
 ],
 "Untrusted host": [
  null,
  "Niezaufany komputer"
 ],
 "User name": [
  null,
  "Nazwa użytkownika"
 ],
 "User name cannot be empty": [
  null,
  "Nazwa użytkownika nie może być pusta"
 ],
 "Validating authentication token": [
  null,
  "Sprawdzanie poprawności tokenu uwierzytelniania"
 ],
 "Verify fingerprint": [
  null,
  "Zweryfikuj odcisk"
 ],
 "View all logs": [
  null,
  "Wszystkie dzienniki"
 ],
 "View automation script": [
  null,
  "Wyświetl skrypt automatyzacji"
 ],
 "Visit firewall": [
  null,
  "Otwórz zaporę sieciową"
 ],
 "Waiting for other software management operations to finish": [
  null,
  "Oczekiwanie na ukończenie pozostałych działań zarządzania oprogramowaniem"
 ],
 "Web Console for Linux servers": [
  null,
  "Konsola internetowa dla serwerów systemu Linux"
 ],
 "Wrong user name or password": [
  null,
  "Błędna nazwa użytkownika lub hasło"
 ],
 "You are connecting to $0 for the first time.": [
  null,
  "Łączenie z $0 po raz pierwszy."
 ],
 "Your browser does not allow paste from the context menu. You can use Shift+Insert.": [
  null,
  "Używana przeglądarka nie zezwala na wklejanie z menu kontekstowego. Można użyć klawiszy Shift+Insert."
 ],
 "Your session has been terminated.": [
  null,
  "Sesja została zakończona."
 ],
 "Your session has expired. Please log in again.": [
  null,
  "Sesja wygasła. Proszę zalogować się ponownie."
 ],
 "Zone": [
  null,
  "Strefa"
 ],
 "[binary data]": [
  null,
  "[dane binarne]"
 ],
 "[no data]": [
  null,
  "[brak danych]"
 ],
 "in less than a minute": [
  null,
  ""
 ],
 "less than a minute ago": [
  null,
  ""
 ],
 "password quality": [
  null,
  "jakość hasła"
 ],
 "show less": [
  null,
  "wyświetl mniej"
 ],
 "show more": [
  null,
  "wyświetl więcej"
 ]
};
