window.cockpit_po = {
 "": {
  "plural-forms": (n) => (n>1),
  "language": "tr",
  "language-direction": "ltr"
 },
 "$0 GiB": [
  null,
  "$0 GiB"
 ],
 "$0 day": [
  null,
  "$0 gün",
  "$0 gün"
 ],
 "$0 error": [
  null,
  "$0 hatası"
 ],
 "$0 exited with code $1": [
  null,
  "$0, $1 koduyla çıkış yaptı"
 ],
 "$0 failed": [
  null,
  "$0 başarısız oldu"
 ],
 "$0 hour": [
  null,
  "$0 saat",
  "$0 saat"
 ],
 "$0 is not available from any repository.": [
  null,
  "$0, hiçbir depoda yok."
 ],
 "$0 key changed": [
  null,
  "$0 anahtarı değişti"
 ],
 "$0 killed with signal $1": [
  null,
  "$0, $1 sinyali ile sonlandırıldı"
 ],
 "$0 minute": [
  null,
  "$0 dakika",
  "$0 dakika"
 ],
 "$0 month": [
  null,
  "$0 ay",
  "$0 ay"
 ],
 "$0 week": [
  null,
  "$0 hafta",
  "$0 hafta"
 ],
 "$0 will be installed.": [
  null,
  "$0 kurulacak."
 ],
 "$0 year": [
  null,
  "$0 yıl",
  "$0 yıl"
 ],
 "1 day": [
  null,
  "1 gün"
 ],
 "1 hour": [
  null,
  "1 saat"
 ],
 "1 minute": [
  null,
  "1 dakika"
 ],
 "1 week": [
  null,
  "1 hafta"
 ],
 "20 minutes": [
  null,
  "20 dakika"
 ],
 "40 minutes": [
  null,
  "40 dakika"
 ],
 "5 minutes": [
  null,
  "5 dakika"
 ],
 "6 hours": [
  null,
  "6 saat"
 ],
 "60 minutes": [
  null,
  "60 dakika"
 ],
 "A compatible version of Cockpit is not installed on $0.": [
  null,
  "$0 üzerinde uyumlu bir Cockpit sürümü kurulu değil."
 ],
 "A modern browser is required for security, reliability, and performance.": [
  null,
  "Güvenlik, güvenilirlik ve performans için modern bir tarayıcı gereklidir."
 ],
 "A new SSH key at $0 will be created for $1 on $2 and it will be added to the $3 file of $4 on $5.": [
  null,
  "$2 üzerinde $1 için $0 konumunda yeni bir SSH anahtarı oluşturulacak ve $5 üzerindeki $4 kullanıcısının $3 dosyasına eklenecektir."
 ],
 "Absent": [
  null,
  "Yok"
 ],
 "Accept key and log in": [
  null,
  "Anahtarı kabul et ve oturum aç"
 ],
 "Acceptable password": [
  null,
  "Kabul edilebilir parola"
 ],
 "Add $0": [
  null,
  "$0 ekle"
 ],
 "Additional packages:": [
  null,
  "Ek paketler:"
 ],
 "Administration with Cockpit Web Console": [
  null,
  "Cockpit Web Konsolu ile Yönetim"
 ],
 "Advanced TCA": [
  null,
  "Gelişmiş TCA"
 ],
 "All-in-one": [
  null,
  "Hepsi-bir-arada"
 ],
 "Ansible": [
  null,
  "Ansible"
 ],
 "Ansible roles documentation": [
  null,
  "Ansible rolleri belgeleri"
 ],
 "Authentication": [
  null,
  "Kimlik doğrulama"
 ],
 "Authentication failed": [
  null,
  "Kimlik doğrulama başarısız oldu"
 ],
 "Authentication is required to perform privileged tasks with the Cockpit Web Console": [
  null,
  "Cockpit Web Konsolu ile yetkili görevleri gerçekleştirmek için kimlik doğrulaması gerekir"
 ],
 "Authorize SSH key": [
  null,
  "SSH anahtarını yetkilendir"
 ],
 "Automatically using NTP": [
  null,
  "Otomatik olarak NTP kullanarak"
 ],
 "Automatically using additional NTP servers": [
  null,
  "Otomatik olarak ek NTP sunucularını kullanarak"
 ],
 "Automatically using specific NTP servers": [
  null,
  "Otomatik olarak belirli NTP sunucularını kullanarak"
 ],
 "Automation script": [
  null,
  "Otomatikleştirme betiği"
 ],
 "Blade": [
  null,
  "Blade"
 ],
 "Blade enclosure": [
  null,
  "Blade kasası"
 ],
 "Bus expansion chassis": [
  null,
  "Veri yolu genişletme kasası"
 ],
 "Bypass browser check": [
  null,
  "Tarayıcı denetimini atla"
 ],
 "Cancel": [
  null,
  "İptal"
 ],
 "Cannot forward login credentials": [
  null,
  "Oturum açma kimlik bilgileri yönlendirilemiyor"
 ],
 "Cannot schedule event in the past": [
  null,
  "Geçmişteki olay zamanlanamıyor"
 ],
 "Change": [
  null,
  "Değiştir"
 ],
 "Change system time": [
  null,
  "Sistem saatini değiştir"
 ],
 "Changed keys are often the result of an operating system reinstallation. However, an unexpected change may indicate a third-party attempt to intercept your connection.": [
  null,
  "Değiştirilen anahtarlar genellikle bir işletim sisteminin yeniden kurulması sonucudur. Ancak, beklenmeyen bir değişiklik, üçüncü tarafın bağlantınıza müdahale etme girişimini gösterebilir."
 ],
 "Checking installed software": [
  null,
  "Kurulu yazılımlar denetleniyor"
 ],
 "Close": [
  null,
  "Kapat"
 ],
 "Cockpit": [
  null,
  "Cockpit"
 ],
 "Cockpit authentication is configured incorrectly.": [
  null,
  "Cockpit kimlik doğrulaması yanlış yapılandırılmış."
 ],
 "Cockpit configuration of NetworkManager and Firewalld": [
  null,
  "NetworkManager ve Firewalld'un Cockpit yapılandırması"
 ],
 "Cockpit could not contact the given host.": [
  null,
  "Cockpit, verilen anamakineyle iletişim kuramadı."
 ],
 "Cockpit is a server manager that makes it easy to administer your Linux servers via a web browser. Jumping between the terminal and the web tool is no problem. A service started via Cockpit can be stopped via the terminal. Likewise, if an error occurs in the terminal, it can be seen in the Cockpit journal interface.": [
  null,
  "Cockpit, Linux sunucularınızı bir web tarayıcısı aracılığıyla yönetmenizi kolaylaştıran bir sunucu yöneticisidir. Terminal ve web aracı arasında geçiş yapmak sorun değildir. Cockpit aracılığıyla başlatılan bir hizmet terminal aracılığıyla durdurulabilir. Aynı şekilde, terminalde bir hata meydana gelirse, Cockpit günlüğü arayüzünde görülebilir."
 ],
 "Cockpit is not compatible with the software on the system.": [
  null,
  "Cockpit, sistemdeki yazılımla uyumlu değil."
 ],
 "Cockpit is not installed": [
  null,
  "Cockpit kurulu değil"
 ],
 "Cockpit is not installed on the system.": [
  null,
  "Cockpit sistemde kurulu değil."
 ],
 "Cockpit is perfect for new sysadmins, allowing them to easily perform simple tasks such as storage administration, inspecting journals and starting and stopping services. You can monitor and administer several servers at the same time. Just add them with a single click and your machines will look after its buddies.": [
  null,
  "Cockpit yeni sistem yöneticileri için mükemmeldir; depolama yönetimi, günlükleri inceleme, hizmetleri başlatma ve durdurma gibi basit görevleri kolayca gerçekleştirmelerine olanak tanır. Aynı anda birkaç sunucuyu izleyebilir ve yönetebilirsiniz. Bunları tek bir tıklama ile ekleyin ve makineleriniz arkadaşlarıyla ilgilensin."
 ],
 "Cockpit might not render correctly in your browser": [
  null,
  "Cockpit tarayıcınızda düzgün görüntülenmeyebilir"
 ],
 "Collect and package diagnostic and support data": [
  null,
  "Tanılama ve destek verilerini topla ve paketle"
 ],
 "Collect kernel crash dumps": [
  null,
  "Çekirdek çökme dökümlerini topla"
 ],
 "Compact PCI": [
  null,
  "Compact PCI"
 ],
 "Confirm key password": [
  null,
  "Anahtar parolasını onayla"
 ],
 "Connect to": [
  null,
  "Şuna bağlan"
 ],
 "Connect to:": [
  null,
  "Şuna bağlan:"
 ],
 "Connection has timed out.": [
  null,
  "Bağlantı zaman aşımına uğradı."
 ],
 "Convertible": [
  null,
  "Dönüştürülebilir"
 ],
 "Copied": [
  null,
  "Kopyalandı"
 ],
 "Copy": [
  null,
  "Kopyala"
 ],
 "Copy to clipboard": [
  null,
  "Panoya kopyala"
 ],
 "Create $0": [
  null,
  "$0 oluştur"
 ],
 "Create a new SSH key and authorize it": [
  null,
  "Yeni bir SSH anahtarı oluştur ve yetkilendir"
 ],
 "Create new task file with this content.": [
  null,
  "Bu içerikle yeni görev dosyası oluştur."
 ],
 "Ctrl+Insert": [
  null,
  "Ctrl+Insert"
 ],
 "Delay": [
  null,
  "Gecikme"
 ],
 "Desktop": [
  null,
  "Masaüstü"
 ],
 "Detachable": [
  null,
  "Ayrılabilir"
 ],
 "Diagnostic reports": [
  null,
  "Tanılama raporları"
 ],
 "Docking station": [
  null,
  "Kenetleme istasyonu"
 ],
 "Download a new browser for free": [
  null,
  "Ücretsiz olarak yeni bir tarayıcı indir"
 ],
 "Downloading $0": [
  null,
  "$0 indiriliyor"
 ],
 "Dual rank": [
  null,
  "Çift sıra"
 ],
 "Embedded PC": [
  null,
  "Gömülü PC"
 ],
 "Excellent password": [
  null,
  "Mükemmel parola"
 ],
 "Expansion chassis": [
  null,
  "Genişletme kasası"
 ],
 "Failed to change password": [
  null,
  "Parolayı değiştirme başarısız oldu"
 ],
 "Failed to enable $0 in firewalld": [
  null,
  "firewalld içinde $0 etkinleştirme başarısız oldu"
 ],
 "Go to now": [
  null,
  "Şimdiye git"
 ],
 "Handheld": [
  null,
  "Elde taşınan"
 ],
 "Hide confirmation password": [
  null,
  "Onay parolasını gizle"
 ],
 "Hide password": [
  null,
  "Parolayı gizle"
 ],
 "Host is unknown": [
  null,
  "Anamakine bilinmiyor"
 ],
 "Host key is incorrect": [
  null,
  "Anamakine anahtarı yanlış"
 ],
 "Hostkey does not match": [
  null,
  "Anamakine anahtarı eşleşmiyor"
 ],
 "Hostkey is unknown": [
  null,
  "Anamakine anahtarı bilinmiyor"
 ],
 "If the fingerprint matches, click \"Accept key and log in\". Otherwise, do not log in and contact your administrator.": [
  null,
  "Eğer parmak izi eşleşirse, \"Anahtarı kabul et ve oturum aç\"a tıklayın. Aksi takdirde, oturum açmayın ve yöneticinize başvurun."
 ],
 "If the fingerprint matches, click 'Trust and add host'. Otherwise, do not connect and contact your administrator.": [
  null,
  "Eğer parmak izi eşleşirse, 'Anamakineye güven ve ekle'ye tıklayın. Aksi takdirde, bağlanmayın ve yöneticinize başvurun."
 ],
 "Install": [
  null,
  "Kur"
 ],
 "Install software": [
  null,
  "Yazılım kur"
 ],
 "Install the cockpit-system package (and optionally other cockpit packages) on $0 to enable web console access.": [
  null,
  "Web konsolu erişimini etkinleştirmek için cockpit-system paketini (ve isteğe bağlı olarak diğer cockpit paketlerini) $0 üzerine yükleyin."
 ],
 "Installing $0": [
  null,
  "$0 kuruluyor"
 ],
 "Internal error": [
  null,
  "İç hata"
 ],
 "Internal error: Invalid challenge header": [
  null,
  "İç hata: Geçersiz sınama üstbilgisi"
 ],
 "Internal protocol error": [
  null,
  "İç protokol hatası"
 ],
 "Invalid date format": [
  null,
  "Geçersiz tarih biçimi"
 ],
 "Invalid date format and invalid time format": [
  null,
  "Geçersiz tarih ve saat biçimi"
 ],
 "Invalid file permissions": [
  null,
  "Geçersiz dosya izinleri"
 ],
 "Invalid time format": [
  null,
  "Geçersiz saat biçimi"
 ],
 "Invalid timezone": [
  null,
  "Geçersiz saat dilimi"
 ],
 "IoT gateway": [
  null,
  "IoT ağ geçidi"
 ],
 "Kernel dump": [
  null,
  "Çekirdek dökümü"
 ],
 "Key password": [
  null,
  "Anahtar parolası"
 ],
 "Laptop": [
  null,
  "Dizüstü"
 ],
 "Learn more": [
  null,
  "Daha fazla bilgi edinin"
 ],
 "Loading system modifications...": [
  null,
  "Sistem değişiklikleri yükleniyor..."
 ],
 "Log in": [
  null,
  "Oturum aç"
 ],
 "Log in to $0": [
  null,
  "$0 üzerinde oturum aç"
 ],
 "Log in with your server user account.": [
  null,
  "Sunucu kullanıcı hesabınızla oturum açın."
 ],
 "Log messages": [
  null,
  "Günlük iletileri"
 ],
 "Login": [
  null,
  "Oturum aç"
 ],
 "Login again": [
  null,
  "Tekrar oturum aç"
 ],
 "Login failed": [
  null,
  "Oturum açma başarısız oldu"
 ],
 "Logout successful": [
  null,
  "Oturumu kapatma başarılı oldu"
 ],
 "Low profile desktop": [
  null,
  "Düşük profilli masaüstü"
 ],
 "Lunch box": [
  null,
  "Lunch box"
 ],
 "Main server chassis": [
  null,
  "Ana sunucu kasası"
 ],
 "Manage storage": [
  null,
  "Depolamayı yönet"
 ],
 "Manually": [
  null,
  "El ile"
 ],
 "Message to logged in users": [
  null,
  "Oturum açmış kullanıcılar için ileti"
 ],
 "Mini PC": [
  null,
  "Mini PC"
 ],
 "Mini tower": [
  null,
  "Mini tower"
 ],
 "Multi-system chassis": [
  null,
  "Çok sistemli kasa"
 ],
 "NTP server": [
  null,
  "NTP sunucusu"
 ],
 "Need at least one NTP server": [
  null,
  "En az bir NTP sunucusu gerekli"
 ],
 "Networking": [
  null,
  "Ağ"
 ],
 "New host": [
  null,
  "Yeni anamakine"
 ],
 "New password was not accepted": [
  null,
  "Yeni parola kabul edilmedi"
 ],
 "No delay": [
  null,
  "Gecikme yok"
 ],
 "No results found": [
  null,
  "Bulunan sonuçlar yok"
 ],
 "No such file or directory": [
  null,
  "Böyle bir dosya ya da dizin yok"
 ],
 "No system modifications": [
  null,
  "Sistem değişiklikleri yok"
 ],
 "Not a valid private key": [
  null,
  "Geçerli bir özel anahtar değil"
 ],
 "Not permitted to perform this action.": [
  null,
  "Bu eylemi gerçekleştirmeye izinli değil."
 ],
 "Not synchronized": [
  null,
  "Eşitlenmedi"
 ],
 "Notebook": [
  null,
  "Notebook"
 ],
 "Occurrences": [
  null,
  "Oluşumlar"
 ],
 "Ok": [
  null,
  "Tamam"
 ],
 "Old password not accepted": [
  null,
  "Eski parola kabul edilmedi"
 ],
 "Once Cockpit is installed, enable it with \"systemctl enable --now cockpit.socket\".": [
  null,
  "Cockpit kurulduktan sonra, \"systemctl enable --now cockpit.socket\" komutuyla etkinleştirin."
 ],
 "Or use a bundled browser": [
  null,
  "Veya paketlenmiş bir tarayıcı kullanın"
 ],
 "Other": [
  null,
  "Diğer"
 ],
 "Other options": [
  null,
  "Diğer seçenekler"
 ],
 "PackageKit crashed": [
  null,
  "PackageKit çöktü"
 ],
 "Packageless session unavailable": [
  null,
  "Paketsiz oturum mevcut değil"
 ],
 "Password": [
  null,
  "Parola"
 ],
 "Password is not acceptable": [
  null,
  "Parola kabul edilebilir değil"
 ],
 "Password is too weak": [
  null,
  "Parola çok zayıf"
 ],
 "Password not accepted": [
  null,
  "Parola kabul edilmedi"
 ],
 "Paste": [
  null,
  "Yapıştır"
 ],
 "Paste error": [
  null,
  "Yapıştırma hatası"
 ],
 "Path to file": [
  null,
  "Dosyanın yolu"
 ],
 "Peripheral chassis": [
  null,
  "Çevresel donanım kasası"
 ],
 "Permission denied": [
  null,
  "İzin reddedildi"
 ],
 "Pick date": [
  null,
  "Tarih seçin"
 ],
 "Pizza box": [
  null,
  "Pizza box"
 ],
 "Please enable JavaScript to use the Web Console.": [
  null,
  "Web Konsolunu kullanmak için lütfen JavaScript'i etkinleştirin."
 ],
 "Please specify the host to connect to": [
  null,
  "Lütfen bağlanılacak anamakineyi belirtin"
 ],
 "Portable": [
  null,
  "Taşınabilir"
 ],
 "Present": [
  null,
  "Mevcut"
 ],
 "Prompting via ssh-add timed out": [
  null,
  "ssh-add aracılığıyla sorma zaman aşımına uğradı"
 ],
 "Prompting via ssh-keygen timed out": [
  null,
  "ssh-keygen aracılığıyla sorma zaman aşımına uğradı"
 ],
 "RAID chassis": [
  null,
  "RAID kasası"
 ],
 "Rack mount chassis": [
  null,
  "Raf montajlı kasa"
 ],
 "Reboot": [
  null,
  "Yeniden başlat"
 ],
 "Recent hosts": [
  null,
  "En son anamakineler"
 ],
 "Refusing to connect": [
  null,
  "Bağlanmayı reddediyor"
 ],
 "Removals:": [
  null,
  "Kaldırılanlar:"
 ],
 "Remove host": [
  null,
  "Anamakineyi kaldır"
 ],
 "Removing $0": [
  null,
  "$0 kaldırılıyor"
 ],
 "Row expansion": [
  null,
  "Satır genişletme"
 ],
 "Row select": [
  null,
  "Satır seçimi"
 ],
 "Run this command over a trusted network or physically on the remote machine:": [
  null,
  "Bu komutu güvenilir bir ağ üzerinden veya fiziksel olarak uzaktaki makinede çalıştırın:"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "SSH key": [
  null,
  "SSH anahtarı"
 ],
 "SSH key login": [
  null,
  "SSH anahtarı oturum açma"
 ],
 "Sealed-case PC": [
  null,
  "Mühürlü Kasa PC"
 ],
 "Security Enhanced Linux configuration and troubleshooting": [
  null,
  "Güvenlik Gelişmiş Linux yapılandırması ve sorun giderme"
 ],
 "Server": [
  null,
  "Sunucu"
 ],
 "Server closed connection": [
  null,
  "Sunucu bağlantıyı kapattı"
 ],
 "Server has closed the connection.": [
  null,
  "Sunucu bağlantıyı kapattı."
 ],
 "Set time": [
  null,
  "Saati ayarla"
 ],
 "Shell script": [
  null,
  "Kabuk betiği"
 ],
 "Shift+Insert": [
  null,
  "Shift+Insert"
 ],
 "Show confirmation password": [
  null,
  "Onay parolasını göster"
 ],
 "Show password": [
  null,
  "Parolayı göster"
 ],
 "Shut down": [
  null,
  "Kapat"
 ],
 "Single rank": [
  null,
  "Tek sıra"
 ],
 "Space-saving computer": [
  null,
  "Yerden kazandıran bilgisayar"
 ],
 "Specific time": [
  null,
  "Belirli bir zaman"
 ],
 "Stick PC": [
  null,
  "Çubuk PC"
 ],
 "Storage": [
  null,
  "Depolama"
 ],
 "Strong password": [
  null,
  "Güçlü parola"
 ],
 "Sub-Chassis": [
  null,
  "Alt Kasa"
 ],
 "Sub-Notebook": [
  null,
  "Alt Dizüstü"
 ],
 "Synchronized": [
  null,
  "Eşitlendi"
 ],
 "Synchronized with $0": [
  null,
  "$0 ile eşitlendi"
 ],
 "Synchronizing": [
  null,
  "Eşitleniyor"
 ],
 "Tablet": [
  null,
  "Tablet"
 ],
 "The SSH key $0 of $1 on $2 will be added to the $3 file of $4 on $5.": [
  null,
  "$2 üzerindeki $1 kullanıcısının $0 SSH anahtarı, $5 üzerindeki $4 kullanıcısının $3 dosyasına eklenecektir."
 ],
 "The SSH key $0 will be made available for the remainder of the session and will be available for login to other hosts as well.": [
  null,
  "$0 SSH anahtarı, oturumun geri kalanı için kullanılabilir olacak ve diğer anamakinelerde oturum açmak için de kullanılabilecektir."
 ],
 "The SSH key for logging in to $0 is protected by a password, and the host does not allow logging in with a password. Please provide the password of the key at $1.": [
  null,
  "$0 üzerinde oturum açmak için kullanılan SSH anahtarı korumalı ve anamakine bir parola ile oturum açmaya izin vermiyor. Lütfen $1 konumundaki anahtarın parolasını sağlayın."
 ],
 "The SSH key for logging in to $0 is protected. You can log in with either your login password or by providing the password of the key at $1.": [
  null,
  "$0 üzerinde oturum açmak için kullanılan SSH anahtarı korumalı. Oturum açma parolanızla veya $1 konumundaki anahtarın parolasını sağlayarak oturum açabilirsiniz."
 ],
 "The fingerprint should match:": [
  null,
  "Parmak izi eşleşmeli:"
 ],
 "The key password can not be empty": [
  null,
  "Anahtar parolası boş olamaz"
 ],
 "The key passwords do not match": [
  null,
  "Anahtar parolaları eşleşmiyor"
 ],
 "The logged in user is not permitted to view system modifications": [
  null,
  "Oturum açmış kullanıcının sistem değişikliklerini görüntülemesine izin verilmiyor"
 ],
 "The password can not be empty": [
  null,
  "Parola boş olamaz"
 ],
 "The resulting fingerprint is fine to share via public methods, including email.": [
  null,
  "Ortaya çıkan parmak izinin, e-posta dahil olmak üzere herkese açık yöntemlerle paylaşılması uygundur."
 ],
 "The resulting fingerprint is fine to share via public methods, including email. If you are asking someone else to do the verification for you, they can send the results using any method.": [
  null,
  "Ortaya çıkan parmak izi, e-posta dahil ortak yöntemler aracılığıyla paylaşılabilir. Eğer başka birinden doğrulamayı sizin için yapmasını istiyorsanız, sonuçları herhangi bir yöntemi kullanarak gönderebilirler."
 ],
 "The server refused to authenticate '$0' using password authentication, and no other supported authentication methods are available.": [
  null,
  "Sunucu, parola kimlik doğrulamasını kullanarak '$0' kullanıcısının kimliğini doğrulamayı reddetti ve kullanılabilir başka kimlik doğrulama yöntemi yok."
 ],
 "The server refused to authenticate using any supported methods.": [
  null,
  "Sunucu, desteklenen herhangi bir yöntemi kullanarak kimlik doğrulamayı reddetti."
 ],
 "The web browser configuration prevents Cockpit from running (inaccessible $0)": [
  null,
  "Web tarayıcısı yapılandırması Cockpit'in çalışmasını engelliyor (erişilemeyen $0)"
 ],
 "This tool configures the SELinux policy and can help with understanding and resolving policy violations.": [
  null,
  "Bu araç, SELinux ilkesini yapılandırır ve ilke ihlallerinin anlaşılmasına ve çözülmesine yardımcı olabilir."
 ],
 "This tool configures the system to write kernel crash dumps. It supports the \"local\" (disk), \"ssh\", and \"nfs\" dump targets.": [
  null,
  "Bu araç, sistemi çekirdek çökme dökümlerini yazmak için yapılandırır. \"Yerel\" (disk), \"ssh\" ve \"nfs\" döküm hedeflerini destekler."
 ],
 "This tool generates an archive of configuration and diagnostic information from the running system. The archive may be stored locally or centrally for recording or tracking purposes or may be sent to technical support representatives, developers or system administrators to assist with technical fault-finding and debugging.": [
  null,
  "Bu araç, çalışan sistemden bir yapılandırma ve tanılama bilgileri arşivi oluşturur. Arşiv, kayıt veya izleme amacıyla yerel veya merkezi olarak depolanabilir veya teknik hata bulma ve hata ayıklamaya yardımcı olması için teknik destek temsilcilerine, geliştiricilere veya sistem yöneticilerine gönderilebilir."
 ],
 "This tool manages local storage, such as filesystems, LVM2 volume groups, and NFS mounts.": [
  null,
  "Bu araç, dosya sistemleri, LVM2 birim grupları ve NFS bağlamaları gibi yerel depolamayı yönetir."
 ],
 "This tool manages networking such as bonds, bridges, teams, VLANs and firewalls using NetworkManager and Firewalld. NetworkManager is incompatible with Ubuntu's default systemd-networkd and Debian's ifupdown scripts.": [
  null,
  "Bu araç, NetworkManager ve Firewalld kullanarak birleştirmeler, köprüler, takımlar, VLAN'lar ve güvenlik duvarları gibi ağları yönetir. NetworkManager, Ubuntu'nun varsayılan systemd-networkd ve Debian'ın ifupdown betikleriyle uyumsuzdur."
 ],
 "This web browser is too old to run the Web Console (missing $0)": [
  null,
  "Bu web tarayıcısı, Web Konsolunu çalıştırmak için çok eski ($0 eksik)"
 ],
 "Time zone": [
  null,
  "Saat dilimi"
 ],
 "To ensure that your connection is not intercepted by a malicious third-party, please verify the host key fingerprint:": [
  null,
  "Bağlantınızın kötü niyetli bir üçüncü tarafça engellenmediğinden emin olmak için lütfen anamakine anahtar parmak izini doğrulayın:"
 ],
 "To verify a fingerprint, run the following on $0 while physically sitting at the machine or through a trusted network:": [
  null,
  "Bir parmak izini doğrulamak için makinede fiziksel olarak bulunurken veya güvenilir bir ağ aracılığıyla $0 üzerinde aşağıdakileri çalıştırın:"
 ],
 "Toggle date picker": [
  null,
  "Tarihi seçiciyi aç/kapat"
 ],
 "Too much data": [
  null,
  "Çok fazla veri"
 ],
 "Total size: $0": [
  null,
  "Toplam boyut: $0"
 ],
 "Tower": [
  null,
  "Tower"
 ],
 "Transient packageless sessions require the same operating system and version, for compatibility reasons: $0.": [
  null,
  "Geçici paketsiz oturumlar, uyumluluk nedenleriyle aynı işletim sistemi ve sürümünü gerektirir: $0."
 ],
 "Trust and add host": [
  null,
  "Anamakineye güven ve ekle"
 ],
 "Try again": [
  null,
  "Tekrar dene"
 ],
 "Trying to synchronize with $0": [
  null,
  "$0 ile eşitlemeye çalışılıyor"
 ],
 "Unable to connect to that address": [
  null,
  "Bu adrese bağlanılamıyor"
 ],
 "Unable to log in to $0 using SSH key authentication. Please provide the password.": [
  null,
  "SSH anahtar kimlik doğrulaması kullanılarak $0 için oturum açılamıyor. Lütfen parolayı girin."
 ],
 "Unable to log in to $0. The host does not accept password login or any of your SSH keys.": [
  null,
  "$0 üzerinde oturum açılamıyor. Anamakine, parola ile oturum açmayı veya SSH anahtarlarınızdan hiçbirini kabul etmiyor."
 ],
 "Unknown": [
  null,
  "Bilinmiyor"
 ],
 "Unknown host: $0": [
  null,
  "Bilinmeyen anamakine: $0"
 ],
 "Untrusted host": [
  null,
  "Güvenilmeyen anamakine"
 ],
 "User name": [
  null,
  "Kullanıcı adı"
 ],
 "User name cannot be empty": [
  null,
  "Kullanıcı adı boş olamaz"
 ],
 "Validating authentication token": [
  null,
  "Kimlik doğrulama belirteci doğrulanıyor"
 ],
 "Verify fingerprint": [
  null,
  "Parmak izini doğrula"
 ],
 "View all logs": [
  null,
  "Tüm günlükleri görüntüle"
 ],
 "View automation script": [
  null,
  "Otomatikleştirme betiğini görüntüle"
 ],
 "Visit firewall": [
  null,
  "Güvenlik duvarını ziyaret et"
 ],
 "Waiting for other software management operations to finish": [
  null,
  "Diğer yazılım yönetimi işlemlerinin bitmesi bekleniyor"
 ],
 "Weak password": [
  null,
  "Zayıf parola"
 ],
 "Web Console for Linux servers": [
  null,
  "Linux sunucuları için Web Konsolu"
 ],
 "Wrong user name or password": [
  null,
  "Kullanıcı adı veya parola yanlış"
 ],
 "You are connecting to $0 for the first time.": [
  null,
  "İlk kez $0 için bağlanıyorsunuz."
 ],
 "Your browser does not allow paste from the context menu. You can use Shift+Insert.": [
  null,
  "Tarayıcınız, bağlam menüsünden yapıştırmaya izin vermiyor. Shift+Insert kullanabilirsiniz."
 ],
 "Your session has been terminated.": [
  null,
  "Oturumunuz sonlandırıldı."
 ],
 "Your session has expired. Please log in again.": [
  null,
  "Oturumunuzun süresi doldu. Lütfen tekrar oturum açın."
 ],
 "Zone": [
  null,
  "Bölge"
 ],
 "[binary data]": [
  null,
  "[ikili veri]"
 ],
 "[no data]": [
  null,
  "[veri yok]"
 ],
 "in less than a minute": [
  null,
  "bir dakikadan az süre"
 ],
 "less than a minute ago": [
  null,
  "bir dakikadan az bir süre önce"
 ],
 "password quality": [
  null,
  "parola kalitesi"
 ],
 "show less": [
  null,
  "daha az göster"
 ],
 "show more": [
  null,
  "daha fazla göster"
 ]
};
