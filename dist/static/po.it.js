window.cockpit_po = {
 "": {
  "plural-forms": (n) => n != 1,
  "language": "it",
  "language-direction": "ltr"
 },
 "$0 GiB": [
  null,
  "$0 GiB"
 ],
 "$0 day": [
  null,
  "$0 giorno",
  "$0 giorni"
 ],
 "$0 error": [
  null,
  "Errore $0"
 ],
 "$0 exited with code $1": [
  null,
  "$0 terminato con codice $1"
 ],
 "$0 failed": [
  null,
  "$0 fallito"
 ],
 "$0 hour": [
  null,
  "$0 ora",
  "$0 ore"
 ],
 "$0 is not available from any repository.": [
  null,
  "$0 non è disponibile in nessun archivio web."
 ],
 "$0 key changed": [
  null,
  "Chiave $0 modificata"
 ],
 "$0 killed with signal $1": [
  null,
  "$0 terminato con codice $1"
 ],
 "$0 minute": [
  null,
  "$0 minuto",
  "$0 minuti"
 ],
 "$0 month": [
  null,
  "$0 mese",
  "$0 mesi"
 ],
 "$0 week": [
  null,
  "$0 settimana",
  "$0 settimane"
 ],
 "$0 will be installed.": [
  null,
  "$0 sarà installato."
 ],
 "$0 year": [
  null,
  "$0 anno",
  "$0 anni"
 ],
 "1 day": [
  null,
  "1 giorno"
 ],
 "1 hour": [
  null,
  "1 ora"
 ],
 "1 minute": [
  null,
  "1 minuto"
 ],
 "1 week": [
  null,
  "1 settimana"
 ],
 "20 minutes": [
  null,
  "20 minuti"
 ],
 "40 minutes": [
  null,
  "40 minuti"
 ],
 "5 minutes": [
  null,
  "5 minuti"
 ],
 "6 hours": [
  null,
  "6 ore"
 ],
 "60 minutes": [
  null,
  "60 minuti"
 ],
 "A compatible version of Cockpit is not installed on $0.": [
  null,
  "Una versione compatibile di Cockpit non è installata su $0."
 ],
 "A modern browser is required for security, reliability, and performance.": [
  null,
  "Un browser moderno è necessario per la sicurezza, l'affidabilità e le prestazioni."
 ],
 "A new SSH key at $0 will be created for $1 on $2 and it will be added to the $3 file of $4 on $5.": [
  null,
  "Una nuova chiave SSH in $0 verrà creata per $1 su $2 e sarà aggiunta al file $3 di $4 su $5."
 ],
 "Absent": [
  null,
  "Assente"
 ],
 "Accept key and log in": [
  null,
  "Accetta la chiave ed effettua l'accesso"
 ],
 "Add $0": [
  null,
  "Aggiungi $0"
 ],
 "Additional packages:": [
  null,
  "Pacchetti aggiuntivi:"
 ],
 "Administration with Cockpit Web Console": [
  null,
  "Amministrazione con Cockpit Web Console"
 ],
 "Advanced TCA": [
  null,
  "TCA avanzato"
 ],
 "All-in-one": [
  null,
  "Tutto in una volta"
 ],
 "Ansible": [
  null,
  "Ansible"
 ],
 "Ansible roles documentation": [
  null,
  "Documentazione sui ruoli Ansible"
 ],
 "Authentication": [
  null,
  "Autenticazione"
 ],
 "Authentication failed": [
  null,
  "Autenticazione fallita"
 ],
 "Authentication is required to perform privileged tasks with the Cockpit Web Console": [
  null,
  "E' necessario autenticarsi per eseguire azioni privilegiate con Cockpit Web Console"
 ],
 "Authorize SSH key": [
  null,
  "Autorizza chiave SSH"
 ],
 "Automatically using NTP": [
  null,
  "Automaticamente utilizzando NTP"
 ],
 "Automatically using additional NTP servers": [
  null,
  "Automaticamente utilizzando server NTP addizionali"
 ],
 "Automatically using specific NTP servers": [
  null,
  "Automaticamente utilizzando server NTP specifici"
 ],
 "Automation script": [
  null,
  "Script di automazione"
 ],
 "Blade": [
  null,
  "Blade"
 ],
 "Blade enclosure": [
  null,
  "Chassis del blade"
 ],
 "Bus expansion chassis": [
  null,
  "Bus di espansione chassis"
 ],
 "Cancel": [
  null,
  "Annulla"
 ],
 "Cannot forward login credentials": [
  null,
  "Impossibile inoltrare le credenziali di accesso"
 ],
 "Cannot schedule event in the past": [
  null,
  "Non è possibile programmare eventi del passato"
 ],
 "Change": [
  null,
  "Cambia"
 ],
 "Change system time": [
  null,
  "Modifica dell'ora di sistema"
 ],
 "Changed keys are often the result of an operating system reinstallation. However, an unexpected change may indicate a third-party attempt to intercept your connection.": [
  null,
  "La modifica delle chiavi sono di solito il risultato di una reinstallazione del sistema. Tuttavia, una modifica inaspettata può indicare un tentativo di intercettazione da parte di un soggetto terzo."
 ],
 "Checking installed software": [
  null,
  "Verifica del software installato"
 ],
 "Close": [
  null,
  "Chiudi"
 ],
 "Cockpit": [
  null,
  "Cockpit"
 ],
 "Cockpit authentication is configured incorrectly.": [
  null,
  "L'autenticazione di Cockpit non è configurata correttamente."
 ],
 "Cockpit configuration of NetworkManager and Firewalld": [
  null,
  "Configurazione Cockpit del NetworkManager e del Firewalld"
 ],
 "Cockpit could not contact the given host.": [
  null,
  "Cockpit non ha potuto contattare l'host inserito."
 ],
 "Cockpit is a server manager that makes it easy to administer your Linux servers via a web browser. Jumping between the terminal and the web tool is no problem. A service started via Cockpit can be stopped via the terminal. Likewise, if an error occurs in the terminal, it can be seen in the Cockpit journal interface.": [
  null,
  "Cockpit è un gestore di server che rende facile amministrare i server Linux tramite un browser web. Cambiare tra il terminale e lo strumento web non è un problema. Un servizio avviato tramite Cockpit può essere interrotto tramite il terminale. Allo stesso modo, se si verifica un errore nel terminale, può essere visto nella sezione del registro di Cockpit."
 ],
 "Cockpit is not compatible with the software on the system.": [
  null,
  "Cockpit non è compatibile con il software del sistema."
 ],
 "Cockpit is not installed": [
  null,
  "Cockpit non è installato"
 ],
 "Cockpit is not installed on the system.": [
  null,
  "Cockpit non è installato sul sistema."
 ],
 "Cockpit is perfect for new sysadmins, allowing them to easily perform simple tasks such as storage administration, inspecting journals and starting and stopping services. You can monitor and administer several servers at the same time. Just add them with a single click and your machines will look after its buddies.": [
  null,
  "Cockpit è perfetto per i nuovi amministratori di sistema, consentendo loro di eseguire facilmente semplici operazioni come la gestione dell'archiviazione, l'ispezione del registro e l'avvio e l'arresto dei servizi. È possibile monitorare e amministrare più server allo stesso tempo. Basta aggiungerli con un solo clic e se ne prenderà subito cura."
 ],
 "Cockpit might not render correctly in your browser": [
  null,
  "Cockpit potrebbe non essere visualizzato correttamente nel tuo browser"
 ],
 "Collect and package diagnostic and support data": [
  null,
  "Raccogliere e creare un pacchetto di dati diagnostici e di supporto"
 ],
 "Collect kernel crash dumps": [
  null,
  "Acquisisci i dump dei crash del kernel"
 ],
 "Compact PCI": [
  null,
  "PCI compatto"
 ],
 "Confirm key password": [
  null,
  "Conferma la password chiave"
 ],
 "Connect to": [
  null,
  "Collegati a"
 ],
 "Connect to:": [
  null,
  "Collegati a:"
 ],
 "Connection has timed out.": [
  null,
  "Il collegamento è scaduto."
 ],
 "Convertible": [
  null,
  "Convertibile"
 ],
 "Copied": [
  null,
  "Copiato"
 ],
 "Copy": [
  null,
  "Copia"
 ],
 "Copy to clipboard": [
  null,
  "Copia negli appunti"
 ],
 "Create a new SSH key and authorize it": [
  null,
  "Crea una nuova chiave SSH e autorizzala"
 ],
 "Create new task file with this content.": [
  null,
  "Crea un nuovo file di attività con questo contenuto."
 ],
 "Ctrl+Insert": [
  null,
  "Ctrl+Insert"
 ],
 "Delay": [
  null,
  "Ritardo"
 ],
 "Desktop": [
  null,
  "Desktop"
 ],
 "Detachable": [
  null,
  "Rimovibile"
 ],
 "Diagnostic reports": [
  null,
  "Rapporti diagnostici"
 ],
 "Docking station": [
  null,
  "Stazione di docking"
 ],
 "Download a new browser for free": [
  null,
  "Scarica gratuitamente un nuovo browser"
 ],
 "Downloading $0": [
  null,
  "Download di $0"
 ],
 "Dual rank": [
  null,
  "Dual rank"
 ],
 "Embedded PC": [
  null,
  "PC integrato"
 ],
 "Excellent password": [
  null,
  "Password eccellente"
 ],
 "Expansion chassis": [
  null,
  "Chassis di espansione"
 ],
 "Failed to change password": [
  null,
  "Impossibile cambiare la password"
 ],
 "Failed to enable $0 in firewalld": [
  null,
  "Impossibile abilitare firewalld"
 ],
 "Go to now": [
  null,
  "Vai ora"
 ],
 "Handheld": [
  null,
  "Palmare"
 ],
 "Host key is incorrect": [
  null,
  "La chiave host non è corretta"
 ],
 "If the fingerprint matches, click \"Accept key and log in\". Otherwise, do not log in and contact your administrator.": [
  null,
  "Se l'impronta digitale coincide, clicca \"Accetta la chiave e autentica\". Altrimenti, non autenticare e contatta l'amministratore."
 ],
 "Install": [
  null,
  "Installa"
 ],
 "Install software": [
  null,
  "Installa il software"
 ],
 "Install the cockpit-system package (and optionally other cockpit packages) on $0 to enable web console access.": [
  null,
  ""
 ],
 "Installing $0": [
  null,
  "Installazione di $0"
 ],
 "Internal error": [
  null,
  "Errore interno"
 ],
 "Internal error: Invalid challenge header": [
  null,
  "Errore interno: challenge header non valido"
 ],
 "Invalid date format": [
  null,
  "Formato data non valido"
 ],
 "Invalid date format and invalid time format": [
  null,
  "Formato data non valido e formato ora non valido"
 ],
 "Invalid file permissions": [
  null,
  "Autorizzazioni file non valide"
 ],
 "Invalid time format": [
  null,
  "Formato ora non valido"
 ],
 "Invalid timezone": [
  null,
  "Fuso orario non valido"
 ],
 "IoT gateway": [
  null,
  "Gateway IoT"
 ],
 "Kernel dump": [
  null,
  "Kernel dump"
 ],
 "Key password": [
  null,
  "Password della chiave"
 ],
 "Laptop": [
  null,
  "Portatile"
 ],
 "Learn more": [
  null,
  "Per saperne di più"
 ],
 "Loading system modifications...": [
  null,
  "Caricamento modifiche del sistema..."
 ],
 "Log in": [
  null,
  "Accedi"
 ],
 "Log in to $0": [
  null,
  "Entra in $0"
 ],
 "Log in with your server user account.": [
  null,
  "Accedi con il tuo account utente del server."
 ],
 "Log messages": [
  null,
  "Messaggi di log"
 ],
 "Login": [
  null,
  "Accesso"
 ],
 "Login again": [
  null,
  "Effettua di nuovo il login"
 ],
 "Login failed": [
  null,
  "Login fallito"
 ],
 "Logout successful": [
  null,
  "Logout riuscito"
 ],
 "Low profile desktop": [
  null,
  "Desktop a basso profilo"
 ],
 "Lunch box": [
  null,
  "Lunch box"
 ],
 "Main server chassis": [
  null,
  "Chassis del server principale"
 ],
 "Manage storage": [
  null,
  "Gestisci archiviazione"
 ],
 "Manually": [
  null,
  "Manualmente"
 ],
 "Message to logged in users": [
  null,
  "Messaggio agli utenti autenticati"
 ],
 "Mini PC": [
  null,
  "Mini PC"
 ],
 "Mini tower": [
  null,
  "Mini tower"
 ],
 "Multi-system chassis": [
  null,
  "Chassis multisistema"
 ],
 "NTP server": [
  null,
  "Server NTP"
 ],
 "Need at least one NTP server": [
  null,
  "E' necessario almeno un server NTP"
 ],
 "Networking": [
  null,
  "Rete"
 ],
 "New host": [
  null,
  "Nuovo host"
 ],
 "New password was not accepted": [
  null,
  "La nuova password non è stata accettata"
 ],
 "No delay": [
  null,
  "Nessun ritardo"
 ],
 "No results found": [
  null,
  "nessun risultato trovato"
 ],
 "No such file or directory": [
  null,
  "Nessun file o directory"
 ],
 "No system modifications": [
  null,
  "Nessuna modifica di sistema"
 ],
 "Not a valid private key": [
  null,
  "Chiave privata invalida"
 ],
 "Not permitted to perform this action.": [
  null,
  "Non è consentito eseguire questa azione."
 ],
 "Not synchronized": [
  null,
  "Non sincronizzato"
 ],
 "Notebook": [
  null,
  "Portatile"
 ],
 "Occurrences": [
  null,
  "Occorrenze"
 ],
 "Ok": [
  null,
  "Ok"
 ],
 "Old password not accepted": [
  null,
  "Vecchia password non accettata"
 ],
 "Once Cockpit is installed, enable it with \"systemctl enable --now cockpit.socket\".": [
  null,
  "Una volta installato Cockpit, abilitarlo con \"systemctl enable --now cockpit.socket\"."
 ],
 "Or use a bundled browser": [
  null,
  "Oppure utilizzare un browser in bundle"
 ],
 "Other": [
  null,
  "Altro"
 ],
 "Other options": [
  null,
  "Altre opzioni"
 ],
 "PackageKit crashed": [
  null,
  "PackageKit si è interrotto"
 ],
 "Packageless session unavailable": [
  null,
  ""
 ],
 "Password": [
  null,
  "Password"
 ],
 "Password is not acceptable": [
  null,
  "La password non è accettabile"
 ],
 "Password is too weak": [
  null,
  "La password è troppo debole"
 ],
 "Password not accepted": [
  null,
  "Password non accettata"
 ],
 "Paste": [
  null,
  "Incolla"
 ],
 "Paste error": [
  null,
  "Incolla errore"
 ],
 "Path to file": [
  null,
  "Percorso del file"
 ],
 "Peripheral chassis": [
  null,
  "Chassis periferico"
 ],
 "Permission denied": [
  null,
  "Permesso negato"
 ],
 "Pick date": [
  null,
  "Scegli una data"
 ],
 "Pizza box": [
  null,
  "Pizza box"
 ],
 "Please enable JavaScript to use the Web Console.": [
  null,
  "Abilita Javascript per usare Web Console"
 ],
 "Please specify the host to connect to": [
  null,
  "Specifica l'host a cui connettersi"
 ],
 "Portable": [
  null,
  "Portatile"
 ],
 "Present": [
  null,
  "Presente"
 ],
 "Prompting via ssh-add timed out": [
  null,
  "Richiesta tramite ssh-add scaduta"
 ],
 "Prompting via ssh-keygen timed out": [
  null,
  "Richiesta tramite ssh-keygen scaduta"
 ],
 "RAID chassis": [
  null,
  "Chassis RAID"
 ],
 "Rack mount chassis": [
  null,
  "Chassis a rack"
 ],
 "Reboot": [
  null,
  "Riavvia"
 ],
 "Recent hosts": [
  null,
  "Host recenti"
 ],
 "Removals:": [
  null,
  "Rimozioni:"
 ],
 "Remove host": [
  null,
  "Rimuovere l'host"
 ],
 "Removing $0": [
  null,
  "Rimozione $0"
 ],
 "Run this command over a trusted network or physically on the remote machine:": [
  null,
  ""
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "SSH key": [
  null,
  "Chiave SSH"
 ],
 "Sealed-case PC": [
  null,
  "PC sigillato"
 ],
 "Security Enhanced Linux configuration and troubleshooting": [
  null,
  "Configurazione e risoluzione dei problemi di Security Enhanced Linux"
 ],
 "Server": [
  null,
  "Server"
 ],
 "Server has closed the connection.": [
  null,
  "Il server ha chiuso la connessione."
 ],
 "Set time": [
  null,
  "Imposta tempo"
 ],
 "Shell script": [
  null,
  "Script di shell"
 ],
 "Shift+Insert": [
  null,
  "Shift+Insert"
 ],
 "Shut down": [
  null,
  "Arresto"
 ],
 "Single rank": [
  null,
  "Single rank"
 ],
 "Space-saving computer": [
  null,
  "Computer space-saving"
 ],
 "Specific time": [
  null,
  "Tempo specifico"
 ],
 "Stick PC": [
  null,
  "Stick PC"
 ],
 "Storage": [
  null,
  "Archiviazione"
 ],
 "Sub-Chassis": [
  null,
  "Sub-Chassis"
 ],
 "Sub-Notebook": [
  null,
  "Sub-Notebook"
 ],
 "Synchronized": [
  null,
  "Sincronizzato"
 ],
 "Synchronized with $0": [
  null,
  "Sincronizzato con $0"
 ],
 "Synchronizing": [
  null,
  "Sincronizzazione"
 ],
 "Tablet": [
  null,
  "Tablet"
 ],
 "The key password can not be empty": [
  null,
  "La password della chiave non può essere vuota"
 ],
 "The key passwords do not match": [
  null,
  "Le password della chiave non corrispondono"
 ],
 "The logged in user is not permitted to view system modifications": [
  null,
  "L'utente che ha effettuato l'accesso non è autorizzato a visualizzare le modifiche di sistema"
 ],
 "The resulting fingerprint is fine to share via public methods, including email.": [
  null,
  "L'impronta digitale risultante è idonea per la condivisione pubblica, email inclusa."
 ],
 "The resulting fingerprint is fine to share via public methods, including email. If you are asking someone else to do the verification for you, they can send the results using any method.": [
  null,
  ""
 ],
 "The server refused to authenticate '$0' using password authentication, and no other supported authentication methods are available.": [
  null,
  "Il server ha rifiutato di autenticare '$0' utilizzando l'autenticazione con password, e non sono disponibili altri metodi di autenticazione supportati."
 ],
 "The server refused to authenticate using any supported methods.": [
  null,
  "Il server ha rifiutato di autenticarsi utilizzando qualsiasi metodo supportato."
 ],
 "The web browser configuration prevents Cockpit from running (inaccessible $0)": [
  null,
  "La configurazione del browser web impedisce l'esecuzione di Cockpit ($0 inaccessibile)"
 ],
 "This tool configures the SELinux policy and can help with understanding and resolving policy violations.": [
  null,
  "Questo strumento configura i criteri SELinux e può aiutare a comprendere e risolvere le violazioni dei criteri."
 ],
 "This tool generates an archive of configuration and diagnostic information from the running system. The archive may be stored locally or centrally for recording or tracking purposes or may be sent to technical support representatives, developers or system administrators to assist with technical fault-finding and debugging.": [
  null,
  "Questo strumento genera un archivio di informazioni sulla configurazione e sulla diagnostica del sistema in esecuzione. L'archivio può essere conservato localmente o centralmente per scopi di registrazione o tracciamento oppure può essere inviato ai rappresentanti dell'assistenza tecnica, agli sviluppatori o agli amministratori di sistema per aiutarli nella ricerca di errori e nel debug."
 ],
 "This tool manages local storage, such as filesystems, LVM2 volume groups, and NFS mounts.": [
  null,
  "Questo strumento gestisce lo storage locale, come i filesystem, i gruppi di volumi LVM2 e i mount NFS."
 ],
 "This tool manages networking such as bonds, bridges, teams, VLANs and firewalls using NetworkManager and Firewalld. NetworkManager is incompatible with Ubuntu's default systemd-networkd and Debian's ifupdown scripts.": [
  null,
  "Questo strumento gestisce le reti, come i bond, i bridge, i team, le VLAN e i firewall utilizzando NetworkManager e Firewalld. NetworkManager è incompatibile con gli script systemd-networkd di Ubuntu e ifupdown di Debian."
 ],
 "This web browser is too old to run the Web Console (missing $0)": [
  null,
  "Questo browser web è troppo vecchio per eseguire Web Console ($0 mancante)"
 ],
 "Time zone": [
  null,
  "Fuso Orario"
 ],
 "To ensure that your connection is not intercepted by a malicious third-party, please verify the host key fingerprint:": [
  null,
  "Per assicurare che la connessione non sia intercettata da un soggetto terzo malelvolo, verifica l'impronta digitale dell'host:"
 ],
 "To verify a fingerprint, run the following on $0 while physically sitting at the machine or through a trusted network:": [
  null,
  "Per verificare un'impronta digitale, esegui su $0 mentre sei fisicamente di fronte alla macchina o attraverso una rete fidata:"
 ],
 "Toggle date picker": [
  null,
  "Attiva/disattiva la selezione della data"
 ],
 "Too much data": [
  null,
  "Troppi dati"
 ],
 "Total size: $0": [
  null,
  "Dimensione totale: $0"
 ],
 "Tower": [
  null,
  "Tower"
 ],
 "Transient packageless sessions require the same operating system and version, for compatibility reasons: $0.": [
  null,
  ""
 ],
 "Try again": [
  null,
  "Riprova"
 ],
 "Trying to synchronize with $0": [
  null,
  "Tentativo di sincronizzazione con $0"
 ],
 "Unable to connect to that address": [
  null,
  "Impossibile connettersi a quell'indirizzo"
 ],
 "Unknown": [
  null,
  "Sconosciuto"
 ],
 "Untrusted host": [
  null,
  "Host non fidato"
 ],
 "User name": [
  null,
  "Nome utente"
 ],
 "User name cannot be empty": [
  null,
  "Il nome utente non può essere vuoto"
 ],
 "Validating authentication token": [
  null,
  "Convalida del token di autenticazione"
 ],
 "View automation script": [
  null,
  "Visualizza script di automazione"
 ],
 "Waiting for other software management operations to finish": [
  null,
  "In attesa che finiscano le altre operazioni di gestione del software"
 ],
 "Web Console for Linux servers": [
  null,
  "Web Console per server Linux"
 ],
 "Wrong user name or password": [
  null,
  "Nome utente o password errata"
 ],
 "You are connecting to $0 for the first time.": [
  null,
  "Collegamento a $0 per la prima volta"
 ],
 "Your browser does not allow paste from the context menu. You can use Shift+Insert.": [
  null,
  "Il tuo browser non consente l'incolla dal menu contestuale. Puoi usare Maiusc+Ins."
 ],
 "Your session has been terminated.": [
  null,
  "La tua sessione e' terminata."
 ],
 "Your session has expired. Please log in again.": [
  null,
  "La sessione è scaduta. Effettua di nuovo il login."
 ],
 "[binary data]": [
  null,
  "[dati binari]"
 ],
 "[no data]": [
  null,
  "[nessun dato]"
 ],
 "in less than a minute": [
  null,
  ""
 ],
 "less than a minute ago": [
  null,
  ""
 ],
 "show less": [
  null,
  "mostra meno"
 ],
 "show more": [
  null,
  "mostra di più"
 ]
};
