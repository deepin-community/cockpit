window.cockpit_po = {
 "": {
  "plural-forms": (n) => n != 1,
  "language": "ka",
  "language-direction": "ltr"
 },
 "$0 GiB": [
  null,
  "$0 გიბ"
 ],
 "$0 day": [
  null,
  "$0 დღე",
  "დღეები: $0"
 ],
 "$0 error": [
  null,
  "$0 შეცდომა"
 ],
 "$0 exited with code $1": [
  null,
  "$0-ის გამოსვლის კოდია $1"
 ],
 "$0 failed": [
  null,
  "$0 წარუმატებელია"
 ],
 "$0 hour": [
  null,
  "$0 საათი",
  "საათი: $0"
 ],
 "$0 is not available from any repository.": [
  null,
  "$0 ხელმიუწვდომელია ყველა რეპოზიტორიიდან."
 ],
 "$0 key changed": [
  null,
  "$0 გასაღები შეიცვალა"
 ],
 "$0 killed with signal $1": [
  null,
  "$0 მოკვდა სიგნალით $1"
 ],
 "$0 minute": [
  null,
  "$0 წუთი",
  "წუთი: $0"
 ],
 "$0 month": [
  null,
  "$0 თვე",
  "თვე: $0"
 ],
 "$0 week": [
  null,
  "$0 კვირა",
  "კვირა: $0"
 ],
 "$0 will be installed.": [
  null,
  "დაყენდება $0."
 ],
 "$0 year": [
  null,
  "$0 წელი",
  "წელი: $0"
 ],
 "1 day": [
  null,
  "1 დღე"
 ],
 "1 hour": [
  null,
  "1 საათი"
 ],
 "1 minute": [
  null,
  "1 წთ"
 ],
 "1 week": [
  null,
  "1 კვირა"
 ],
 "20 minutes": [
  null,
  "20 წთ"
 ],
 "40 minutes": [
  null,
  "40 წთ"
 ],
 "5 minutes": [
  null,
  "5 წთ"
 ],
 "6 hours": [
  null,
  "5 სთ"
 ],
 "60 minutes": [
  null,
  "60 წთ"
 ],
 "A compatible version of Cockpit is not installed on $0.": [
  null,
  "$0-ზე არ აყენია Cockpit-ის თავსებადი ვერსია."
 ],
 "A modern browser is required for security, reliability, and performance.": [
  null,
  "უსაფრთხოებისთვის, წარმადობისთვის და საიმედოობისთვის საჭიროა ახალი ბრაუზერი."
 ],
 "A new SSH key at $0 will be created for $1 on $2 and it will be added to the $3 file of $4 on $5.": [
  null,
  "ახალი SSH გასაღები $0-თვის შეიქნება $1-თვის $2-ზე და დაემატება $3 ფაილს $4-ის ფაილს $5-ზე."
 ],
 "Absent": [
  null,
  "აკლია"
 ],
 "Accept key and log in": [
  null,
  "მიიღეთ გასაღები და შედით"
 ],
 "Acceptable password": [
  null,
  "მისაღები პაროლი"
 ],
 "Add $0": [
  null,
  "$0-ის დამატება"
 ],
 "Additional packages:": [
  null,
  "დამატებითი პაკეტები:"
 ],
 "Administration with Cockpit Web Console": [
  null,
  "Cockput ვებ კონსოლით ადმინისტრირება"
 ],
 "Advanced TCA": [
  null,
  "დამატებითი TCA"
 ],
 "All-in-one": [
  null,
  "ყველა-ერთში"
 ],
 "Ansible": [
  null,
  "Ansible"
 ],
 "Ansible roles documentation": [
  null,
  "Ansible-ის როლების დოკუმენტაცია"
 ],
 "Authentication": [
  null,
  "ავთენტიკაცია"
 ],
 "Authentication failed": [
  null,
  "ავთენტიკაციის შეცდომა"
 ],
 "Authentication is required to perform privileged tasks with the Cockpit Web Console": [
  null,
  "Cockpit ვებ კონსოლში პრივილეგირებული ამოცანების შესასრულებლად საჭიროა ავთენტიკაცია"
 ],
 "Authorize SSH key": [
  null,
  "SSH გასაღების ავტორიზაცია"
 ],
 "Automatically using NTP": [
  null,
  "ავტომატურად, NTP-ით"
 ],
 "Automatically using additional NTP servers": [
  null,
  "დამატებითი NTP სერვერების ავტომატური გამოყენება"
 ],
 "Automatically using specific NTP servers": [
  null,
  "მითითებული NTP სერვერების ავტომატური გამოყენება"
 ],
 "Automation script": [
  null,
  "ავტომატიზაციის სკრიპტი"
 ],
 "Blade": [
  null,
  "Blade"
 ],
 "Blade enclosure": [
  null,
  "კალათი"
 ],
 "Bus expansion chassis": [
  null,
  "მატარებლის გაფართოების შასი"
 ],
 "Bypass browser check": [
  null,
  "ბრაუზერის შემოწმების გამოტოვება"
 ],
 "Cancel": [
  null,
  "გაუქმება"
 ],
 "Cannot forward login credentials": [
  null,
  "მომხმარებლისადაპაროლის გადაგზავნის შეცდომა"
 ],
 "Cannot schedule event in the past": [
  null,
  "მოვლენის წარსულ დროში დანიშვნა შეუძლებელია"
 ],
 "Change": [
  null,
  "შეცვლა"
 ],
 "Change system time": [
  null,
  "სისტემური დროის შეცვლა"
 ],
 "Changed keys are often the result of an operating system reinstallation. However, an unexpected change may indicate a third-party attempt to intercept your connection.": [
  null,
  "შეცვლილი გასაღებები ხშირად ოპერაციული სისტემის გადაყენებაზე მიუთითებს. ამავე დროს მოულოდნელი ცვლილება კავშირის გადასაჭერად მესამე პირის ჩარევასაც შეიძლება ნიშნავდეს."
 ],
 "Checking installed software": [
  null,
  "დაყენებული პროგრამული უზრუნველყოფის შემოწმება"
 ],
 "Close": [
  null,
  "დახურვა"
 ],
 "Cockpit": [
  null,
  "Cockpit"
 ],
 "Cockpit authentication is configured incorrectly.": [
  null,
  "Cockpit-ის ავთენტიკაციის არასწორი კონფიგურაცია."
 ],
 "Cockpit configuration of NetworkManager and Firewalld": [
  null,
  "NetworkManager-ის და Firewalld-ის მორგება Cockpit-ით"
 ],
 "Cockpit could not contact the given host.": [
  null,
  "Cockpit-ს მითითებულ ჰოსტთან დაკავშირება არ შეუძლია ."
 ],
 "Cockpit is a server manager that makes it easy to administer your Linux servers via a web browser. Jumping between the terminal and the web tool is no problem. A service started via Cockpit can be stopped via the terminal. Likewise, if an error occurs in the terminal, it can be seen in the Cockpit journal interface.": [
  null,
  "Cockpit წარმოადგენს სერვერის მმართველს, რომლითაც Linux სერვერების ადმინისტრირება ბრაუზერითაც შეგიძლიათ. ტერმინალსა და ვებ ხელსაწყოს შორის გადართვა პრობლემა არაა. Cockpit-ით გაშვებული სერვისი შეგიძლიათ გააჩეროთ ტერმინალთაც. ასევე, თუ შეცდომა დაფიქსირდება ტერმინალში, მისი ნახვა Cockpit-ის საშუალებითაც შეგიძლიათ."
 ],
 "Cockpit is not compatible with the software on the system.": [
  null,
  "Cockpit-ი შეუთავსებელია თქვენს სერვერზე დაყენებულ პროგრამულ უზრუნველყოფასთან."
 ],
 "Cockpit is not installed": [
  null,
  "Cockpit-ი დაყენებული არაა"
 ],
 "Cockpit is not installed on the system.": [
  null,
  "Cockpit-ი ამ სისტემაზე დაყენებული არაა."
 ],
 "Cockpit is perfect for new sysadmins, allowing them to easily perform simple tasks such as storage administration, inspecting journals and starting and stopping services. You can monitor and administer several servers at the same time. Just add them with a single click and your machines will look after its buddies.": [
  null,
  "Cockpit შესანიშნავია ახალი სისტემური ადმინისტრატორებისთვის. ის მათ საშუალებას აძლევს ადვილად შეასრულონ ისეთი მარტივი ამოცანები, როგორიცაა შენახვის ადმინისტრირება, ჟურნალების შემოწმება და სერვისების დაწყება და გაჩერება. შეგიძლიათ ერთდროულად რამდენიმე სერვერის მონიტორინგი და ადმინისტრირება. უბრალოდ დაამატეთ ისინი ერთი დაწკაპუნებით და თქვენი მანქანები იზრუნებს მის მეგობრებზე."
 ],
 "Cockpit might not render correctly in your browser": [
  null,
  "Cockpit-ი თქვენს ბრაუზერში შეიძლება არასწორად გამოჩნდეს"
 ],
 "Collect and package diagnostic and support data": [
  null,
  "მოაგროვეთ დიაგნოსტიკური და მხარდაჭერის მონაცემები"
 ],
 "Collect kernel crash dumps": [
  null,
  "ოპერაციული სისტემის ბირთვის ავარიის დამპები"
 ],
 "Compact PCI": [
  null,
  "კომპაქტური PCI"
 ],
 "Confirm key password": [
  null,
  "მეორედ შეიყვანეთ გასაღების პაროლი"
 ],
 "Connect to": [
  null,
  "დაკავშირება"
 ],
 "Connect to:": [
  null,
  "დაკავშირება:"
 ],
 "Connection has timed out.": [
  null,
  "კავშირის დრო გავიდა."
 ],
 "Convertible": [
  null,
  "გარდაქმნადი"
 ],
 "Copied": [
  null,
  "დაკოპირებულია"
 ],
 "Copy": [
  null,
  "კოპირება"
 ],
 "Copy to clipboard": [
  null,
  "ბაფერში კოპირება"
 ],
 "Create $0": [
  null,
  "$0-ის შექმნა"
 ],
 "Create a new SSH key and authorize it": [
  null,
  "შექმენით SSH-ის ახალი გასაღები და გაატარეთ ავტორიზაცია"
 ],
 "Create new task file with this content.": [
  null,
  "ახალი ამოცანის ამ შემცველობით შექმნა."
 ],
 "Ctrl+Insert": [
  null,
  "Ctrl+Insert"
 ],
 "Delay": [
  null,
  "დაყოვნება"
 ],
 "Desktop": [
  null,
  "სამუშაო მაგიდა"
 ],
 "Detachable": [
  null,
  "მოძრობადი"
 ],
 "Diagnostic reports": [
  null,
  "დიაგნოსტიკის ანგარიშები"
 ],
 "Docking station": [
  null,
  "სამაგრი დაფა"
 ],
 "Download a new browser for free": [
  null,
  "გადმოწერეთ ახალი ბრაუზერი უფასოდ"
 ],
 "Downloading $0": [
  null,
  "$0-ის გადმოწერა"
 ],
 "Dual rank": [
  null,
  "ორმაგი რანგი"
 ],
 "Embedded PC": [
  null,
  "ჩაშენებული PC"
 ],
 "Excellent password": [
  null,
  "გადასარევი პაროლი"
 ],
 "Expansion chassis": [
  null,
  "გაფართოების კორპუსი"
 ],
 "Failed to change password": [
  null,
  "პაროლის შეცვლის შეცდომა"
 ],
 "Failed to enable $0 in firewalld": [
  null,
  "firewalld-ში $0-ის ჩართვის შეცდომა"
 ],
 "Go to now": [
  null,
  "ახლავე გადასვლა"
 ],
 "Handheld": [
  null,
  "ჯიბის"
 ],
 "Hide confirmation password": [
  null,
  "დადასტურების პაროლის დამალვა"
 ],
 "Hide password": [
  null,
  "პაროლის დამალვა"
 ],
 "Host is unknown": [
  null,
  "ჰოსტი უცნობია"
 ],
 "Host key is incorrect": [
  null,
  "ჰოსტის გასაღები არასწორია"
 ],
 "Hostkey does not match": [
  null,
  "ჰოსტის გასაღებები არ ემთხვევა"
 ],
 "Hostkey is unknown": [
  null,
  "ჰოსტის გასაღები უცნობია"
 ],
 "If the fingerprint matches, click \"Accept key and log in\". Otherwise, do not log in and contact your administrator.": [
  null,
  "თუ ანაბეჭდი ემთხვევა, დააწექით \"გასაღების მიღება და შესვლა\"-ს. ან არ შეხვიდეთ და დაუკავშირდით ადმინისტრატორს."
 ],
 "If the fingerprint matches, click 'Trust and add host'. Otherwise, do not connect and contact your administrator.": [
  null,
  "თუ ანაბეჭდი ემთხვევა, დააწექით \"ჰოსტის ნდობა და დამატებას\", ან არ შეხვიდეთ და დაუკავშირდით ადმინისტრატორს."
 ],
 "Install": [
  null,
  "დაყენება"
 ],
 "Install software": [
  null,
  "პროგრამების დაყენება"
 ],
 "Install the cockpit-system package (and optionally other cockpit packages) on $0 to enable web console access.": [
  null,
  "ვებ-კონსოლთან წვდომის ჩასართავად $0-ზე cockit-system პაკეტი (და არასავალდებულო cockpit-ის პაკეტები) დააყენეთ."
 ],
 "Installing $0": [
  null,
  "$0-ის დაყენება"
 ],
 "Internal error": [
  null,
  "შიდა შეცდომა"
 ],
 "Internal error: Invalid challenge header": [
  null,
  "შიდა შეცდომა: გამოწვევის არასწორი თავსართი"
 ],
 "Internal protocol error": [
  null,
  "შიდა პროტოკოლის შეცდომა"
 ],
 "Invalid date format": [
  null,
  "თარიღის არასწორი ფორმატი"
 ],
 "Invalid date format and invalid time format": [
  null,
  "თარიღისა და დროის არასწორი ფორმატი"
 ],
 "Invalid file permissions": [
  null,
  "ფაილის არასწორი წვდომები"
 ],
 "Invalid time format": [
  null,
  "დროის არასწორი ფორმატი"
 ],
 "Invalid timezone": [
  null,
  "დროის არასწორი სარტყელი"
 ],
 "IoT gateway": [
  null,
  "IoT gateway"
 ],
 "Kernel dump": [
  null,
  "ბირთვის დამპი"
 ],
 "Key password": [
  null,
  "გასაღების პაროლი"
 ],
 "Laptop": [
  null,
  "ლეპტოპი"
 ],
 "Learn more": [
  null,
  "გაიგეთ მეტი"
 ],
 "Loading system modifications...": [
  null,
  "სისტემის ცვლილებების ჩატვირთვა..."
 ],
 "Log in": [
  null,
  "შესვლა"
 ],
 "Log in to $0": [
  null,
  "$0-ში შესვლა"
 ],
 "Log in with your server user account.": [
  null,
  "შედით სერვერის თქვენი ანგარიშით."
 ],
 "Log messages": [
  null,
  "ჟურნალის შეტყობინებები"
 ],
 "Login": [
  null,
  "შესვლა"
 ],
 "Login again": [
  null,
  "თავიდან შესვლა"
 ],
 "Login failed": [
  null,
  "შესვლა წარუმატებელია"
 ],
 "Logout successful": [
  null,
  "გასვლა წარმატებულია"
 ],
 "Low profile desktop": [
  null,
  "დაბალი პროფილის სამუშაო მაგიდა"
 ],
 "Lunch box": [
  null,
  "Lunch box"
 ],
 "Main server chassis": [
  null,
  "სერვერის მთავარი შასი"
 ],
 "Manage storage": [
  null,
  "საცავის მართვა"
 ],
 "Manually": [
  null,
  "ხელით მითითებული"
 ],
 "Message to logged in users": [
  null,
  "შესული მომხმარებლებისთვის შეტყობინების გაგზავნა"
 ],
 "Mini PC": [
  null,
  "მინი PC"
 ],
 "Mini tower": [
  null,
  "კომპიუტერი პატარა ყუთით"
 ],
 "Multi-system chassis": [
  null,
  "მრავალსისტემიანი ყუთი"
 ],
 "NTP server": [
  null,
  "NTP სერვერი"
 ],
 "Need at least one NTP server": [
  null,
  "საჭიროა ერთი NTP სერვერი მაინც"
 ],
 "Networking": [
  null,
  "ქსელი"
 ],
 "New host": [
  null,
  "ახალი ჰოსტი"
 ],
 "New password was not accepted": [
  null,
  "ახალი პაროლი მიუღებელია"
 ],
 "No delay": [
  null,
  "დაყოვნების გარეშე"
 ],
 "No results found": [
  null,
  "შედეგები ნაპოვნი არაა"
 ],
 "No such file or directory": [
  null,
  "ფაილი ან საქაღალდე არ არსებობს"
 ],
 "No system modifications": [
  null,
  "სისტემა შეცვლილი არაა"
 ],
 "Not a valid private key": [
  null,
  "არ წარმოადგენს სწორ პირად გასაღებს"
 ],
 "Not permitted to perform this action.": [
  null,
  "არ გაქვთ მითითებული მოქმედების შესასრულებლად საკმარისი წვდომა."
 ],
 "Not synchronized": [
  null,
  "სინქრონიზებული არაა"
 ],
 "Notebook": [
  null,
  "ნოუთბუქი"
 ],
 "Occurrences": [
  null,
  "გამოვლენები"
 ],
 "Ok": [
  null,
  "დიახ"
 ],
 "Old password not accepted": [
  null,
  "ძველი პაროლი მიუღებელია"
 ],
 "Once Cockpit is installed, enable it with \"systemctl enable --now cockpit.socket\".": [
  null,
  "როცა Cockpit-ს დააყენებთ, შეგიძლიათ მისი ჩართვაც, ბრძანებით \"systemctl enable --now cockpit.socket\"."
 ],
 "Or use a bundled browser": [
  null,
  "ან გამოიყენეთ მოყოლილი ბრაუზერი"
 ],
 "Other": [
  null,
  "სხვა"
 ],
 "Other options": [
  null,
  "სხვა პარამეტრები"
 ],
 "PackageKit crashed": [
  null,
  "PackageKit-ის ავარია"
 ],
 "Packageless session unavailable": [
  null,
  "უპაკეტო სესია ხელმიუწვდომელია"
 ],
 "Password": [
  null,
  "პაროლი"
 ],
 "Password is not acceptable": [
  null,
  "პაროლი მიუღებელია"
 ],
 "Password is too weak": [
  null,
  "პაროლი ძალიან სუსტია"
 ],
 "Password not accepted": [
  null,
  "პაროლი მიუღებელია"
 ],
 "Paste": [
  null,
  "ჩასმა"
 ],
 "Paste error": [
  null,
  "ჩასმის შეცდომა"
 ],
 "Path to file": [
  null,
  "ბილიკი ფაილამდე"
 ],
 "Peripheral chassis": [
  null,
  "გარე კორპუსი"
 ],
 "Permission denied": [
  null,
  "წვდომა აკრძალულია"
 ],
 "Pick date": [
  null,
  "აირჩიეთ თარიღი"
 ],
 "Pizza box": [
  null,
  "პიცისყუთი"
 ],
 "Please enable JavaScript to use the Web Console.": [
  null,
  "ვებ კონსოლის გამოსაყენებლად საჭიროა JavaScript-ის ჩართვა."
 ],
 "Please specify the host to connect to": [
  null,
  "შეიყვანეთ მისაერთებელი ჰოსტის სახელი"
 ],
 "Portable": [
  null,
  "გადატანადი"
 ],
 "Present": [
  null,
  "წარმოდგენილია"
 ],
 "Prompting via ssh-add timed out": [
  null,
  "მოთხოვნას ssh-add-ის გავლით დრო გაუვიდა"
 ],
 "Prompting via ssh-keygen timed out": [
  null,
  "მოთხოვნას ssh-keygen-ის გავლით დრო გაუვიდა"
 ],
 "RAID chassis": [
  null,
  "RAID კალათი"
 ],
 "Rack mount chassis": [
  null,
  "რეკში ჩასადგმელი შასი"
 ],
 "Reboot": [
  null,
  "გადატვირთვა"
 ],
 "Recent hosts": [
  null,
  "ბოლო ჰოსტები"
 ],
 "Refusing to connect": [
  null,
  "კავშირი უარყოფილია"
 ],
 "Removals:": [
  null,
  "წაიშლება:"
 ],
 "Remove host": [
  null,
  "ჰოსტის წაშლა"
 ],
 "Removing $0": [
  null,
  "$0-ის წაშლა"
 ],
 "Row expansion": [
  null,
  "მწკრივის გაფართოება"
 ],
 "Row select": [
  null,
  "მწკრივის არჩევა"
 ],
 "Run this command over a trusted network or physically on the remote machine:": [
  null,
  "გაუშვით ეს ბრძანება სანდო ქსელით ან ფიზიკურად, დაშორებულ მანქანაზე:"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "SSH key": [
  null,
  "SSH გასაღები"
 ],
 "SSH key login": [
  null,
  "SSH გასაღებით შესვლა"
 ],
 "Sealed-case PC": [
  null,
  "დალუქული PC"
 ],
 "Security Enhanced Linux configuration and troubleshooting": [
  null,
  "Linux-ის გაფართოებული უსაფრთხოების (SELinux) მორგება და გამართვა"
 ],
 "Server": [
  null,
  "სერვერი"
 ],
 "Server closed connection": [
  null,
  "სერვერმა დახურა კავშირი"
 ],
 "Server has closed the connection.": [
  null,
  "სერვერმა დახურა კავშირი."
 ],
 "Set time": [
  null,
  "დროის დაყენება"
 ],
 "Shell script": [
  null,
  "გარსის სკრიპტი"
 ],
 "Shift+Insert": [
  null,
  "Shift+Insert"
 ],
 "Show confirmation password": [
  null,
  "დადასტურების პაროლის ჩვენება"
 ],
 "Show password": [
  null,
  "პაროლის ჩვენება"
 ],
 "Shut down": [
  null,
  "გამორთვა"
 ],
 "Single rank": [
  null,
  "ერთრანგიანი"
 ],
 "Space-saving computer": [
  null,
  "პატარა ზომის კომპიუტერი"
 ],
 "Specific time": [
  null,
  "მითითებული დრო"
 ],
 "Stick PC": [
  null,
  "Stick PC"
 ],
 "Storage": [
  null,
  "საცავი"
 ],
 "Strong password": [
  null,
  "ძლიერი პაროლი"
 ],
 "Sub-Chassis": [
  null,
  "ქვე-კორპუსი"
 ],
 "Sub-Notebook": [
  null,
  "ქვე-ნოუთბუქი"
 ],
 "Synchronized": [
  null,
  "სინქრონიზებულია"
 ],
 "Synchronized with $0": [
  null,
  "სინქრონიზებულია $0-თან"
 ],
 "Synchronizing": [
  null,
  "სინქრონიზაცია"
 ],
 "Tablet": [
  null,
  "ტაბლეტი"
 ],
 "The SSH key $0 of $1 on $2 will be added to the $3 file of $4 on $5.": [
  null,
  "SSH გასაღები $1-ის $0 $2-ზე დაემატება ფაილ $3-ს $5-ზე $4-ს."
 ],
 "The SSH key $0 will be made available for the remainder of the session and will be available for login to other hosts as well.": [
  null,
  "SSH გასაღები $0 ხელმისაწვდომია სესიის ბოლომდე და ხელმისაწვდომი იქნება სხვა ჰოსტებზე შესასვლელადაც."
 ],
 "The SSH key for logging in to $0 is protected by a password, and the host does not allow logging in with a password. Please provide the password of the key at $1.": [
  null,
  "$0-ზე შესასვლელი SSH გასაღები პაროლითაა დაცული და ჰოსტს პაროლით შესვლა გამორთული აქვს. შეიყვანეთ $1-ზე არსებული გასაღების პაროლი."
 ],
 "The SSH key for logging in to $0 is protected. You can log in with either your login password or by providing the password of the key at $1.": [
  null,
  "$0-ზე შესასვლელი SSH გასაღები დაცულია. შეგიძლიათ შეხვიდეთ ან თქვენი პაროლით ან გასაღები $1-ის პაროლით."
 ],
 "The fingerprint should match:": [
  null,
  "ანაბეჭდების უნდა ემთხვეოდეს:"
 ],
 "The key password can not be empty": [
  null,
  "გასაღების პაროლი ცარიელი ვერ იქნება"
 ],
 "The key passwords do not match": [
  null,
  "გასაღების პაროლები არ ემთხვევა"
 ],
 "The logged in user is not permitted to view system modifications": [
  null,
  "შესულ მომხმარებელს არ აქვს სისტემური ცვლილებების ნახვს უფლება"
 ],
 "The password can not be empty": [
  null,
  "პაროლი არ შეიძლება ცარიელი იყოს"
 ],
 "The resulting fingerprint is fine to share via public methods, including email.": [
  null,
  "მიღებული ანაბეჭდების გაზიარება პრობლემა არაა."
 ],
 "The resulting fingerprint is fine to share via public methods, including email. If you are asking someone else to do the verification for you, they can send the results using any method.": [
  null,
  "მიღებული ანაბეჭდის გაზიარება, ელფოსტის ჩათვლით, უსაფრთხოა. თუ თქვენ სხვას სთხოვთ, თქვენი სახელით გადაამოწმოს, მათ, შედეგების გამოგზავნა ნებისმიერი მეთოდით შეუძლიათ."
 ],
 "The server refused to authenticate '$0' using password authentication, and no other supported authentication methods are available.": [
  null,
  "სერვერმა უარყო $0-ის ავთენტიკაცია პაროლის საშუალებით და ავთენტიკაციის სხვა საშუალებები ხელმიუწვდომელია."
 ],
 "The server refused to authenticate using any supported methods.": [
  null,
  "სერვერმა ყველა მხარდაჭერილი მეთოდით ავთენტიკაცია უარჰყო."
 ],
 "The web browser configuration prevents Cockpit from running (inaccessible $0)": [
  null,
  "Cockpit-ის გაშვება შეუძლებელია ბრაუზერის კონფიგურაციის გამო ($0 მიუწვდომელია)"
 ],
 "This tool configures the SELinux policy and can help with understanding and resolving policy violations.": [
  null,
  "ეს პროგრამა როგორც SELinux-ის პოლიტიკის მორგებაში, ასევე მის ბოლომდე გაგებაში და პოლიტიკის დარღვევის გადაწყვეტაში დაგეხმარებათ."
 ],
 "This tool configures the system to write kernel crash dumps. It supports the \"local\" (disk), \"ssh\", and \"nfs\" dump targets.": [
  null,
  "ეს პროგრამა სისტემას ბირთვის ავარიის შემთხვევაში დისკზე დამპის ჩაწერის მორგებაში დაგეხმარებათ. დამპის მხარდაჭერილი სამიზნეებია: \"local\" (დისკი), \"ssh\" და \"nfs\"."
 ],
 "This tool generates an archive of configuration and diagnostic information from the running system. The archive may be stored locally or centrally for recording or tracking purposes or may be sent to technical support representatives, developers or system administrators to assist with technical fault-finding and debugging.": [
  null,
  "ეს პროგრამა გაშვებული სისტემიდან კონფიგურაცისა და დიაგნოსტიკის მოგროვებაში დაგეხმარებათ. არქივი შეგიძლიათ ლოკალურად შეინახოთ, ან ცენტრალურად, ჩაწერისა და ტრეკინგის მიზნებისთვის, ან შეგიძლიათ გადააგზავნოთ მხარდაჭერის, პროგრამისტებისა და სისტემური ადმინისტრატორების ჯგუფებთან, რათა აპარატურული პრობლემები აღმოაჩინოთ და გადაჭრათ."
 ],
 "This tool manages local storage, such as filesystems, LVM2 volume groups, and NFS mounts.": [
  null,
  "ეს პროგრამა ლოკალურ საცავს, როგორიცაა ფაილურ სისტემები, LVM2 ტომის ჯგუფები და NFS მიმაგრებები, მართავს."
 ],
 "This tool manages networking such as bonds, bridges, teams, VLANs and firewalls using NetworkManager and Firewalld. NetworkManager is incompatible with Ubuntu's default systemd-networkd and Debian's ifupdown scripts.": [
  null,
  "ეს პროგრამა მართავს ქსელს. bond ინტერფეისების, ხიდების, ჯგუფური ინტერფეისების, VLAN-ების და ბრანდმაუერების მართვა NetworkManager-ისა და FIrewalld-ის საშუალებით. NetworkManager-ი Ubuntu-ის ნაგულისხმებ systemd-networkd და Debian-ის ifupdown სკრიპტებთან შეუთავსებელია."
 ],
 "This web browser is too old to run the Web Console (missing $0)": [
  null,
  "ეს ბრაუზერ ძალიან ძველია ვებ კონსოლის გასაშვებად (არ გააჩნია $0)"
 ],
 "Time zone": [
  null,
  "დროის სარტყელი"
 ],
 "To ensure that your connection is not intercepted by a malicious third-party, please verify the host key fingerprint:": [
  null,
  "იმაში დასარწმუნებლად, რომ არ ხდება კავშირის გადაჭერა, შეამოწმეთ ჰოსტის ანაბეჭდი:"
 ],
 "To verify a fingerprint, run the following on $0 while physically sitting at the machine or through a trusted network:": [
  null,
  "ანაბეჭდის შესამოწმებლად გაუშვით ეს ბრძანება $0-ზე როცა ფიზიკურად ან სანდო ქსელით იქნებით შესული მანქანაზე:"
 ],
 "Toggle date picker": [
  null,
  "თარიღის ამრჩევის გადართვა"
 ],
 "Too much data": [
  null,
  "მეტისმეტად ბევრი მონაცემი"
 ],
 "Total size: $0": [
  null,
  "ჯამური ზომა: $0"
 ],
 "Tower": [
  null,
  "კომპიუტერის კორპუსი"
 ],
 "Transient packageless sessions require the same operating system and version, for compatibility reasons: $0.": [
  null,
  "შუალედურ უპაკეტო სესიებს იგივე ოპერაციული სისტემა და ვერსია სჭირდება თავსებადობისთვის: $0."
 ],
 "Trust and add host": [
  null,
  "ჰოსტის ნდობა და დამატება"
 ],
 "Try again": [
  null,
  "თავიდან სცადეთ"
 ],
 "Trying to synchronize with $0": [
  null,
  "$0-თან სინქრონიზაციის მცდელობა"
 ],
 "Unable to connect to that address": [
  null,
  "მისამართზე დაკავშირების შეცდომა"
 ],
 "Unable to log in to $0 using SSH key authentication. Please provide the password.": [
  null,
  "$0-ზე SSH გასაღებით ავთენტიკაციის შესვლის პრობლემა. შეიყვანეთ პაროლი."
 ],
 "Unable to log in to $0. The host does not accept password login or any of your SSH keys.": [
  null,
  "$0-ზე შესვლის პრობლემა. ჰოსტი არ იღებს პაროლით შესვლას ან არცერთ თქვენს SSH გასაღებს."
 ],
 "Unknown": [
  null,
  "უცნობი"
 ],
 "Unknown host: $0": [
  null,
  "უცნობი ჰოსტი: $0"
 ],
 "Untrusted host": [
  null,
  "არასანდო ჰოსტი"
 ],
 "User name": [
  null,
  "მომხმარებლის სახელი"
 ],
 "User name cannot be empty": [
  null,
  "მომხმარებლის სახელი ცარიელი არ შეიძლება იყოს"
 ],
 "Validating authentication token": [
  null,
  "ავთენტიკაციის კოდის გადამოწმება"
 ],
 "Verify fingerprint": [
  null,
  "ანაბეჭდის გადამოწმება"
 ],
 "View all logs": [
  null,
  "ყველა ჟურნალის ნახვა"
 ],
 "View automation script": [
  null,
  "ავტომატიზაციის სკრიპტის ნახვა"
 ],
 "Visit firewall": [
  null,
  "ბრანდმაუერზე გადასვლა"
 ],
 "Waiting for other software management operations to finish": [
  null,
  "პროგრამების მართვის სხვა ოპერაციების დასრულების მოლოდინი"
 ],
 "Weak password": [
  null,
  "სუსტი პაროლი"
 ],
 "Web Console for Linux servers": [
  null,
  "ვებ კონსოლი Linux სერვერებისთვის"
 ],
 "Wrong user name or password": [
  null,
  "არასწორი მოხმარებელი ან პაროლი"
 ],
 "You are connecting to $0 for the first time.": [
  null,
  "$0-ს პირველად უკავშირდებით."
 ],
 "Your browser does not allow paste from the context menu. You can use Shift+Insert.": [
  null,
  "თქვენს ბრაუზერს არ გააჩნია კონტექსტური მენიუდან ჩასმის მხარდაჭერა. სცადეთ დააჭიროთ Shift+Insert-ს."
 ],
 "Your session has been terminated.": [
  null,
  "თქვენი სესია გაწყვეტილია."
 ],
 "Your session has expired. Please log in again.": [
  null,
  "სესიის ვადა გასულია. თავიდან შედით."
 ],
 "Zone": [
  null,
  "ზონა"
 ],
 "[binary data]": [
  null,
  "[ბინარული მონაცემები]"
 ],
 "[no data]": [
  null,
  "[მონაცემების გარეშე]"
 ],
 "in less than a minute": [
  null,
  "წუთზე ნაკლებში"
 ],
 "less than a minute ago": [
  null,
  "წუთზე ნაკლების წინ"
 ],
 "password quality": [
  null,
  "პაროლის ხარისხი"
 ],
 "show less": [
  null,
  "ნაკლების ჩვენება"
 ],
 "show more": [
  null,
  "მეტის ჩვენება"
 ]
};
