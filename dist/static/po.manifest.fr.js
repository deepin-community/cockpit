window.cockpit_po = {
 "": {
  "plural-forms": (n) => n > 1,
  "language": "fr",
  "language-direction": "ltr"
 },
 "Diagnostic reports": [
  null,
  "Rapports de diagnostic"
 ],
 "Kernel dump": [
  null,
  "Kernel Dump"
 ],
 "Networking": [
  null,
  "Réseau"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "Storage": [
  null,
  "Stockage"
 ]
};
